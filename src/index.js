//import { browserHistory }       from 'react-router';
//import { hashHistory }          from 'react-router';

//import { createHashHistory }    from 'history';
//import { syncHistoryWithStore } from 'react-router-redux';
//import  createHistory           from 'history/createBrowserHistory';
//import { Router, useRouterHistory } from 'react-router';
//import { Router }               from 'react-router-dom';

import 'regenerator-runtime/runtime';
import React								  from 'react';
import ReactDOM								from 'react-dom';

import configureStore					from 'store/index';
import Root										from 'containers/Root';
import injectTapEventPlugin		from 'react-tap-event-plugin';
import registerServiceWorker	from './registerServiceWorker';

registerServiceWorker();
injectTapEventPlugin();

// const myBrowserHistory = useRouterHistory(createHistory);
// const hashHistory = createHashHistory();
// const store = configureStore(browserHistory);
// const store = configureStore(hashHistory);
/* const store = configureStore(myBrowserHistory);*/

const store = configureStore();
console.log("store", store);

// const history = syncHistoryWithStore(browserHistory, store);
// const history = syncHistoryWithStore(hashHistory, store);
// const history = syncHistoryWithStore(myBrowserHistory, store);

const target = document.getElementById('root');
const node = <Root store={store}/>;
ReactDOM.render(node, target);
