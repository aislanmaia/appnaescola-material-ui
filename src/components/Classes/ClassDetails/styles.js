const styles = theme => ({
	grid: {
		paddingTop: '1rem',
	},
  infoName: {
		display: 'block',
	},
  infoClassroom: {
		display: 'block',
	},
	content: {
    display: 'inline-flex',
		alignItems: 'baseline',
		'& *': {
			fontSize: '1rem',
		},
	},
	studentsGrid: {
    display: 'block',
		paddingBottom: '.5rem',
	},
	professorsGrid: {
    display: 'block',
		paddingBottom: '.5rem',
	},
	fieldset: {
		marginTop: '.2rem',
		padding: 0,
		paddingTop: '.4rem',
		border: 'none',
		borderTop: 'none',
		fontSize: '1.2rem',
		'& legend': {
			paddingRight: '1rem',
		},
	}
});

export default styles;
