import React					from 'react';
import { withStyles } from 'material-ui/styles';
import styles         from './styles';
import Typography     from 'material-ui/Typography';
import {
  Grid,
  VirtualTable,
  TableHeaderRow,
}                     from '@devexpress/dx-react-grid-material-ui';
import Paper from 'material-ui/Paper';
import { Grid as RGrid, Row, Col }   from 'react-flexbox-grid';

const ClassDetails = (props) => {
	const {
		_class,
		classes,
		professorsTableColumns,
		studentsTableColumns,
	} = props;

	const renderClassroomName = () => {
    const { classroom } = _class;
    return classroom ? classroom.name : <i>Sem sala definida</i>
	}

	return (
		<RGrid fluid className={classes.grid}>
			<Row>
				<Col>
					<div className={classes.infoName}>
						<div className={classes.content}>
							<Typography type="body1" align='left'>
								<b>Nome da Turma: &nbsp;</b>
							</Typography>
							<Typography type="body1" gutterBottom>
								<i>{_class.name}</i>
							</Typography>
						</div>
					</div>

					<div className={classes.infoClassroom}>
						<div className={classes.content}>
							<Typography type="body1" align='left'>
								<b>Sala: &nbsp;</b>
							</Typography>
							<Typography type="body1" gutterBottom>
								<i>{renderClassroomName()}</i>
							</Typography>
						</div>
					</div>

					<div className={classes.studentsGrid}>
						<fieldset className={classes.fieldset}>
							<legend>
								<Typography type="body1" align="left">
									<b>Alunos da Turma</b>
								</Typography>
							</legend>

							<Paper>
								<Grid
									rows={_class.students}
									columns={studentsTableColumns}
								>
									<VirtualTable
										messages={{noData: "Sem dados para exibir"}}
										height={400}
									/>
									<TableHeaderRow />
								</Grid>
							</Paper>
						</fieldset>
					</div>

					<div className={classes.professorsGrid}>
						<fieldset className={classes.fieldset}>
							<legend>
								<Typography type="body1" align="left">
									<b>Professores da Turma</b>
								</Typography>
							</legend>

							<Paper>
								<Grid
									rows={_class.professors}
									columns={professorsTableColumns}
								>
									<VirtualTable
										messages={{noData: "Sem dados para exibir"}}
										height={400}
									/>
									<TableHeaderRow />
								</Grid>
							</Paper>
						</fieldset>
					</div>
				</Col>
			</Row>
		</RGrid>
	)
}

export default withStyles(styles, { withTheme: true })(ClassDetails);
