import React					from 'react';
import PropTypes			from 'prop-types';
import Downshift			from 'downshift';
import TextField			from 'material-ui/TextField';
import Paper					from 'material-ui/Paper';
import { MenuItem }		from 'material-ui/Menu';
import { withStyles } from 'material-ui/styles';
import { debounce }		from 'lodash';
import styles         from './styles';
import { Field }      from 'redux-form';


function renderInput(inputProps) {
  const { label, classes, autoFocus, onChange, onBlur, value, ref, ...other } = inputProps;
	console.log("value in renderInput", value);

  return (
		<TextField
		  name={other.name}
			label={label}
			type="text"
			autoFocus={autoFocus}
			className={classes.textField}
			style={{textIndent: 0}}
			value={value}
			inputRef={ref}
			InputProps={{
				classes: {
					input: classes.input,
				},
				...other,
			}}
		  onChange={onChange}
		  onBlur={onBlur}
		/>
	);
}

function renderItem(params) {
  const { item, index, itemProps, theme, highlightedIndex, selectedItem } = params;
  const isHighlighted = highlightedIndex === index;
  const isSelected = selectedItem === item.name;

  return (
			<MenuItem
				{...itemProps}
				key={item.id}
				selected={isHighlighted}
				component="div"
				style={{
					zIndex: 1000,
					fontWeight: isSelected
										? theme.typography.fontWeightMedium
										: theme.typography.fontWeightRegular,
				}}
			>
			{item.name}
     </MenuItem>
  );
}

function renderItemsContainer(options) {
  const { containerProps, children } = options;

  return (
			<Paper {...containerProps} style={{position: 'absolute', width: '100%', zIndex: '1000'}} square>
      {children}
     </Paper>
  );
}

function getItems(inputValue, items) {
	console.log("CALLED getItems with", items);
  let count = 0;

  return items.filter(item => {
    const keep =
					(!inputValue || item.name.toLowerCase().includes(inputValue.toLowerCase())) &&
					count < 5;

    if (keep) {
      count += 1;
    }

    return keep;
  });
}

// const styles = {
//   container: {
//     flexGrow: 1,
//     /* height: 200,*/
// 		zIndex: 1000,
//   },
//   textField: {
//     width: '100%',
//   },
// };

function InputAutoComplete(props) {
  const {
		input,
		label,
		items,
		classes,
		theme,
		/* onChange,*/
		handleOnChange,
		onChangeInput,
		onStateChange,
		onInputValueChange,
		itemToString,
		defaultInputValue,
	} = props;
	console.log("items in InputAutoComplete", items);

  return (
    <Downshift
		  {...input}
		  onChange={(item, utils) => {
				handleOnChange(item, utils.clearSelection);
			}}
		  itemToString={itemToString}
		  onInputValueChange={onInputValueChange}
			defaultInputValue={defaultInputValue}
			//onStateChange={onStateChange}
		  onStateChange={({ inputValue }) => {
					return input.onChange(inputValue);
			}}
			render={({
        getInputProps,
        getItemProps,
        isOpen,
        inputValue,
        selectedItem,
        highlightedIndex,
      }) => (
        <div className={classes.container}>
          {renderInput(
            getInputProps({
							name: input.name,
							label,
              classes,
							onChange: onChangeInput,
							onBlur: input.onBlur,
              placeholder: 'Digite para pesquisar pelo nome',
              id: 'input-autocomplete',
            }),
          )}
          {isOpen
            ? renderItemsContainer({
                children: getItems(inputValue, items).map((item, index) =>
                  renderItem({
                    item,
                    index,
                    theme,
                    itemProps: getItemProps({ item: item }),
                    highlightedIndex,
                    selectedItem,
                  }),
                ),
              })
            : null}
        </div>
      )}
    />
  );
}

InputAutoComplete.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

/* export default withStyles(styles, { withTheme: true })(InputAutoComplete);*/

const Input = props => <Field component={InputAutoComplete} {...props} />;

export default withStyles(styles, { withTheme: true })( Input );
