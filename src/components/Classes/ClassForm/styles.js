import { form, floatingActions } from 'styles/settings';

const styles = theme => (
   Object.assign({}, form(theme), {
		 actions: {
       paddingTop: '2rem'
		 },
		 tooltip: {
			 fontSize: '1rem',
			 padding: '.5rem',
			 zIndex: 4000,
		 },
		 nameSection: {
			 paddingBottom: '4rem',
		 },
		 containerField: {
		 	 extend: form(theme).containerField,
		 	 width: '90%',
		 	 '&:first-of-type': {
		 		 width: '90%',
		 	 },
			 '@media (min-width: 320px) and (max-width: 480px)': {
		 		 '&:first-of-type': {
		 			 width: '75%',
		 		 },
				 width: '75%'
			 }
		 },
		 fields: {
       paddingBottom: '2rem',
		 },
		 buttonSave: {
			 flip: false,
			 position: 'absolute',
			 bottom: 32,
			 right: 32,
		 },
		 profGridContainer: {
			 '& table': {
				 // overflow: 'visible',
			 },
			 '& table tr th:nth-child(1), table tr td:nth-child(1)': {
				 width: '80%',
			 },
			 '& table tr th:nth-child(2), table tr td:nth-child(2)': {
				 overflow: 'visible',
			 },
			 '@media (min-width: 320px) and (max-width: 480px)': {
				 '& table tr th:nth-child(1), table tr td:nth-child(1)': {
					 width: '50%',
					 overflow: 'visible',
				 },
				 '& table tr th:nth-child(2), table tr td:nth-child(2)': {
					 overflow: 'visible',
				 }
			 }
		 },
		 studentGridContainer: {
			 '& table tr th:nth-child(1), table tr td:nth-child(1)': {
				 width: '80%'
			 },
			 '@media (min-width: 320px) and (max-width: 480px)': {
				 '& table tr th:nth-child(1), table tr td:nth-child(1)': {
					 width: '50%'
				 },
			 }
		 },
		 fieldset: {
			 border: 'none',
			 paddingLeft: 0,
			 paddingRight: 0,

			 '& legend': {
				 paddingRight: '10px',
			 }
		 },
		 floatingActions: {
			 ...floatingActions.floatingActions,
			 right: 0,
			 bottom: 0,
		 }
   })
);

export default styles;
