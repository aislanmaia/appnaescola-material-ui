import React, { Component }	from 'react';
import { connect }					from 'react-redux';
import { compose }					from 'redux';
import { withStyles }       from 'material-ui/styles';
import Button								from 'material-ui/Button';
import IconButton           from 'material-ui/IconButton';
import Input								from 'material-ui/Input';
import Typography           from 'material-ui/Typography';
import Paper                from 'material-ui/Paper';
import {
	Template,
	TemplatePlaceholder,
} from '@devexpress/dx-react-core';
import {
  Grid,
	Toolbar,
  VirtualTable,
  TableHeaderRow,
	TableSelection,
}                           from '@devexpress/dx-react-grid-material-ui';
import {
  SelectionState,
	IntegratedSelection,
} from '@devexpress/dx-react-grid';
import {
	reduxForm,
	Field,
	change,
	formValueSelector }				from 'redux-form'; // imported Field
import ClassroomsActions		from 'actions/async/classrooms';
import ProfessorsActions	from 'actions/async/professors';
import StudentsActions		from 'actions/async/students';
import {
  TextField,
}                           from 'redux-form-material-ui';
import Tooltip              from 'material-ui/Tooltip';
import { Row, Col }         from 'react-flexbox-grid';
import {
  selectStudents,
  selectProfessors,
  removeStudents,
  removeProfessors,
  resetSelectStudents,
  resetSelectProfessors }		from 'actions/sync/classes';
import _										from 'lodash';
import { isSubSet }					from 'utils/helpers';

import EditIcon							from 'material-ui-icons/Edit';
import RemoveIcon						from 'material-ui-icons/Delete';
import ShowDetailsIcon      from 'material-ui-icons/RemoveRedEye';
import GroupIcon						from 'material-ui-icons/Group';
import DoorIcon						  from 'material-ui-icons/ContentCopy';
import CheckIcon						from 'material-ui-icons/Check';
import ErrorIcon						from 'material-ui-icons/Error';
import styles								from './styles';
import {
	setSubmitFailed,
	stopSubmit
}														from 'redux-form';
import InputAutoComplete    from './InputAutoComplete';


let classroomsValue = [];

function debouncedCall(...args) {
	const debouncedFn = _.debounce(...args);
	return function(e) {
		e.persist();
		return debouncedFn(e);
	};
}

class ClassForm extends Component {
  constructor(props) {
    super(props);
    this.state =
      { unsaved: true
      , professors: []
      , profsAdded: []
      , profsRemoved: []
      , classroom: []
      , classrooms: []
      , students: []
      , studentsAdded: []
      , studentsRemoved: []
			, studentSelection: []
			, professorSelection: []
      }

		this.InputPropsClass = (classes) => {
			return {
				className: classes.input,
			};
		};

		this.changeStudentSelection = studentSelection => this.setState({ studentSelection });
		this.changeProfessorSelection = professorSelection => this.setState({ professorSelection });
		this._renderSelectProfessors = this._renderSelectProfessors.bind(this);
		this._renderProfs = this._renderProfs.bind(this);
		this.studentActionDisable = this.studentActionDisable.bind(this);
		this.professorActionDisable = this.professorActionDisable.bind(this);

  }
  componentDidMount() {
  }

	componentWillReceiveProps(nextProps) {
	}

  componentWillUnmount() {
    const {
			dispatch,
			resetClassrooms,
			resetActiveClass,
			resetNewClass,
			resetProfessors,
			resetStudents,
		} = this.props;
		resetNewClass();
		resetActiveClass();
    /* resetClassrooms();*/
    /* resetProfessors();*/
		/* resetStudents();*/
		dispatch(resetSelectStudents());
	  dispatch(resetSelectProfessors());
  }

  componentDidUpdate(previousProps, previousState) {
  }

	resetForm() {
		const { submitSucceed } = this.props;
		/* Downshift*/
	}

	/* This is for action icons in student table toolbar */
	studentActionDisable = () => {
    if (_.isEmpty( this.state.studentSelection ) || _.size( this.state.studentSelection ) > 1) {
			return true;
		} else {
      return false;
		}
	}

	/* This is for action icons in professor table toolbar */
	professorActionDisable = () => {
    if (_.isEmpty( this.state.professorSelection ) || _.size( this.state.professorSelection ) > 1) {
			return true;
		} else {
      return false;
		}
	}

	/* This is for applying styles in input */
  handleClassesInput = (field) => {
    const { classes } = this.props;
    return (
       field.meta.touched
				? (!field.meta.error && field.meta.valid)
				? {underline: classes.inputUnderline}
       : {}
       : {}
    )
  }

	/* This is for applying styles in input */
  handleEndAdornment = (field) => {
    const { classes } = this.props;
    return (
			 field.meta.touched
        ? (!field.meta.error && field.meta.valid
           ? <CheckIcon className={classes.validIconStatus} />
           : <ErrorIcon className={classes.invalidIconStatus} />)
       : (!field.meta.error && field.meta.valid
          ? <CheckIcon className={classes.validIconStatus} /> : '')
    )
  }

  _searchClassrooms = (event) => {
		const value = event.target.value;
		const term = value.trim();
		const termInState = this.state.classroomTermSearched;
		if (!_.isEmpty(term) && term !== termInState) {
			this.setState({classrooms: value})
			this.setState({classroomTermSearched: term})
			const { dispatch } = this.props;
			dispatch(ClassroomsActions.searchClassrooms(value));
		}
	}

  _searchProfessors = (event) => {
		const value = event.target.value;
		const term = value.trim();
		const termInState = this.state.profTermSearched;
		if (!_.isEmpty(term) && term !== termInState) {
			this.setState({professors: term})
			this.setState({profTermSearched: term})
			const { dispatch } = this.props;
			dispatch(ProfessorsActions.searchProfessors(term));
		}
  }

  _searchStudents = (event) => {
		const value = event.target.value;
		const term = value.trim();
		const termInState = this.state.studentTermSearched;
		if (!_.isEmpty(term) && term !== termInState) {
			this.setState({students: term})
			this.setState({studentTermSearched: term})
			const { dispatch } = this.props;
			dispatch(StudentsActions.searchStudents(value, {no_class: true}));
		}
  }

  _handleFieldClassroom = (field) => {
    console.log("field", field);
    const { dispatch } = this.props;
    dispatch(change('ClassForm', 'classrooms', this.state.classrooms));
  }

  _handleChange = (name, value) => {
    console.log("valor para mudar: ", value);
    this.setState({...this.state, [name]: value});
  }

  _addProfessorsToSelected = (source, newProf) => {
    let profsAdded = this.state.profsAdded;
		profsAdded = _.unionBy(profsAdded, [newProf], 'id');
    this.setState({profsAdded: profsAdded});
    this.props.dispatch(selectProfessors(source, newProf));
  }

  _removeProfessorFromSelected = (source, toBeRemoved) => {
    let profsRemoved = this.state.profsRemoved;
		const removed = _.reject(source, p => !_.includes(toBeRemoved, p.id))
	  const rest = _.difference(source, removed);

		profsRemoved = _.union(profsRemoved, removed);
    let profsAdded = this.state.profsAdded;
    let added = _.differenceWith(profsAdded, profsRemoved, _.isEqual);

		const professorSelection = _.difference(this.state.professorSelection, toBeRemoved)
    this.setState({profsAdded: added});
    this.setState({profsRemoved});
    this.changeProfessorSelection(professorSelection);
    this.props.dispatch(removeProfessors(removed))
  }

  _removeStudentFromSelected = (source, toBeRemoved) => {
    let studentsRemoved = this.state.studentsRemoved;
	  const removed = _.reject(source, s => !_.includes(toBeRemoved, s.id))
	  const rest = _.difference(source, removed);

    studentsRemoved = _.union(studentsRemoved, removed);
    let studentsAdded = this.state.studentsAdded;
    let added = _.differenceWith(studentsAdded, studentsRemoved, _.isEqual);
		const studentSelection = _.difference(this.state.studentSelection, toBeRemoved)
    this.setState({studentsAdded: added});
    this.setState({studentsRemoved: studentsRemoved});
    this.changeStudentSelection(studentSelection);
    this.props.dispatch(removeStudents(removed));
  }

  _handleChangeProf = (value, callback) => {
		if (value) {
			/* Get the professors added to form's state*/
			let profs = this.state.profsAdded;
			/* Get the professors already added to class being edited */
			let professors = this.props.initialValues.profsAdded;

			const profToAdd = {id: value.id, name: value.name};

			if (_.isEmpty(professors)) {
				this._addProfessorsToSelected(profs, profToAdd);
			} else {
				this._addProfessorsToSelected(professors, profToAdd);
			}

			if (callback) callback();
		}
  }

  _addStudentsToSelected = (source, newStudent) => {
    let studentsAdded = this.state.studentsAdded;
		studentsAdded = _.unionBy(studentsAdded, [newStudent], 'id');
    this.setState({studentsAdded});
    this.props.dispatch(selectStudents(source, newStudent));
  }

  _handleChangeStudent = (value, callback) => {
		if (value) {
			let studentsAdded = this.state.studentsAdded;
			let initialStudents = this.props.initialValues.studentsAdded;
			const studentToAdd = {id: value.id, name: value.name};
			this.setState({studentTermSearched: null});

			if (_.isEmpty(initialStudents)) {
				this._addStudentsToSelected(studentsAdded, studentToAdd);
			} else {
				this._addStudentsToSelected(initialStudents, studentToAdd);
			}

			if (callback) callback();
		}
	}

	_handleChangeClassroom = (value) => {
		this.setState({classrooms: value})
		value ? this.props.dispatch(change("ClassForm", "classroom_id", value.id)) : "";
	}

  _handleGoBackClick (e) {
    e.preventDefault();
    this.props.router.goBack();
  }

  _renderError = (error, field) => {
    if (error) return error.message;
    return field.meta.touched && field.meta.error ? field.meta.error : ''
  }

  _renderProfs = (profs) => {
		const { classes } = this.props;
		const { professorSelection } = this.state;
		const Toolbar = children => (
			<div style={{paddingLeft: '8px'}}>
				<div style={{paddingLeft: '16px', paddingTop: '8px'}}>
					<Typography type="caption" gutterBottom align="left">
						Ações
					</Typography>
				</div>
				<div style={{textAlign: 'left', display: 'flex'}}>
					<Tooltip
						id="tooltip-icon-prof-edit" title="Editar" placement="bottom"
						classes={{
							tooltipBottom: classes.tooltip,
						}}
					>
						<div>
							<IconButton disabled={this.professorActionDisable()} color="primary">
								<EditIcon />
							</IconButton>
						</div>
					</Tooltip>

					<Tooltip
						id="tooltip-icon-prof-remove" title="Remover" placement="bottom"
						classes={{
							tooltipBottom: classes.tooltip,
						}}
					>
						<div>
							<IconButton
								disabled={_.isEmpty(professorSelection)}
								onClick={this._removeProfessorFromSelected.bind(this, profs, this.state.professorSelection)}
								color="primary"
							>
								<RemoveIcon />
							</IconButton>
						</div>
					</Tooltip>

					<Tooltip
						id="tooltip-icon-prof-show" title="Exibir" placement="bottom"
						classes={{
							tooltipBottom: classes.tooltip,
						}}
					>
						<div>
							<IconButton disabled={this.professorActionDisable()} color="primary">
								<ShowDetailsIcon />
							</IconButton>
						</div>
					</Tooltip>
				</div>
			</div>
		)
		return (
			<Paper className={classes.profGridContainer}>
				<Grid
					rows={profs}
					columns={[
						{ name: 'name', title: 'Nome' },
						/* { name: 'actions', title: 'Ações' }*/
					]}
					getRowId={row => row.id}
				>
					<Toolbar rootComponent={Toolbar}/>
					{/*
						<ActionsTypeProvider
						for={['actions']}
						/>
					*/}
					<SelectionState
            selection={professorSelection}
            onSelectionChange={this.changeProfessorSelection}
          />
					<IntegratedSelection />
					<VirtualTable
						messages={{noData: "Sem professores na turma"}}
						height={400}
					/>
					<TableSelection
					  showSelectAll
						selectByRowClick
						highlightRow
					/>
					<TableHeaderRow />
				</Grid>
			</Paper>
		)
  }

  _renderStudents = (students) => {
		const { classes } = this.props;
		const { studentSelection } = this.state;

		const Toolbar = children => (
			<div style={{paddingLeft: '8px'}}>
				<div style={{paddingLeft: '16px', paddingTop: '8px'}}>
					<Typography type="caption" gutterBottom align="left">
						Ações
					</Typography>
				</div>
				<div style={{textAlign: 'left', display: 'flex'}}>
					<Tooltip
						id="tooltip-icon-student-edit" title="Editar" placement="bottom"
						classes={{
							tooltipBottom: classes.tooltip,
						}}
					>
						<div>
							<IconButton disabled={this.studentActionDisable()} color="primary">
								<EditIcon />
							</IconButton>
						</div>
					</Tooltip>

					<Tooltip
						id="tooltip-icon-student-remove" title="Remover" placement="bottom"
						classes={{
							tooltipBottom: classes.tooltip,
						}}
					>
						<div>
							<IconButton
								disabled={_.isEmpty(studentSelection)}
								onClick={this._removeStudentFromSelected.bind(this, students, this.state.studentSelection)}
								color="primary"
							>
								<RemoveIcon />
							</IconButton>
						</div>
					</Tooltip>

					<Tooltip
						id="tooltip-icon-student-show" title="Exibir" placement="bottom"
						classes={{
							tooltipBottom: classes.tooltip,
						}}
					>
						<div>
							<IconButton disabled={this.studentActionDisable()} color="primary">
								<ShowDetailsIcon />
							</IconButton>
						</div>
					</Tooltip>
				</div>
			</div>
		)

		return (
			<Paper className={classes.studentsGridContainer}>
				<Grid
					rows={students}
					columns={[
						{ name: 'name', title: 'Nome' },
					]}
					getRowId={row => row.id}
				>
					<Toolbar rootComponent={Toolbar}/>
					<SelectionState
            selection={studentSelection}
            onSelectionChange={this.changeStudentSelection}
          />
					<IntegratedSelection />
					<VirtualTable
						messages={{noData: "Sem alunos na turma"}}
						height={400}
					/>
					<TableSelection
					  showSelectAll
						selectByRowClick
						highlightRow
					/>
					<TableHeaderRow />
				</Grid>
			</Paper>
		)

  }

  _renderInputName = (field) => {
		const { classes } = this.props;
    return (
			<div className={classes.containerField}>
				<EditIcon className={classes.fieldIcon}/>
				<TextField
					type="text"
					style={{textIndent: 0}}
					label="Nome"
					placeholder="Digite o nome da turma"
					className={classes.textField}
					InputProps={{
						endAdornment: this.handleEndAdornment(field),
						classes: this.handleClassesInput(field),
						inputProps: this.InputPropsClass(classes)
					}}
					error={field.meta.touched && field.meta.error ? !!field.meta.error : false}
					helperText={field.meta.touched && field.meta.error ? field.meta.error : ''}
					{...field.input}
					autoFocus={true}
				/>
			</div>
		);
  }

  _renderSelectProfessors = (field) => {
    const { professors, professorsError, classes } = this.props;
		let profs = _.differenceBy(professors, this.props.profsAdded, 'id');

    return (
			<div className={classes.containerField}>
				<GroupIcon className={classes.fieldIcon}/>
				<InputAutoComplete
				  name="professor"
			    label="Escolha os Professores"
					items={profs}
					className={classes.textField}
					//onChange={( item, utils ) => {this._handleChangeProf(item); /* utils.clearSelection(); */}}
					handleOnChange={this._handleChangeProf}
					onChangeInput={debouncedCall(this._searchProfessors, 500)}
					itemToString={item => item ? item.name : ''}
				/>
			</div>
    )
  }


  _renderSelectClassrooms = (field) => {
    const { classrooms, classes, classroomsError, initialValues, values } = this.props;
		const { classroom } = initialValues;

    const { activeClass } = this.props;

    /* let value = '';
		 * if (!field.meta.active && !field.meta.touched) {
		 *   if (activeClass) {
		 *     value = activeClass.classroom ? activeClass.classroom.name : this.state.classrooms;
		 *   }
		 * } else {
		 *   value = this.state.classrooms;
		 * }
		 */
    return (
			<div className={classes.containerField}>
				<DoorIcon className={classes.fieldIcon}/>
				<InputAutoComplete
				  name="classroom"
				  defaultInputValue={classroom ? classroom.name : ''}
			    label="Escolha a Sala"
					items={classrooms}
					className={classes.textField}
					//onChange={item => this._handleChangeClassroom(item)}
				  handleOnChange={this._handleChangeClassroom}
					onChangeInput={debouncedCall(this._searchClassrooms, 500)}
					//onInputValueChange={_.debounce(this._searchClassrooms, 500)}
					itemToString={item => item ? item.name : ''}
					//onStateChange={ item => console.log("state", item)}
				/>
			</div>
    )

  }


  _renderSelectStudents = (field) => {
    const { classes, studentsError, students } = this.props;
		let studs = _.differenceBy(students, this.props.studentsAdded, 'id');
    return (
			<div className={classes.containerField}>
				<GroupIcon className={classes.fieldIcon}/>
				<InputAutoComplete
				  name="student"
			    label="Escolha os Alunos"
					items={studs}
					className={classes.textField}
					//onChange={( item, utils ) => {this._handleChangeStudent(item); utils.clearSelection();}}
				  handleOnChange={this._handleChangeStudent}
					onChangeInput={debouncedCall(this._searchStudents, 500)}
					itemToString={item => item ? item.name : ''}
				/>
			</div>
    )
  }


  render() {
    const {
			activeClass,
			classes,
			classId,
			dirty,
			dispatch,
			handleSubmit,
			submitting,
			selectedProfessors,
			professors,
			students,
			initialValues,
			values,
		} = this.props;


    console.log("this.props", this.props);
    // console.log("selectedProfessors.all", selectedProfessors.all);
    // console.log("values.professors", values.professors);
    // console.log("this.props.professors", this.props.professors);
    console.log("professors", professors);
    console.log("this.state.professorSelection", this.state.professorSelection);
    console.log("values of form", values);
    console.log("studentsAdded in state", this.state.studentsAdded);

    let professorsAdded = [];
    let studentsAdded = [];
    if (activeClass) {
      if (activeClass.professors.length > 0) {
        // console.log("activeClass - entrou no primeiro if")
        professorsAdded = activeClass.professors;
      } else {
        if (this.state.profsAdded.length > 0) {
          // console.log("activeClass - entrou no segundo if")
          professorsAdded = this.state.profsAdded;
        } else {
          // console.log("activeClass - entrou no terceiro if")
          professorsAdded = selectedProfessors.all;
        }
      }
    } else {
      if (this.state.profsAdded.length > 0) {
        console.log("entrou no segundo if")
        professorsAdded = this.state.profsAdded;
      } else {
        console.log("entrou no terceiro if")
        console.log("selectedProfessors.all", selectedProfessors.all)
        professorsAdded = selectedProfessors.all;
      }
    }

    /* const professorsAdded = activeClass ? activeClass.professors : this.state.profsAdded;*/
    if (activeClass) {
      if (activeClass.students.length > 0) {
        console.log("activeClass - entrou no segundo if")
        studentsAdded = activeClass.students;
      } else {
        console.log("activeClass - entrou no else do segundo if")
        studentsAdded = this.state.studentsAdded;
        console.log("studentsAdded", studentsAdded);
      }
    } else {
      console.log("entrou no else do primeiro if")
      studentsAdded = this.state.studentsAdded;
    }
    /* const studentsAdded = activeClass ? activeClass.students : this.state.studentsAdded;
     */
    return (
      <div className="classform-section">
        <form onSubmit={
          classId ? handleSubmit(this.props.updateClass.bind(this, initialValues, values, dispatch))
                    : handleSubmit(this.props.createClass.bind(this, values, dispatch, this.forceUpdate))
        }
				>
					<Row around="sm">
						<Col xs={12} md={6} className={classes.fields}>
							<Field
								name="name"
								component={this._renderInputName}
								type="text"
							/>
						</Col>
						<Col xs={12} md={6} className={classes.fields}>
							<Field
								name="professors"
								value={this.props.professors}
								component={this._renderSelectProfessors}
							/>
						</Col>
					</Row>
					<Row between="sm">
						<Col xs={12} md={6} className={classes.fields}>
							<Field
								type="text"
								name="classroom"
								value={values.classrooms}
								source={this.props.classrooms}
								component={this._renderSelectClassrooms}
							/>
							{/* {this._renderSelectClassrooms()} */}
						</Col>
						<Col xs={12} md={6} className={classes.fields}>
							<Field
								name="students"
							  value={this.props.students}
								component={this._renderSelectStudents}
							/>
						</Col>
					</Row>

					<Row around="sm">
						<Col xs={12} md={6} className={classes.fields}>
							<fieldset className={classes.fieldset}>
								<legend>
									<Typography type="body1" align="left">
										<span>Professores</span>
									</Typography>
								</legend>
								{this._renderProfs(this.props.professorsAll)}
							</fieldset>
						</Col>
						<Col xs={12} md={6} className={classes.fields}>
							<fieldset className={classes.fieldset}>
								<legend>
									<Typography type="body1" align="left">
										<span>Alunos da Turma</span>
									</Typography>
								</legend>
								{this._renderStudents(this.props.studentsAll)}
							</fieldset>
						</Col>
					</Row>
					<div className={classes.floatingActions}>
						<Tooltip
							id="tooltip-icon-save" title="Salvar dados" placement="bottom"
							classes={{
								tooltipBottom: classes.tooltip,
							}}
						>
							<Button fab rootRef={el => this.button = el} color="primary" aria-label="save" type="submit" className={classes.buttonSave}>
								<CheckIcon />
							</Button>
						</Tooltip>
					</div>
				</form>
			</div>
		);
	}

}


const selector = formValueSelector('ClassForm');

const mapStateToProps =	(state) => {
	// can select values individually
	const values = selector(
		state,
		'id',
		'name',
		'professors',
		'students',
		'classroom_id',
		'classroom'
	);

	console.log("Mapping and selecting values from state and form...");

	let professors = state.classes.activeClass.class ? state.classes.activeClass.class.professors : [];
	let professorsSelected = state.classes.professorsSelected;
	let professorsAll = state.classes.professorsSelected.all;
	let professorsRemoved = state.classes.professorsSelected.removed;
	let professorsAdded = state.classes.professorsSelected.added;
	let students = state.classes.activeClass.class ? state.classes.activeClass.class.students : [];
	let studentsSelected = state.classes.studentsSelected;
	let studentsAll = state.classes.studentsSelected.all;
	let studentsRemoved = state.classes.studentsSelected.removed;
	let studentsAdded = state.classes.studentsSelected.added;

	if (!_.isEmpty(professors) && _.isEmpty(professorsSelected.all)) {
		values.professors = professors;
		values.profsRemoved = professorsRemoved;
		values.profsAdded = professorsAdded;
		console.log("values.professors", values.professors);
		console.log("values.professorsRemoved", values.professorsRemoved);
	} else {
		values.profsAdded = state.classes.professorsSelected.added;
		values.profsRemoved = professorsRemoved;
		console.log("values.profsAdded", values.profsAdded);
		console.log("values.profsAll", state.classes.professorsSelected.all);
		console.log("values.profsRemoved", values.profsRemoved);

		/* If the list of all professors preadded is empty, no make sense send to the server
		 * any information about professors removed.
		 */
		if (_.isEmpty(professorsAll) && !_.isEmpty(professorsRemoved)) {
			console.log("entrou aqui -- professorsAll empty && professorsRemoved contem elemento(s)")
			console.log("professorsAll", professorsAll)
			values.profsRemoved = [];
			console.log("values.professorsRemoved", values.professorsRemoved);
		}
	}

	if (!_.isEmpty(students) && _.isEmpty(studentsSelected.all)) {
		values.students = students;
		console.log("values.students", values.students);
		/* values.studentsAll = studentsAll;*/
		values.studentsRemoved = studentsRemoved;
		values.studentsAdded = studentsAdded;
	} else {
		/* values.studentsAll = studentsAll;*/
		values.students = students;
		values.studentsAdded = state.classes.studentsSelected.added;
		values.studentsRemoved = studentsRemoved;

		/* If the list of all students preadded is empty, no make sense send to the server
		 * any information about students removed.
		 */
		if (_.isEmpty(studentsAll) && !_.isEmpty(studentsRemoved)) {
			values.studentsRemoved = [];
		}
	}


	return {
		values,
		professorsAll,
		professorsAdded,
		studentsAll: studentsAll,
		studentsAdded: studentsAdded
	}
}

const wrapper = compose(
	 connect(mapStateToProps),
	 withStyles(styles, {withTheme: true}),
);

export default wrapper(ClassForm);
