const styles = theme => ({
	root: { paddingTop: '.5rem' },
  inputSingleline: { height: '2rem' },
	rowSelected: {
		backgroundColor: '#ccc',
	},
	rowStyle: {
		"& td:first-child": {
			padding: '.5rem 56px .5rem 24px',
		},
		cursor: 'pointer',
	},
})

export default styles;
