import React, { Component } from 'react';
import { connect }					from 'react-redux';
// import { push }          from 'react-router-redux';
import { withStyles }				from 'material-ui/styles';
import { compose }					from 'redux';
import MUIGrid              from 'material-ui/Grid';
import Loadable							from 'containers/Loadable';
import styles								from './styles';

const ClassesTable = Loadable({loader: () => import('components/Classes/ClassList/ClassesTable')});


class ClassList extends Component {

	constructor(props) {
    super(props);

		this.state =
			{ dialogDeleteActive: false,
				_class: '',
				currentPage: props.currentPage,
			};
  }

	_renderActions = (_class) => {
		return (
      [
        <span key={_class.id}>
          {/* <TooltipButton icon='visibility' tooltip='Ver detalhes' />
							<TooltipButton icon='edit' tooltip='Editar sala' onMouseUp={this._handleEditClick.bind(null, _class.id)} />
							<TooltipButton icon='delete' tooltip='Remover sala' onMouseUp={this._handleToggleDeleteDialog.bind(null, _class)} /> */}
        </span>
      ]
    )
  }

  render() {
		const { fetchMoreClasses,
						tableSearchClasses,
						currentPage,
						searchResults,
						setSelection,
						selection,
						searchDialogOpen,
						requestClearSelectedRows,
						toggleRequestClearSelectedRows,
						deleteDispatched,
					} = this.props;

		return (
		  <MUIGrid container>
			  <MUIGrid item xs={12} sm={12}>
					<div className="list-classes">
						<ClassesTable
							_classes={this.props._classes}
							fetchMoreData={fetchMoreClasses}
							tableSearchClasses={tableSearchClasses}
							currentPage={currentPage}
							searchResults={searchResults}
							setSelection={setSelection}
							selection={selection}
							searchDialogOpen={searchDialogOpen}
							requestClearSelectedRows={requestClearSelectedRows}
							toggleRequestClearSelectedRows={toggleRequestClearSelectedRows}
							deleteDispatched={deleteDispatched}
						/>
					</div>
				</MUIGrid>
			</MUIGrid>
		);
	}
}

const mapStateToProps = (state) => (
	{
		/* currentPage: state.classrooms.classroomsList.classrooms.page_number,*/
    totalPages: state.classrooms.classroomsList.classrooms ? state.classrooms.classroomsList.classrooms.total_pages : 1
		, deleteError: state.classrooms.deletedClassroom ? state.classrooms.deletedClassroom.error : null
		, deleteSuccess: state.classrooms.deletedClassroom ? state.classrooms.deletedClassroom.success : null
  }
);

const mapDispatchToProps = (dispatch) => ({
  dispatch
});


const wrapper = compose(
	 connect(mapStateToProps, mapDispatchToProps),
   withStyles(styles, {withTheme: true}),
);

export default wrapper(ClassList);

