import React, { Component } from 'react';
import { connect }					from 'react-redux';
import { withStyles }				from 'material-ui/styles';
import { compose }					from 'redux';
import classNames           from 'classnames';
import MUIGrid              from 'material-ui/Grid';
import Checkbox							from 'material-ui/Checkbox';
import Input								from 'material-ui/Input';
import Paper                from 'material-ui/Paper';
import styles								from './styles';

// import ClickAwayListener    from 'material-ui/utils/ClickAwayListener';
// import Grow                 from 'material-ui/transitions/Grow';
// import { Manager, Target, Popper, Arrow }  from 'react-popper';
import {
  Template, TemplateConnector, TemplateRenderer
} from '@devexpress/dx-react-core';
import {
  Grid,
	/* TableView,*/
	VirtualTable,
	TableHeaderRow,
	TableFilterRow,
	TableSelection,
	/* TableColumnResizing,*/
}														from '@devexpress/dx-react-grid-material-ui';
import {
  FilteringState,
  IntegratedFiltering,
	IntegratedSelection,
	SelectionState,
	SortingState,
  IntegratedSorting,
}														from '@devexpress/dx-react-grid';
import _                    from 'lodash';

/* const RowComponent = ({ row, tableRow, selected, onToggle, ...restProps }) => {
 *   // workaround for using the click & doubleClick events at the same time
 *   // from https://stackoverflow.com/questions/25777826/onclick-works-but-ondoubleclick-is-ignored-on-react-component
 *   let timer = 0;
 *   let delay = 200;
 *   let prevent = false;
 *   const handleClick = () => {
 *     timer = setTimeout(() => {
 *       if (!prevent) {
 *         onToggle();
 * 
 * 				this.props.setSelection(row);
 *       }
 *       prevent = false;
 *     }, delay);
 *   };
 *   const handleDoubleClick = () => {
 *     clearTimeout(timer);
 *     prevent = true;
 *     alert(JSON.stringify(tableRow.row));
 *   }
 *   return (
 *     <EnhancedRow
 *       {...restProps}
 *       className={selected ? 'active' : ''}
 *       style={{ color: 'green' }}
 *       onClick={handleClick}
 *       onDoubleClick={handleDoubleClick}
 * 			onShow={this.rowShown}
 *     />
 *   );
 * };
 * */

const cellStyle = {
	"&:first-child": {
		padding: '.5rem 56px .5rem 24px',
	},
	borderBottom: '1px rgba(0, 0, 0, 0.075) solid',
	fontSize: '.8rem',
}


const FilterCell = ({value, setFilter, classes}) => {
	return (
		 <td>
			 <Input
				 label='Nome'
				 placeholder='Filtrar'
				 value={value}
				 onChange={
					 e => {
						 setFilter(e.target.value ? { value: e.target.value } : null);
					 }
				 }
				 classes={{
					 root: classes.root,
					 inputSingleline: classes.inputSingleline,
				 }}
				 // margin="normal"
				 />
		 </td>
	)
}

class RowBaseComponent extends React.PureComponent {
  componentDidMount() {
		console.log("Class ROW did mount...");
    this.props.onShow(this.props.row);
  }
  render() {
    const { children } = this.props;
    return <tr
						className={classNames({ [ this.props.classes.rowSelected ] : this.props.selected }, this.props.classes.rowStyle)}
						onClick={this.props.handleClick}
						//onDoubleClick={this.props.handleDoubleClick}
					 >
			{children}
		</tr>;
  }
}

const EnhancedRow = withStyles(styles, { withTheme: true })(RowBaseComponent);

class Cell extends Component {
  componentDidMount() {
  }
  render() {
    const { value } = this.props;
		if (this.props.column.name === 'students_count' ||
			  this.props.column.name === 'professors_count') {
			return <td style={cellStyle}>{value} cadastrados</td>;
		}
    return <td style={cellStyle}>{value}</td>;
  }
}

class ClassesTable extends Component {

	constructor(props) {
    super(props);

		this.state =
			{	currentPage: props.currentPage,
				filters: [{ columnName: 'name', value: '' }],
				selection: [],
				sorting: [{ columnName: 'name', direction: 'asc' }],
			 /* rowSelection: [],*/
				noDataMessage: 'Sem dados retornados para exibir',
			 /* columnWidths: {
					name: 150, students_count: 150, professors_count: 180
					},*/
			};

    this.rowShown = this.rowShown.bind(this);
		this.selectFilterEditor = this.selectFilterEditor.bind(this);
		// this.tableSearchClasses = this.tableSearchClasses.bind(this);

		this.changeSorting = sorting => this.setState({ sorting });
		this.integratedSortingColumnExtensions = [
			{ columnName: 'name', compare: this.compareNumbers },
		];
		this.changeFilters = filters => this.setState({ filters });
		/* this.setRowSelection = this.setRowSelection.bind(this);*/
		this.clearSelectedRows = this.clearSelectedRows.bind(this);

		this.changeSelection = selection => this.setState({ selection });
		/* this.changeSelection = selection => props.setSelection(selection);
		 */
		/* this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
		 */
		/* this.changeColumnWidths = (columnWidths) => {
			 this.setState({ columnWidths });
			 };*/
		this.handleCheckboxAllChange = this.handleCheckboxAllChange.bind(this);
		/* this.tableRowTemplate = ({ row, children, selected, changeSelected }) => {*/
		this.RowComponent = ({ row, tableRow, children, selected, onToggle, ...restProps }) => {
      // workaround for using the click & doubleClick events at the same time
      // from https://stackoverflow.com/questions/25777826/onclick-works-but-ondoubleclick-is-ignored-on-react-component
      let timer = 0;
      let delay = 0;
      let prevent = false;
      const handleClick = () => {
        timer = setTimeout(() => {
          if (!prevent) {
						// console.log('handleClick called with:');
						// console.log('row = ', row);
						// console.log('this.state.rowSelection', this.state.rowSelection);
            /* if (!_.isEqual(this.state.rowSelection, row)) {*/
						/* changeSelected();*/
						  onToggle();
							this.props.setSelection(tableRow.row);
						/* }*/
          }
          prevent = false;
        }, delay);
      };
      const handleDoubleClick = () => {
        clearTimeout(timer);
        prevent = true;
        console.log("row selected by double click", JSON.stringify(row));
      }
      return (
				<EnhancedRow
          {...restProps}
				  row={row}
					children={children}
					handleClick={handleClick}
					handleDoubleClick={handleDoubleClick}
					selected={selected}
					onShow={this.rowShown}
				/>
			);
    };

		this.headerRowTemplate = ({tableRow, children, style}) => (
			 <tr
				 style={{
					 width: '100%',
					 fontSize: '.9rem',
					 textTransform: 'uppercase',
					 fontWeight: 'bolder',
					 display: 'table-row',
				 }}
				>
				 {children}
			 </tr>
		);

	}

	componentDidMount() {
	}

	componentWillReceiveProps = (nextProps) => {
		if (nextProps) {
      if (nextProps.requestClearSelectedRows) {
				this.setState({selection: []});
				this.changeFilters([{ columnName: 'name', value: '' }])
				this.props.toggleRequestClearSelectedRows();
			}
		}
	}

	/* setRowSelection(row) {
		 this.setState({rowSelection: [row]});
		 }
	 */
	selectFilterEditor({ column, filter, setFilter }) {
		const { classes } = this.props;
    // const  setFilterFn =
		// 			val => setFilter(val ? { value: val } : null);
		return (
			<FilterCell
				value={filter ? filter.value : null}
				setFilter={setFilter}
				classes={classes}
			/>
		);
	}

  _showMore = (page) => {
    const { fetchMoreData } = this.props;
    fetchMoreData(page);
    this.setState({currentPage: page});
  }


	rowShown(row) {
		const { data } = this.props._classes;

		if (data.length - data.indexOf(row) < 2) {
			this._showMore(this.props.currentPage + 1);
    }
  }

  getRowsData(searchResults, _classes) {
		if (searchResults && !_.isEmpty(searchResults)) {
			return searchResults;
		}
		if (_classes.data && !_.isEmpty(_classes.data)) {
			return _classes.data;
		}
	}

	clearSelectedRows() {
		this.setState({selection: []});
	}

	/* This wrapper function to _.debounce is necessary because
		 the event.target is null if using _.debounce directly and how
		 React treats synthetic event. So, using e.persist() the warning
		 regarding that treatment is cleared up and the event is passed
		 to the inner function.
	 */
	debouncedSearchClasses(...args) {
    const debouncedFn = _.debounce(...args);
		return function(e) {
      e.persist();
			return debouncedFn(e);
		};
	}

	compareNumbers = (name1, name2) => {
		let num1 = this.extractNumbersFromName(name1);
		let num2 = this.extractNumbersFromName(name2);
		if (!!num1 && !!num2) {
			if (num1 === num2) return 0;
			return (num1 < num2) ? -1 : 1;
		} else {
			return 0;
		}
	}

	extractNumbersFromName = (nameStringWithNumber) => {
		let numStr = nameStringWithNumber.replace(/[^0-9]/g, '');
		return (numStr) ? _.parseInt(numStr) : 0;
	}

	getColumnCompare = columnName => (
		columnName === 'name' ? this.compareNumbers : undefined
	);


	handleCheckboxAllChange = (onToggle, allSelected) => {
		if (allSelected) {
			this.props.setSelection([], true);
		} else {
			this.props.setSelection(this.getRowsData(this.props.searchResults, this.props._classes), true);
		}
		onToggle();
	}

	/* handleCheckboxChange = (changeSelected, row, selected, e) => {
		 this.setState({rowSelection: row});
		 changeSelected();
		 this.props.setSelection(row);
		 console.log("args: ", e, changeSelected, row, selected);
		 e.stopPropagation();
		 }
	 */
	render() {
		const { searchResults,
						_classes,
						deleteDispatched,
					} = this.props;

		// eslint-disable-next-line
		{ deleteDispatched ? this.clearSelectedRows() : null }

		return (
			 <MUIGrid container>
				 <MUIGrid item xs={12}>
					 <Paper>
						 <Grid
							 rows={this.getRowsData(searchResults, _classes)}
							 columns={[
								 { name: 'name', title: 'Nome da Turma' },
								 { name: 'students_count', title: 'Total de Alunos' },
								 { name: 'professors_count', title: 'Total de Professores' },
							 ]}>
							 <FilteringState
								 defaultFilters={[]}
								 filters={this.state.filters}
								 onFiltersChange={this.changeFilters}
							 />
							 <IntegratedFiltering />
							 <SelectionState
								 selection={this.state.selection}
								 onSelectionChange={this.changeSelection}
							 />
							 <IntegratedSelection />
							 <SortingState
								 sorting={this.state.sorting}
								 onSortingChange={this.changeSorting}
							 />
							 <IntegratedSorting
								 columnExtensions={this.integratedSortingColumnExtensions}
							 />
							 <VirtualTable
								 rowComponent={this.RowComponent}
								 cellComponent={Cell}
								 messages={{ noData: this.state.noDataMessage }}
							 />
							 {/* <TableColumnResizing
									 columnWidths={this.state.columnWidths}
									 onColumnWidthsChange={this.changeColumnWidths}
									 /> */}
							 <TableHeaderRow
								 rowComponent={this.headerRowTemplate}
								 allowSorting
								 showSortingControls
								 messages={{sortingHint:"Ordenar coluna"}}
							 />
							 <TableFilterRow
							 //filterCellTemplate={(props) => <FilterCell {...props} />}
								 filterCellTemplate={this.selectFilterEditor}
								 messages={{ filterPlaceholder: 'Filtrar...' }}
							 />

							 {/* Somehow using TableSelection with selectByRowClick props causes the VirtualTable
									 not loading more rows at scrolling. The fix here is just don't use it, or override the default
									 'tableViewRow' template passing the params accord.
								 */}
							 <TableSelection
								 selectByRowClick
								 showSelectAll
								 cellComponent={
									 ({row, selected, onToggle }) => {
										 return (
											 <td style={{padding: 0}}>
												 <Checkbox
												 checked={!!selected}
												 //onChange={this.handleCheckboxChange.bind(this, changeSelected, row, selected)}
												 />
											 </td>
										 )
									 }
								 }
								 headerCellComponent={
									 ({selectionAvailable, allSelected, someSelected, onToggle}) => {
										 return (
											 <td>
												 <Checkbox
												 checked={!!allSelected}
												 indeterminate={!!someSelected}
												 onChange={this.handleCheckboxAllChange.bind(null, onToggle, allSelected)}
												 />
											 </td>
										 )
									 }
								 }
							 />

							 {/* override default 'tableViewRow' template */}
							 <Template
								 name="tableRow"
								 // name="tableViewRow"
								 // use custom template only for table data rows
								 predicate={({ tableRow }) => tableRow.type === 'data'}
							 >
								 {params => (
									 <TemplateConnector>
										 {({ selection }, { toggleSelection }) => (
											 <this.RowComponent
											 {...params}
											 selected={selection.has(params.tableRow.rowId)}
											 onToggle={() => toggleSelection({ rowIds: [params.tableRow.rowId] })}
											 />
										 )}
									 </TemplateConnector>
								 )}
			         </Template>
						 </Grid>
					 </Paper>
				</MUIGrid>
			 </MUIGrid>
		);
	}
}

const mapStateToProps = (state) => (
	 {
		 /* currentPage: state.classes.classesList.classes.page_number,*/
     totalPages: state.classes.classesList.classes ? state.classes.classesList.classes.total_pages : 1
   }
);

const mapDispatchToProps = (dispatch) => ({
  dispatch,
	// searchClasses: (term) => dispatch(ClassesActions.tableSearchClasses(term)),
});

const wrapper = compose(
	 connect(mapStateToProps, mapDispatchToProps),
   withStyles(styles, {withTheme: true}),
);

export default wrapper( ClassesTable );
