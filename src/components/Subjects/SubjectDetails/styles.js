const styles = theme => ({
  infoName: {
		display: 'block',
	},
	infoOptional: {
		display: 'block',
	},
	content: {
    display: 'inline-flex',
		alignItems: 'baseline',
		'& *': {
			fontSize: '1rem',
		},
	},
});

export default styles;
