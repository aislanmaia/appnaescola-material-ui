import React, { Component } from 'react';
import { compose }					from 'redux';
import { withStyles }       from 'material-ui/styles';
import Button               from 'material-ui/Button';
import { Field,
         }           from 'redux-form'; // imported Field
import { Row, Col }         from 'react-flexbox-grid';
import styles								from './styles.js';
import EditIcon							from 'material-ui-icons/Edit';
import CheckIcon						from 'material-ui-icons/Check';
import ErrorIcon						from 'material-ui-icons/Error';
import { FormControlLabel } from 'material-ui/Form';
import Tooltip              from 'material-ui/Tooltip';
import {
  Checkbox,
  TextField,
} from 'redux-form-material-ui';


class SubjectForm extends Component {

  state =
  { unsaved: true
  , name: ''
  , optional: false
  };

	/* dispatchClickSave() {
		 const { requestedSaveForm} = this.props;
		 const buttonSave = this.button;
		 requestedSaveForm();
		 buttonSave.click();
		 }
	 */
  handleClassesInput = (field) => {
    const { classes } = this.props;
    return (
       field.meta.touched
				? (!field.meta.error && field.meta.valid)
				? {underline: classes.inputUnderline}
       : {}
       : {}
    )
  }

  handleEndAdornment = (field) => {
    const { classes } = this.props;
    return (
			 field.meta.touched
        ? (!field.meta.error && field.meta.valid
           ? <CheckIcon className={classes.validIconStatus} />
           : <ErrorIcon className={classes.invalidIconStatus} />)
       : (!field.meta.error && field.meta.valid
          ? <CheckIcon className={classes.validIconStatus} /> : '')
    )
  }

  componentWillUnmount() {
    const { resetActiveSubject, resetNewSubject } = this.props;
    resetNewSubject();
    resetActiveSubject();
  }


  renderNameField = (field) => {
		const { classes } = this.props;
    return (
			 <div className={classes.containerField}>
				 <EditIcon className={classes.fieldIcon}/>
				 <TextField
					 type="text"
					 style={{textIndent: 0}}
					 label="Nome"
					 placeholder="Digite o nome da matéria"
					 className={classes.textField}
					 inputClassName={classes.input}
					 InputProps={{
						 endAdornment: this.handleEndAdornment(field),
						 classes: this.handleClassesInput(field)
					 }}
					 //autoFocus={true}
					 error={field.meta.touched && field.meta.error ? !!field.meta.error : false}
					 helperText={field.meta.touched && field.meta.error ? field.meta.error : ''}
					 {...field.input}
					 />
			 </div>
		)
	}

	renderCheckOptionalField = (field) => {
    // return (
    //   <Checkbox
    //     checked={this.state.optional}
    //     label="É opcional?"
    //     onChange={this._handleChange.bind(this, 'optional')}
    //     theme={formTheme}
    //     {...field.input}
    //   />
    // )
  }

  render() {
    const {
			classes,
			initialValues,
			handleSubmit,
			subjectId,
      /* requestSave,*/
      /* requestedSaveForm,*/
		} = this.props;

		console.log("subjectId", subjectId);

		// eslint-disable-next-line
		/* {requestSave ? this.dispatchClickSave() : null}
		 */
    return (
      <div className="subjects-form">

        <form ref="form" onSubmit={
          subjectId ?
								handleSubmit(this.props.updateSubject.bind(this, initialValues))
							: handleSubmit(this.props.createSubject.bind(this))
				}
        >
          <div className="subject-inputs-section">
            <Row className={classes.nameSection}>
							<Col xs={12} md={12} className={classes.fields} >
								<Field
									name="name"
									component={this.renderNameField}
									type="text"
									/>
							</Col>
							<div className={classes.fields}>
								<Col xs={12} md={12} className={classes.checkbox}>
									{ /*<Field
											 name="optional"
											 component={this.renderCheckOptionalField}
											 type="checkbox"
											 />*/ }
									<FormControlLabel  control={<Field name="optional" component={Checkbox} /> } label="É opcional?" />
								</Col>
							</div>
						</Row>
						<div className={classes.floatingActions}>
							<Tooltip
								id="tooltip-icon-save" title="Salvar dados" placement="bottom"
								classes={{
									tooltipBottom: classes.tooltip,
								}}
							>
								<Button fab rootRef={el => this.button = el} color="primary" aria-label="save" type="submit">
									<CheckIcon />
								</Button>
							</Tooltip>
						</div>
          </div>
					{/* </BoxCard>
						*/}
        <div className={styles.footerActionsSubjectNew}>
          {/* <div styleName="actionItem">
							<TooltipButton
              tooltipPosition="left"
              type="button"
              styleName="backButton"
              icon={
              <FontAwesome name="arrow-left" />
              }
              tooltip='Voltar para tela anterior'
              //href="#"
              onMouseUp={::this._handleGoBackClick}
              floating mini />
							</div>
							<div styleName="actionItem">
							<TooltipButton
              tooltipPosition="left"
              className={formTheme.buttonSave}
              icon={
              <FontAwesome name="check" />
              }
              //onMouseUp={::this._handleSaveSubjectClick}
              tooltip='Salvar Matéria'
              floating mini
              type="submit"
              //disabled={submitting}
							/>
							</div> */}
        </div>
        </form>
      </div>
    );
  }

}

const wrapper = compose(
  withStyles(styles, {withTheme: true}),
);

export default wrapper( SubjectForm );
