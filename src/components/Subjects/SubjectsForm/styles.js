import { form, floatingActions } from 'styles/settings';

const styles = theme => (
   Object.assign({}, form(theme), {
     checkbox: {
       display: 'flex',
			 alignSelf: 'end',

			 '& p': {
         fontSize: '1.6rem',
			 }
		 },
		 actions: {
       paddingTop: '2rem'
		 },
		 tooltip: {
			 fontSize: '1rem',
			 padding: '.5rem',
		 },
		 nameSection: {
			 paddingBottom: '4rem',
		 },
		 fields: {
       paddingBottom: '2rem',
		 },
   }, floatingActions)
);

export default styles;
