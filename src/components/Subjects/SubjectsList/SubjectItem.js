import React														from 'react';
import { withStyles }										from 'material-ui/styles';
import Card, { CardContent, CardMedia } from 'material-ui/Card';
import IconButton                       from 'material-ui/IconButton';
import EditIcon                         from 'material-ui-icons/Edit';
import ShowDetailsIcon                  from 'material-ui-icons/RemoveRedEye';
import DeleteIcon                       from 'material-ui-icons/Delete';
import Typography												from 'material-ui/Typography';
import Tooltip                          from 'material-ui/Tooltip';
import { Row, Col }     								from 'react-flexbox-grid';
import styles														from './styles';

const SubjectItem = (props) => {
	const { classes, subject } = props;

	const isOptional = (optional) => {
    if (optional) return "Opcional";
		return "Obrigatória";
	}

	return(
		 <div className={ classes.subjectItem }>
			 <Card className={classes.card}>
				 <div className={classes.cardDetails}>
					 <CardContent className={classes.cardContent}>
						 <Typography type="headline">{subject.name}</Typography>
						 <Typography type="subheading" color="secondary">
							 {isOptional( subject.optional )}
						 </Typography>
					 </CardContent>
					 <div className={classes.actions}>
						 <Tooltip
						   id="tooltip-icon-show" title="Exibir detalhes" placement="bottom"
							 classes={{
								 tooltipBottom: classes.tooltip,
							 }}
						  >
							 <IconButton aria-label="Mostrar" onClick={props.handleShowDetailsClick.bind(null, subject)}>
								 <ShowDetailsIcon  />
							 </IconButton>
						 </Tooltip>
						 <Tooltip
							 id="tooltip-icon-edit" title="Editar matéria" placement="bottom"
							 classes={{
								 tooltipBottom: classes.tooltip,
							 }}
						 >
							 <IconButton aria-label="Editar" onClick={props.handleEditSubjectClick.bind(null, subject)}>
								 <EditIcon />
							 </IconButton>
						 </Tooltip>
						 <Tooltip
							 id="tooltip-icon-show" title="Remover matéria" placement="bottom"
							 classes={{
								 tooltipBottom: classes.tooltip,
							 }}
							 >
							 <IconButton aria-label="Remover" onClick={props.handleToggleDeleteDialog.bind(null, subject)}>
								 <DeleteIcon />
							 </IconButton>
						 </Tooltip>
					 </div>
				 </div>
				 <Row center="xs" middle="xs">
				 <Col xs md lg>
					 <CardMedia
						 className={classes.cardImage}
						 image="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA0NjAgNDYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0NjAgNDYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPHBvbHlnb24gc3R5bGU9ImZpbGw6I0M4NTIzQjsiIHBvaW50cz0iNzkuMywxNDIuNDA5IDc2LjYyNSwxNzguNzI0IDEyOS4xNDgsMTc4LjcyNCAxMzEuODIzLDE0Mi40MDkgIi8+Cjxwb2x5Z29uIHN0eWxlPSJmaWxsOiNFNUM0MDA7IiBwb2ludHM9IjY0LjA2NCwzNTAuNjg2IDExNi40NjIsMzUyLjQzMSAxMTcuMjU4LDM0MS40NDUgNjQuODYsMzM5LjY5NyAiLz4KPHBvbHlnb24gc3R5bGU9ImZpbGw6I0M4NTIzQjsiIHBvaW50cz0iNjIuMjExLDM3Ni40NDEgMTE0LjczNCwzNzYuNDQxIDExNi40NjIsMzUyLjQ2IDY0LjA2NCwzNTAuNzMyICIvPgo8cG9seWdvbiBzdHlsZT0iZmlsbDojQzg1MjNCOyIgcG9pbnRzPSI2NC44NiwzMzkuNjk3IDExNy4yNTgsMzQxLjQ0NSAxMjcuMDk3LDIwMi45MyA3NC41NzQsMjAyLjkzICIvPgo8cG9seWdvbiBzdHlsZT0iZmlsbDojRTVDNDAwOyIgcG9pbnRzPSI3NC41NzQsMjAyLjkzIDEyNy4wOTcsMjAyLjkzIDEyOS4xNDgsMTc4LjcyNCA3Ni42MjUsMTc4LjcyNCAiLz4KPHJlY3QgeD0iMTMzLjE2MyIgeT0iNDUuNTciIHN0eWxlPSJmaWxsOiNFNUM0MDA7IiB3aWR0aD0iNzIuNjM1IiBoZWlnaHQ9IjQwLjM1MyIvPgo8cmVjdCB4PSIxMzMuMTYzIiB5PSIxNTguNTUxIiBzdHlsZT0iZmlsbDojRTVDNDAwOyIgd2lkdGg9IjcyLjYzNSIgaGVpZ2h0PSIyMTcuODkiLz4KPHJlY3QgeD0iMTMzLjE2MyIgeT0iODUuOTIzIiBzdHlsZT0iZmlsbDojNTk1OTVBOyIgd2lkdGg9IjcyLjYzNSIgaGVpZ2h0PSI3Mi42MjkiLz4KPHJlY3QgeD0iMjEzLjg1NyIgeT0iMTk0Ljg2OSIgc3R5bGU9ImZpbGw6IzYwOEE3QzsiIHdpZHRoPSI1Mi40NjMiIGhlaWdodD0iNDQuMzc5Ii8+CjxyZWN0IHg9IjIxMy44NTciIHk9IjIzOS4yNDgiIHN0eWxlPSJmaWxsOiNBQkNCNTc7IiB3aWR0aD0iNTIuNDYzIiBoZWlnaHQ9Ijg0LjczMyIvPgo8cmVjdCB4PSIyMTMuODU3IiB5PSIzNDAuMTI3IiBzdHlsZT0iZmlsbDojQUJDQjU3OyIgd2lkdGg9IjUyLjQ2MyIgaGVpZ2h0PSIzNi4zMTQiLz4KPHJlY3QgeD0iMjEzLjg1NyIgeT0iMzIzLjk4MSIgc3R5bGU9ImZpbGw6I0U1QzQwMDsiIHdpZHRoPSI1Mi40NjMiIGhlaWdodD0iMTYuMTQ2Ii8+CjxyZWN0IHg9IjIxMy44NTciIHk9IjEzNC4zMzQiIHN0eWxlPSJmaWxsOiNBQkNCNTc7IiB3aWR0aD0iNTIuNDYzIiBoZWlnaHQ9IjYwLjUzNSIvPgo8cmVjdCB4PSIyMTMuODU3IiB5PSI4MS44ODciIHN0eWxlPSJmaWxsOiNBQkNCNTc7IiB3aWR0aD0iNTIuNDYzIiBoZWlnaHQ9IjQwLjM0NyIvPgo8cmVjdCB4PSIyMTMuODU3IiB5PSIxMjIuMjM0IiBzdHlsZT0iZmlsbDojNjA4QTdDOyIgd2lkdGg9IjUyLjQ2MyIgaGVpZ2h0PSIxMi4xIi8+Cjxwb2x5Z29uIHN0eWxlPSJmaWxsOiNBQkNCNTc7IiBwb2ludHM9IjI5My43MzYsNzIuNzUxIDI2OC40MTEsNzQuNTEzIDI2OS41MDIsODguMjc2IDI5NC44MTQsODYuNTA4ICIvPgo8cG9seWdvbiBzdHlsZT0iZmlsbDojNjA4QTdDOyIgcG9pbnRzPSIyOTIuMjgzLDM3Ni40NDEgMzE3Ljc0LDM3Ni40NDEgMzE0Ljk2NiwzNDEuMzk5IDI4OS43NTksMzQ0LjY1MiAiLz4KPHBvbHlnb24gc3R5bGU9ImZpbGw6IzYwOEE3QzsiIHBvaW50cz0iMjkxLjUxMiw0My41NTkgMjY2LjIwMiw0NS41NyAyNjguNDExLDc0LjUxMyAyOTMuNzM2LDcyLjc1MSAiLz4KPHBvbHlnb24gc3R5bGU9ImZpbGw6IzYwOEE3QzsiIHBvaW50cz0iMjk0LjgxNCw4Ni41MDggMjY5LjUwMiw4OC4yNzYgMjg3LjkzOCwzMjEuNTIzIDMxMy4xNDUsMzE4LjI4OSAiLz4KPHBvbHlnb24gc3R5bGU9ImZpbGw6I0FCQ0I1NzsiIHBvaW50cz0iMjg3LjkzOCwzMjEuNTIzIDI4OS43NTksMzQ0LjYwMyAzMTQuOTY2LDM0MS4zNjkgMzEzLjE0NSwzMTguMjg5ICIvPgo8cmVjdCB4PSIzMjIuOCIgeT0iMzIzLjk4MSIgc3R5bGU9ImZpbGw6I0M4NTIzQjsiIHdpZHRoPSI3Mi42MzUiIGhlaWdodD0iNTIuNDYiLz4KPHJlY3QgeD0iMzIyLjgiIHk9IjEwNi4wOTIiIHN0eWxlPSJmaWxsOiNDODUyM0I7IiB3aWR0aD0iNzIuNjM1IiBoZWlnaHQ9IjE1Ny4zNzEiLz4KPHJlY3QgeD0iMzIyLjgiIHk9IjI2My40NjIiIHN0eWxlPSJmaWxsOiNFNUM0MDA7IiB3aWR0aD0iNzIuNjM1IiBoZWlnaHQ9IjYwLjUxOSIvPgo8cmVjdCB5PSIzNzYuNDQxIiBzdHlsZT0iZmlsbDojQjlCQkJDOyIgd2lkdGg9IjQ2MCIgaGVpZ2h0PSI0MCIvPgo8cmVjdCB5PSIzOTYuNDQxIiBzdHlsZT0ib3BhY2l0eTowLjM7ZmlsbDojNUI1QjVGO2VuYWJsZS1iYWNrZ3JvdW5kOm5ldyAgICA7IiB3aWR0aD0iNDYwIiBoZWlnaHQ9IjIwIi8+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo="
						 />
				 </Col>
				 </Row>
			 </Card>
		 </div>
	);
};

export default withStyles(styles, { withTheme: true })(SubjectItem);
