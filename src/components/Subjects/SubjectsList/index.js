import React														from 'react';
// import { connect }										from 'react-redux';
import { withStyles }										from 'material-ui/styles';
import SubjectItem										from './SubjectItem';
// import ActionsSubject								from 'actions/async/subjects';
import Typography												from 'material-ui/Typography';
import Button														from 'material-ui/Button';
import styles														from './styles';
import classNames												from 'classnames';
import { Row, Col }     								from 'react-flexbox-grid';
/* import Loading													from 'components/Spinner';*/
import _                                from 'lodash';

const SubjectsList = (props) => {

  const {subjects, dispatch, /* loading,*/ classes} = props;
  const {
		handleShowDetailsClick,
    handleEditSubjectClick,
    handleRemoveSubjectClick,
    handleToggleDeleteDialog,
  } = props;

  const _renderActions = (subject) => {
    return (
      [
        <div key={subject.id} className={classes.iconActions}>
        </div>
      ]
    )
  }


  const _renderItems = (subjects) => {
		console.log("subjects in _renderItems()", subjects);
    if (subjects && !_.isEmpty( subjects )) {
      return subjects.map( (subject) => {
				console.log("Calling in subjects.map with", subject);
        return (
					<Col key={subject.id} xs sm md>
						 <Row center="xs">
							 <SubjectItem
								 subject={subject}
								 dispatch={dispatch}
								 removeSubject={handleRemoveSubjectClick}
								 renderActions={_renderActions}
					       handleShowDetailsClick={handleShowDetailsClick}
								 handleToggleDeleteDialog={handleToggleDeleteDialog}
								 handleEditSubjectClick={handleEditSubjectClick}
								/>
						 </Row>
					 </Col>
				);
      })
    } else {
      return (
			  <div className={classes.emptyContainer}>
					<div className={classes.emptyContent}>
						<Col xs sm md>
							<div className={classes.emptyWarning}>
								<div className={classNames(classes.icon, classes.iconEmptyState)}></div>
								<Typography type="body1" component="div">
									<h2 className={classes.emptyContentText}>
										Não existem <b>Matérias</b> cadastradas no momento.
									</h2>
								</Typography>
								<div className={classes.emptyContentAction}>
									<Button raised color="primary" aria-label="add" className={classes.emptyContentActionButton}>
										Nova Matéria
									</Button>
								</div>
							</div>
						</Col>
					</div>
        </div>
      )
    }
  };

  return (
    <Row center="xs">
      {/* { loading ?
					<Loading label="Carregando matérias cadastradas ..."/>
					: _renderItems(subjects) } */}
		{ _renderItems(subjects) }
    </Row>
  );
}

// const mapStateToProps = (state) => (
//   {
//     loading: state.subjects.subjectsList.loading
//   }
// );

// const mapDispatchToProps = (dispatch) => ({
//   dispatch,
// });

export default withStyles(styles, {withTheme: true})(SubjectsList);
