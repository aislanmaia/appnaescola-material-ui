const styles = theme => ({
  subjectItem: {
    paddingBottom: '2rem',

    '@media (min-width: 320px) and (max-width: 360px)': {
      //flex: 1,
    },
  },
  card: {
    display: 'flex',
    height: 'calc(100% - .3rem)',
  },
  cardDetails: {
    display: 'flex',
    flexDirection: 'column',
  },
  cardContent: {
    '& *': {
      display: 'box',
      display: '-webkit-box', // eslint-disable-line no-dupe-keys
    },
  },
  cardImage: {
    width: 151,
    height: 151,

    '@media (min-width: 320px) and (max-width: 350px)': {
      width: 74,
      height: 74,
    },

    '@media (device-width: 360px)': {
      width: 100,
      height: 120,
    }
  },
  actions: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },

  tooltip: {
    fontSize: '1rem',
    padding: '.5rem',
  },

	emptyContainer: {
    padding: '5rem 15rem 15rem 15rem',
    display: 'flex',
    alignItems: 'center',

    '@media (min-width: 320px) and (max-width: 460px)': {
      padding: '.5rem',
    },
    
	},

  emptyContent: {
    flex: 1,
  },
  
  emptyContentText: {
    paddingTop: '2.5rem',
    color: "rgba(0,0,0,.30)", 
    margin: 0, 
    fontStyle: "italic",
    fontWeight: "initial",
    
    '@media (min-width: 320px) and (max-width: 460px)': {
      paddingTop: '.5rem',
      fontSize: '1.2rem',
    },
  },
  
  emptyContentAction: {
     paddingTop: '.5rem',
  },
  
  emptyContentActionButton: {
    margin: theme.spacing.unit,
    padding: '10px 30px',
    fontSize: '1.4rem',
    
    '@media (min-width: 320px) and (max-width: 460px)': {
      padding: '.5rem',
      fontSize: '1.2rem',
    },
  },

	iconEmptyState: {
    background: `
    url('data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIuMDA4IDUxMi4wMDgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMi4wMDggNTEyLjAwODsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCI+CjxwYXRoIHN0eWxlPSJmaWxsOiM1RERERDM7IiBkPSJNMjU2LjAwNCwwLjAwOGMxNDEuMzg1LDAsMjU2LDExNC42MTUsMjU2LDI1NnMtMTE0LjYxNSwyNTYtMjU2LDI1NnMtMjU2LTExNC42MTUtMjU2LTI1NiAgUzExNC42MTksMC4wMDgsMjU2LjAwNCwwLjAwOHoiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0M4Q0FDQzsiIGQ9Ik0xNDQuMDAzLDQxMi4xMjJoNi41MjZjMzguMzY0LDAuMzkzLDc1LjM3NCw3LjcxOCwxMDQuMzAxLDI1LjI0NiAgYzI4Ljk4My0xNy41MjgsNjguMjg0LTI0Ljg0OSwxMDYuNjQ3LTI1LjI0NkgzNjhjMzMuMTc3LDAuMzIzLDY3LjYyMyw1LjYzLDk5LjkwNCwxMy44MzN2MTQuNzk0ICBjLTc3LjgwMy0xOS41MTktMTU5Ljk0OC0xOC44MDMtMTk1LjE0Myw0LjM2NWMtNi4yMDMsNy4yMDUtMTQuNjM3LDYuNzA2LTE3LjgzMyw1LjY2N2MtMi42MjgsMS4wMzUtOS40ODIsMS41MzgtMTUuNjg1LTUuNjY3ICBjLTM1LjE5NS0yMy4xNjgtMTE3LjM0LTIzLjg4NC0xOTUuMTQzLTQuMzY1di0xNC43OTRDNzYuMzgsNDE3Ljc1MiwxMTAuODI3LDQxMi40NSwxNDQuMDAzLDQxMi4xMjJ6Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiNGRkZGRkY7IiBkPSJNNDQuMTA0LDE0OS4wMjhjNzMuNjMyLTIyLjQ3LDE2OC4xNTYtMjIuMzIzLDIxMS45LDEyLjYxNCAgYzQzLjc0OS0zNC45MzYsMTM4LjI3Mi0zNS4wODQsMjExLjktMTIuNjE0djI3Ni45MjNjLTcyLjc0MS0xOC40OC0xNTYuNDg5LTIyLjI3Ni0yMTEuOSwxMi41NTQgIGMtNTUuNDA3LTM0LjgzLTEzOS4xNTQtMzEuMDM0LTIxMS45LTEyLjU1NFYxNDkuMDI4eiIvPgo8cGF0aCBzdHlsZT0iZmlsbDojRUNGMEYxOyIgZD0iTTI0MS42NDQsMTUyLjMwN2M0LjU4MiwyLjM0MiwxMC41MTcsNi4xOTQsMTQuMzU1LDkuMzM1YzQuMTQzLTMuMzA3LDguNzM5LTYuMywxMy43MjctOC45OTd2Mjc4LjI3NiAgYy00LjczOSwyLjI5MS05LjMyNSw0LjgxMy0xMy43MjcsNy41NzlsLTEzLjExMy03LjI4OGwtMS4yNDItMjc4LjkwOVYxNTIuMzA3eiIvPgo8cGF0aCBzdHlsZT0iZmlsbDojMjBEMEMyOyIgZD0iTTI1Ni4wMDQsMTg5Ljk1OWMtNDcuMjg3LTQ2LjY2My0xNTIuOTQxLTMyLjE3NC0yMTEuOS0xNy4yMTR2LTMuMjQyICBjNTkuMjYzLTE0LjkwNSwxNjIuODQ0LTI4LjcxNSwyMTEuOSwxNi41NThjNDkuMDU2LTQ1LjI3MywxNTIuNjM3LTMxLjQ2OCwyMTEuOS0xNi41NTh2My4yNDIgIEM0MDguOTQ1LDE1Ny43ODUsMzAzLjI5NSwxNDMuMjk2LDI1Ni4wMDQsMTg5Ljk1OXogTTI1Ni4wMDQsMjEwLjY1NkwyNTYuMDA0LDIxMC42NTZjNDcuMjkyLTQ2LjY1OSwxNTIuOTQxLTMyLjE3NCwyMTEuOS0xNy4yMTQgIHYtMy4yNDJjLTU5LjI2My0xNC45MDUtMTYyLjg0NC0yOC43MTUtMjExLjksMTYuNTYzYy00OS4wNTYtNDUuMjc4LTE1Mi42MzItMzEuNDY4LTIxMS45LTE2LjU2M3YzLjI0MiAgQzEwMy4wNjIsMTc4LjQ4MiwyMDguNzE3LDE2My45OTMsMjU2LjAwNCwyMTAuNjU2eiBNMjU2LjAwNCwyMzEuMzUzTDI1Ni4wMDQsMjMxLjM1M2M0Ny4yOTItNDYuNjU5LDE1Mi45NDEtMzIuMTc0LDIxMS45LTE3LjIxNCAgdi0zLjI0MmMtNTkuMjYzLTE0LjkwNS0xNjIuODQ0LTI4LjcxNS0yMTEuOSwxNi41NThjLTQ5LjA1Ni00NS4yNzgtMTUyLjYzMi0zMS40NjgtMjExLjktMTYuNTU4djMuMjQyICBDMTAzLjA2MiwxOTkuMTc4LDIwOC43MTcsMTg0LjY4OSwyNTYuMDA0LDIzMS4zNTN6IE0yNTYuMDA0LDI1Mi4wNDlMMjU2LjAwNCwyNTIuMDQ5Yy00Ny4yODctNDYuNjYzLTE1Mi45NDEtMzIuMTc0LTIxMS45LTE3LjIxNCAgdi0zLjI0MmM1OS4yNjMtMTQuOTA1LDE2Mi44NDQtMjguNzE1LDIxMS45LDE2LjU1OGM0OS4wNTYtNDUuMjczLDE1Mi42MzctMzEuNDY4LDIxMS45LTE2LjU1OHYzLjI0MiAgQzQwOC45NDUsMjE5Ljg3NSwzMDMuMjk1LDIwNS4zODYsMjU2LjAwNCwyNTIuMDQ5eiBNMjU2LjAwNCwyNzIuNzQ2TDI1Ni4wMDQsMjcyLjc0NmMtNDcuMjg3LTQ2LjY2My0xNTIuOTQxLTMyLjE3NC0yMTEuOS0xNy4yMTQgIHYtMy4yNDJjNTkuMjYzLTE0LjkwNSwxNjIuODQ0LTI4LjcxNSwyMTEuOSwxNi41NThjNDkuMDU2LTQ1LjI3OCwxNTIuNjM3LTMxLjQ2OCwyMTEuOS0xNi41NTh2My4yNDIgIEM0MDguOTQ1LDI0MC41NzIsMzAzLjI5NSwyMjYuMDgzLDI1Ni4wMDQsMjcyLjc0NnogTTI1Ni4wMDQsMjkzLjQ0M0wyNTYuMDA0LDI5My40NDNjNDcuMjkyLTQ2LjY1OSwxNTIuOTQxLTMyLjE3NCwyMTEuOS0xNy4yMTQgIHYtMy4yNDJjLTU5LjI2My0xNC45MDUtMTYyLjg0NC0yOC43MTUtMjExLjksMTYuNTU4Yy00OS4wNTYtNDUuMjc4LTE1Mi42MzItMzEuNDY4LTIxMS45LTE2LjU1OHYzLjI0MiAgQzEwMy4wNjIsMjYxLjI2OCwyMDguNzE3LDI0Ni43NzksMjU2LjAwNCwyOTMuNDQzeiBNMjU2LjAwNCwzMTQuMTRMMjU2LjAwNCwzMTQuMTRjNDcuMjkyLTQ2LjY1OSwxNTIuOTQxLTMyLjE3NCwyMTEuOS0xNy4yMTQgIHYtMy4yNDJjLTU5LjI2My0xNC45MDUtMTYyLjg0NC0yOC43MTUtMjExLjksMTYuNTU4Yy00OS4wNTYtNDUuMjc4LTE1Mi42MzItMzEuNDY4LTIxMS45LTE2LjU1OHYzLjI0MiAgQzEwMy4wNjIsMjgxLjk2NSwyMDguNzE3LDI2Ny40NzYsMjU2LjAwNCwzMTQuMTR6IE0yNTYuMDA0LDMzNC44MzZMMjU2LjAwNCwzMzQuODM2Yy00Ny4yODctNDYuNjYzLTE1Mi45NDEtMzIuMTc0LTIxMS45LTE3LjIxNCAgdi0zLjI0MmM1OS4yNjMtMTQuOTA1LDE2Mi44NDQtMjguNzE1LDIxMS45LDE2LjU1OGM0OS4wNTYtNDUuMjc4LDE1Mi42MzctMzEuNDY4LDIxMS45LTE2LjU1OHYzLjI0MiAgQzQwOC45NDUsMzAyLjY2MiwzMDMuMjk1LDI4OC4xNzMsMjU2LjAwNCwzMzQuODM2eiBNMjU2LjAwNCwzNTUuNTMzTDI1Ni4wMDQsMzU1LjUzM2MtNDcuMjg3LTQ2LjY1OS0xNTIuOTQxLTMyLjE3NC0yMTEuOS0xNy4yMTQgIHYtMy4yNDJjNTkuMjYzLTE0LjkwOSwxNjIuODQ0LTI4LjcxNSwyMTEuOSwxNi41NThjNDkuMDU2LTQ1LjI3OCwxNTIuNjM3LTMxLjQ2OCwyMTEuOS0xNi41NTh2My4yNDIgIEM0MDguOTQ1LDMyMy4zNTksMzAzLjI5NSwzMDguODc0LDI1Ni4wMDQsMzU1LjUzM3ogTTI1Ni4wMDQsMzc2LjIzTDI1Ni4wMDQsMzc2LjIzYzQ3LjI5Mi00Ni42NTksMTUyLjk0MS0zMi4xNzQsMjExLjktMTcuMjE0ICB2LTMuMjQyYy01OS4yNjMtMTQuOTA1LTE2Mi44NDQtMjguNzE1LTIxMS45LDE2LjU1OGMtNDkuMDU2LTQ1LjI3OC0xNTIuNjMyLTMxLjQ2OC0yMTEuOS0xNi41NTh2My4yNDIgIEMxMDMuMDYyLDM0NC4wNTUsMjA4LjcxNywzMjkuNTY2LDI1Ni4wMDQsMzc2LjIzeiBNMjU2LjAwNCwzOTYuOTI2TDI1Ni4wMDQsMzk2LjkyNmM0Ny4yOTItNDYuNjYzLDE1Mi45NDEtMzIuMTc0LDIxMS45LTE3LjIxNCAgdi0zLjI0MmMtNTkuMjYzLTE0LjkwNS0xNjIuODQ0LTI4LjcxNS0yMTEuOSwxNi41NThjLTQ5LjA1Ni00NS4yNzMtMTUyLjYzMi0zMS40NjgtMjExLjktMTYuNTU4djMuMjQyICBDMTAzLjA2MiwzNjQuNzUyLDIwOC43MTcsMzUwLjI2MywyNTYuMDA0LDM5Ni45MjZ6IE0yNTYuMDA0LDQxNy42MjNMMjU2LjAwNCw0MTcuNjIzYy00Ny4yODctNDYuNjYzLTE1Mi45NDEtMzIuMTc0LTIxMS45LTE3LjIxNCAgdi0zLjI0MmM1OS4yNjMtMTQuOTA1LDE2Mi44NDQtMjguNzE1LDIxMS45LDE2LjU1OGM0OS4wNTYtNDUuMjc4LDE1Mi42MzctMzEuNDY4LDIxMS45LTE2LjU1OHYzLjI0MiAgQzQwOC45NDUsMzg1LjQ0OSwzMDMuMjk1LDM3MC45NiwyNTYuMDA0LDQxNy42MjN6Ii8+CjxnPgoJPHBhdGggc3R5bGU9ImZpbGw6I0ZGNUI2MTsiIGQ9Ik05MS4zMjYsMTM4LjE3OGwzLjM4MS0wLjUzMXYyNzguMjk1bC0zLjM4MSwwLjQ5VjEzOC4xNzh6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojRkY1QjYxOyIgZD0iTTQyMC42ODEsMTM4LjE3OGwtMy4zODEtMC41MzF2Mjc4LjI5NWwzLjM4MSwwLjQ5VjEzOC4xNzh6Ii8+CjwvZz4KPGc+Cgk8cGF0aCBzdHlsZT0iZmlsbDojQzhDQUNDOyIgZD0iTTI5Ni40MzcsMTQuNTAxbDExNS44NzEsNTUuODg3YzYuMTI5LDQuNjcsNC4zNTEsMzMuNTI4LDMuNjA3LDM4Ljg5OSAgIGMtMC43NDgsNS4zNzItNC40MDIsMy40OTItOC40MjktMC42OTdjLTQuMDI4LTQuMTg5LDguNTY4LTI4LjU3Ni0xLjcwOS0zMy4wMzNMMjc3Ljc0OSwyMC4wMjEgICBjMS4yNTItMi4yODYsMy4xNzMtNC42MzMsNS40MDktNS4zNjJjMy4yMTUtMS4wNDgsNi44NS0xLjMxNiwxMy4yNzQtMC4xNTdIMjk2LjQzN3oiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiNDOENBQ0M7IiBkPSJNNDUwLjA4NSw2Ny4wMDNMMzI0LjI0Miw0MC4yOTdjLTcuNzA0LTAuMDU1LTIzLjk1OCwyMy44NTEtMjYuNjYsMjguNTU4ICAgYy0yLjY5Nyw0LjcwNywxLjM0NCw1LjQ1NSw3LjA5NCw0LjYwNWM1Ljc0Ni0wLjg0NSwxMC43MTEtMjcuODQyLDIxLjU2NS0yNS4wOGwxMzUuMjQyLDM0LjQxOSAgIGMwLjQwNi0yLjU3MywwLjMyMy01LjYwMy0wLjk5OC03LjU1MkM0NTguNTgzLDcyLjQ1Myw0NTUuODc3LDcwLjAxNCw0NTAuMDg1LDY3LjAwM3oiLz4KPC9nPgo8cGF0aCBzdHlsZT0iZmlsbDojRkZGRkZGOyIgZD0iTTQ1OS4zMTgsODkuMTM2Yy0xLjI3NSwyLjI4Ni0yLjUzMSw0LjU4Ni0zLjc4Nyw2Ljg0Yy0yLjM0Miw0LjIxMi00LjY3NCw4LjQzNC02Ljk5NywxMi42NTUgIGMtOS42NTMsMTcuNTYxLTQzLjk0OCwxMC4zMDQtNTYuNTM0LTAuMTA2Yy0wLjU0NS0wLjQ1My0xLjA1OC0wLjk0Ny0xLjUzOC0xLjQ3M2MtMy4xMTMtMy40MDktNC44OTYtOC4yMjYtNS45NTgtMTIuNjQ2ICBjLTEuMjg5LTUuMzUzLTEuNzU1LTEwLjk5Ny0xLjg5OC0xNi40OThjLTAuMDg4LTMuNDIzLDAuMTQzLTcuMjQ3LTAuMDQyLTEwLjY3OWMwLjEyLTAuNTQsMC4yNTQtMS4wNjcsMC4zOTMtMS41NzVsMjIuODMxLDkuOTAzICBjMTAuMjcyLDQuNDU3LTIuMzE5LDI4Ljg0NCwxLjcwOSwzMy4wMzNjNC4wMjgsNC4xODksNy42ODEsNi4wNjksOC40MjUsMC42OTdjMC43NDgtNS4zNzIsMi41MjItMzQuMjI1LTMuNjA3LTM4Ljg5OWwtMC40NTMtMC4yMTcgIGw0OS42MjksMTIuNjMyYy0wLjMxOSwyLjEwMi0xLjAzOSw0LjMwOS0yLjE2Miw2LjMzN0w0NTkuMzE4LDg5LjEzNnogTTM4OS4zOTksNTQuMTIxTDM4OS4zOTksNTQuMTIxbDYwLjY4MSwxMi44NzcgIGMtMTYuMTg5LTguNDI1LTQ1LjI1NS0xNC43OTktNTcuMTAyLTE1LjYwN2MtMS4xNjksMC42ODgtMi4zODgsMS41NzUtMy41OCwyLjcyNVY1NC4xMjF6IE0yNzUuNTg3LDI2LjM1M0wyNzUuNTg3LDI2LjM1MyAgYy0wLjM4OCwyLjU4Ny0wLjgwNCw1LjE3OC0xLjE5Miw3LjczMmMtMC43MjUsNC43NjItMS40Niw5LjUyOS0yLjIxMiwxNC4yOWMtMy4xMDgsMTkuNzkxLDI4LjQ1NiwzNS4wNDMsNDQuNzc5LDM0LjUwNyAgYzAuNzExLTAuMDIzLDEuNDE4LTAuMTAyLDIuMTItMC4yMjJjNC41NDUtMC43OSw4LjkxLTMuNTEsMTIuNDU3LTYuMzZjNC4yOTUtMy40NDYsOC4xMi03LjYyMSwxMS41OTgtMTEuODg0ICBjMi4xNjItMi42NTYsNC4zMjMtNS44MTUsNi41NjMtOC40MmMwLjIzNi0wLjUwMywwLjQ1My0wLjk5OCwwLjY1MS0xLjQ4M2wtMjQuMTE1LTYuMTM4Yy0xMC44NTQtMi43NjItMTUuODE1LDI0LjIzNS0yMS41NjUsMjUuMDggIGMtNS43NDYsMC44NDUtOS43OTIsMC4wOTctNy4wOS00LjYwNWMyLjY5Ny00LjcwMiwxOC45NTEtMjguNjEzLDI2LjY2LTI4LjU1OGwwLjQ5NCwwLjEwNmwtNDYuOTgyLTIwLjM3OCAgQzI3Ni43MjQsMjEuODc4LDI3NS45MzgsMjQuMDYyLDI3NS41ODcsMjYuMzUzeiBNMzUyLjMxNCw0MS40NDdMMzUyLjMxNCw0MS40NDdsLTU1Ljg3OC0yNi45NTEgIGMxNy45NjIsMy4yNDIsNDQuODQ4LDE1Ljk5NSw1NC43MTQsMjIuNkMzNTEuNjU0LDM4LjM1MywzNTIuMDc0LDM5LjgwOCwzNTIuMzE0LDQxLjQ0N3oiLz4KPHBhdGggc3R5bGU9ImZpbGw6IzBFNzg4NjsiIGQ9Ik0yNzUuODUxLDEuMjQxYzE0LjA2LDAuMyw3Mi42MjYsMTkuMDM0LDc4LjYwNywyNS43MjdjMC42NjUsMC43NDQsMTMuODg0LDkuNDk2LDEzLjg3LDkuNTI0ICBsNS41NzksMi4zNjVsNS44NjEsMS41NDdjMC4wMDktMC4wMjgsMTUuODE5LDEuMTM2LDE2LjgwMywwLjk1MWM4Ljc5OS0xLjYzLDY2LjI1MiwxOS4yNTYsNzcuODA4LDI3LjY3NiAgYzMuMjMzLDEuMjg0LDYuNTMxLDIuNzQ4LDYuNDQ4LDMuMzc2YzAuMzg4LDEuNjAzLTQuMDM3LDExLjQzMS00LjQ4NSwxMS43NTVjLTAuMzUxLDAuMTI1LTEuOTQ5LDAuMTExLTQuNDItMC4wMDUgIGMtMC41NCwwLjQ4LTEuMTA5LDEuMTU1LTEuNjY3LDIuMDY5Yy0yLjMyMywzLjgxLTExLjU2MSwyMC41NC0xNC43MDYsMjYuMjYyYy0xMi4zOTIsMjIuNTQtNTIuMTE4LDE1Ljg4NC02OC42NDksMi4xOTkgIGMtMTQuOTIzLTEyLjM1MS0xMS44ODktNDIuMzYzLTEyLjQwMS00OC4wOTVjLTAuMTAyLTEuMTI3LDAuMDQ2LTIuODMxLDAuMzQyLTQuODRjLTIuMjAzLTIuNTUtNS4wMjUtNS4yOTMtNi45NTYtNS4yNyAgYy0xLjUxLTEuMjAxLTUuNDIyLTAuNzU3LTguNzI1LTAuMDg4Yy0wLjk5MywxLjc2OS0xLjkyMSwzLjIwNS0yLjY5Myw0LjAzN2MtMy45MDcsNC4yMjItMTkuODc1LDI5LjgxNC0zOS4yMzYsMzAuNDQ3ICBjLTIxLjQ1LDAuNzA3LTU2Ljk0NS0xOC4zMzctNTIuOTUtNDMuNzQ5YzEuMDEyLTYuNDUyLDMuOTQ0LTI1LjMzNCw0LjQzNC0yOS43NjhjMC4xMjUtMS4xMTgsMC4xMjktMi4wNTEsMC4wNTUtMi44MTMgIGMtMS43MTgtMS4yMTktMi43OS0yLjA0Ni0yLjk3NC0yLjMzMmMtMC4xNTItMC41MzEsMi4zNi0xMS4wMTEsMy42NDQtMTIuMDQ2YzAuMjk2LTAuNTAzLDMuMzEyLDAuMTk0LDYuNDA2LDEuMDYyTDI3NS44NTEsMS4yNDF6ICAgTTI3NS41ODcsMjYuMzU4TDI3NS41ODcsMjYuMzU4Yy0wLjM4OCwyLjU5MS0wLjc5OSw1LjE3OC0xLjE5Miw3LjczMmMtMC43MjUsNC43NjItMS40Niw5LjUyOS0yLjIwOCwxNC4yOSAgYy0zLjExMywxOS43OTYsMjguNDU2LDM1LjA0Nyw0NC43NzksMzQuNTExYzAuNzExLTAuMDIzLDEuNDE4LTAuMDk3LDIuMTItMC4yMjJjNC41NDUtMC43OSw4LjkxLTMuNTEsMTIuNDU3LTYuMzU1ICBjNC4yOTEtMy40NDYsOC4xMi03LjYyNiwxMS41OTMtMTEuODg5YzIuMTY2LTIuNjU2LDQuMzIzLTUuODE1LDYuNTY4LTguNDJjMy45MDMtOC4zNTEsMy4xMjctMTQuNjk3LDEuNDQ2LTE4Ljg5NSAgYy0xMy4yMTktOC44NTktNTcuMDA5LTI4Ljc0My02OC45MTctMjEuOTk5Yy0zLjAyNSwxLjcxNC01Ljg3NSw2LjIxMi02LjY0NiwxMS4yNTFWMjYuMzU4eiBNNDU5LjMyMiw4OS4xNDFMNDU5LjMyMiw4OS4xNDEgIGMyLjQ3MS00LjQ1NywyLjk3NC05Ljc1OSwxLjYzLTEyLjk2NWMtNS4yOTMtMTIuNjE4LTUyLjA5NS0yMy42OS02Ny45Ny0yNC43NzVjLTMuODk4LDIuMjk2LTguMzk3LDYuODM2LTEwLjQyLDE1LjgyOSAgYzAuMTgsMy40MzYtMC4wNDYsNy4yNTYsMC4wNDIsMTAuNjc5YzAuMTQzLDUuNTAxLDAuNjEsMTEuMTQ1LDEuODk4LDE2LjQ5OGMxLjA2Miw0LjQyLDIuODUsOS4yNDIsNS45NjMsMTIuNjUxICBjMC40OCwwLjUyNywwLjk5MywxLjAyMSwxLjU0MywxLjQ3M2MxMi41ODYsMTAuNDE1LDQ2Ljg4MSwxNy42NjcsNTYuNTM0LDAuMTA2YzIuMzIzLTQuMjIyLDQuNjU2LTguNDQzLDYuOTk3LTEyLjY1NSAgYzEuMjU2LTIuMjU5LDIuNTEzLTQuNTU5LDMuNzg3LTYuODRINDU5LjMyMnoiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0ZFRDE5ODsiIGQ9Ik0yMTcuMzE3LDI1Mi40MWwtNDAuMjUzLTIxLjY4MWwxOC41ODYtMTguNTkxbDIxLjY2Miw0MC4yNzFIMjE3LjMxN3oiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0ZGNUI2MTsiIGQ9Ik0xODIuNTA1LDIyNS4yODhjNC42NDItMC40MjUsNy4zOTktMi44MDQsNy43MTMtNy43MTNMNDQuODUyLDcyLjIwNGwtNy43MTMsNy43MThMMTgyLjUwNSwyMjUuMjg4eiIvPgo8cGF0aCBzdHlsZT0iZmlsbDojRUM0QjUzOyIgZD0iTTE4Mi41MDUsMjI1LjI4OGMwLjM5MywzLjcxMy0xLjIwMSw1LjcxOC01LjQzNiw1LjQzNkwzMS42OTgsODUuMzUzbDUuNDM2LTUuNDM2bDE0NS4zNjcsMTQ1LjM2NyAgTDE4Mi41MDUsMjI1LjI4OHoiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0VDRjBGMTsiIGQ9Ik01MC4yODQsNjYuNzY3TDMxLjY5OCw4NS4zNTNsLTQuNTkxLTQuNTkxbDE4LjU4Ni0xOC41ODZMNTAuMjg0LDY2Ljc2N3oiLz4KPHBhdGggc3R5bGU9ImZpbGw6IzIwRDBDMjsiIGQ9Ik0zMS45Miw1Ny41ODVsLTkuNDA0LDkuNDA0Yy0yLjUyNiwyLjUyNi0yLjUyNiw2LjY1NiwwLDkuMTgybDQuNTkxLDQuNTkxbDE4LjU4Ni0xOC41ODYgIGwtNC41OTEtNC41OTFDMzguNTc1LDU1LjA2MywzNC40NDYsNTUuMDU5LDMxLjkyLDU3LjU4NXoiLz4KPHBhdGggc3R5bGU9ImZpbGw6IzY2NjY2NjsiIGQ9Ik0yMTkuOTM2LDI1NC4yNjJsLTAuNzc2LDAuNzcxbC0xNi45MzctMTAuNzU3bDYuOTMzLTYuOTMzbDEwLjc4LDE2LjkxNFYyNTQuMjYyeiIvPgo8cGF0aCBzdHlsZT0iZmlsbDojRUM0QjUzOyIgZD0iTTE5MC4yMTksMjE3LjU3NWMzLjcxNCwwLjM5Myw1LjcxOC0xLjE5Niw1LjQzNi01LjQzNkw1MC4yODQsNjYuNzcybC01LjQzNiw1LjQzNkwxOTAuMjE0LDIxNy41OCAgTDE5MC4yMTksMjE3LjU3NXoiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==') 50% 50% no-repeat;
    `,
    backgroundSize: '100%',
    width: 256,
    height: 256,
    paddingBottom: 10,
    
    '@media (min-width: 320px) and (max-width: 360px)': {
      width: 124,
      height: 124,
    },
  },
  icon: { /* Customizes all icons at once */
    display: 'inline-block',
  }


});

export default styles;
