import { drawerWidth } from 'styles/settings';

const styles = theme => ({
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  flex: {
    flex: 1,
  },
  iconMenu: {
    cursor: 'pointer',
  },
	rightMenuDisabled: {
    pointerEvents: 'none',
	},
	popper: {
    top: '10px !important',
	},
	popperArrow: {
		borderWidth: '0 10px 10px 10px',
    borderColor: 'transparent transparent #fff transparent',
    top: '-10px',
    left: 'calc(50% - 10px)',
    marginTop: 0,
    marginBottom: 0,
		width: 0,
		height: 0,
		borderStyle: 'solid',
		position: 'absolute',
		margin: 5,
		filter: 'drop-shadow(0 -3px 1px rgba(0, 0, 0, 0.15))',
	},
});

export default styles;
