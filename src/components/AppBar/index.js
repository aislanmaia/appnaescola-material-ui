import React, { Component }         from 'react';
import PropTypes                    from 'prop-types';
import { withStyles }               from 'material-ui/styles';
import { withRouter }               from 'react-router';
import { connect }                  from 'react-redux';
import classNames                   from 'classnames';
import AppBarMui                    from 'material-ui/AppBar';
import Toolbar                      from 'material-ui/Toolbar';
import IconButton                   from 'material-ui/IconButton';
import Typography                   from 'material-ui/Typography';
import MoreVertIcon                 from 'material-ui-icons/MoreVert';
import MenuIcon                     from 'material-ui-icons/Menu';
import { MenuItem, MenuList }       from 'material-ui/Menu';
import Grow                         from 'material-ui/transitions/Grow';
import Paper                        from 'material-ui/Paper';
import { Manager, Target, Popper, Arrow }  from 'react-popper';
import ClickAwayListener            from 'material-ui/utils/ClickAwayListener';
import UserActions                  from 'actions/async/user';

import styles from './index.styles';

// const rightMenuDisabledStyle = {
//   pointerEvents: 'none',
// };

class AppBar extends Component {
  constructor(props) {
    super(props);

		this.handleClickLogout = this.handleClickLogout.bind(this);
		this.cx = classNames.bind(props.classes);
  }

	componentDidMount() {
    console.log("AppBar mounted");
	}

	handleClickLogout() {
		const { history: { push } } = this.props;
		this.props.handleRightMenuClose();
    this.props.dispatch(UserActions.logoutUser());
		push("/login");
	}

  render() {
    const {
      classes,
      openDrawer,
      openRightMenu,
      handleRightMenuOpen,
      handleRightMenuClose,
      handleDrawerOpen,
    } = this.props;

    return (
      <AppBarMui position="fixed" className={classNames(classes.appBar, openDrawer && classes.appBarShift)}>
        <Toolbar disableGutters={!openDrawer}>
          <IconButton
              //className={classes.menuButton}
              color="contrast"
              arial-label="open drawer"
              onClick={handleDrawerOpen}
              className={classNames(classes.menuButton, openDrawer && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography type="title" color="inherit" className={classes.flex} noWrap>
            App na Escola
          </Typography>
          <Manager>
            <Target>
              <MoreVertIcon
                  aria-label="Menu"
                  aria-owns={openRightMenu ? 'menu-list' : null}
                  aria-haspopup="true"
                  className={classes.iconMenu}
                  onClick={handleRightMenuOpen}
              />
            </Target>
            <Popper placement="bottom-end" eventsEnabled={openRightMenu} className={this.cx({ [classes.rightMenuDisabled]: !openRightMenu }, classes.popper)}>
              <ClickAwayListener onClickAway={handleRightMenuClose} >
                <Grow in={openRightMenu} id="menu-list" style={{ transformOrigin: '0 0 0' }}>
									<div>
										<Paper>
											<MenuList role="menu">
												<MenuItem onClick={handleRightMenuClose}>Profile</MenuItem>
												<MenuItem onClick={handleRightMenuClose}>My account</MenuItem>
												<MenuItem onClick={this.handleClickLogout}>Logout</MenuItem>
											</MenuList>
										</Paper>
										<Arrow>
											{({ arrowProps, restProps }) => (
												<span
													className={classes.popperArrow}
													{...arrowProps}
													/>
											)}
										</Arrow>
									</div>
								</Grow>
              </ClickAwayListener>
            </Popper>
          </Manager>

        </Toolbar>
      </AppBarMui>
    )
  }
};

AppBar.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  openDrawer: PropTypes.bool.isRequired,
  openRightMenu: PropTypes.bool.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

export default withRouter(connect(null, mapDispatchToProps)( withStyles(styles, { withTheme: true })(AppBar) ));
