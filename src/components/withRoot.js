/* eslint-disable flowtype/require-valid-file-annotation */

import React, { Component } from 'react';
import JssProvider from 'react-jss/lib/JssProvider';
import { withStyles, MuiThemeProvider } from 'material-ui/styles';
import wrapDisplayName from 'recompose/wrapDisplayName';
import createContext from 'styles/createContext';

// Apply some reset
const styles = theme => ({
  '@global': {
    html: {
      background: theme.palette.background.default,
      WebkitFontSmoothing: 'antialiased', // Antialiasing.
      MozOsxFontSmoothing: 'grayscale', // Antialiasing.
    },
    body: {
      margin: 0,
			overflowX: 'hidden',
			/* Stylish the scrollbar (works on Chrome) */
			'&::-webkit-scrollbar': {
        width: 8,
				backgroundColor: '#F5F5F5'
			},
			'&::-webkit-scrollbar-track': {
				boxShadow: 'inset 0 0 6px rgba(0,0,0,0.3)',
				backgroundColor: '#F5F5F5',
			},
			'&::-webkit-scrollbar-thumb': {
				borderRadius: 10,
				background: `linear-gradient(to bottom,
                     rgba(93,205,177,0.9) 0%,
                     rgba(83,160,163,0.9) 28%,
                     rgba(83,160,163,0.9) 63%,
                     rgba(93,159,161,0.9) 74%,
                     rgba(102,158,160,0.89) 83%,
                     rgba(75,215,198,0.87) 98%,
                     rgba(71,223,203,0.87) 100%)`,
				border: '1px solid #aaa',
			},
    },
  },
});

let AppWrapper = props => props.children;

AppWrapper = withStyles(styles)(AppWrapper);

const context = createContext();

function withRoot(BaseComponent) {
  class WithRoot extends Component {
    componentDidMount() {
      // Remove the server-side injected CSS.
      const jssStyles = document.querySelector('#jss-server-side');
      if (jssStyles && jssStyles.parentNode) {
        jssStyles.parentNode.removeChild(jssStyles);
      }
    }

    render() {
      return (
        <JssProvider registry={context.sheetsRegistry} jss={context.jss}>
          <MuiThemeProvider theme={context.theme} sheetsManager={context.sheetsManager}>
            <AppWrapper>
              <BaseComponent {...this.props}/>
            </AppWrapper>
          </MuiThemeProvider>
        </JssProvider>
      );
    }
  }

  if (process.env.NODE_ENV !== 'production') {
    WithRoot.displayName = wrapDisplayName(BaseComponent, 'withRoot');
  }

  return WithRoot;
}

export default withRoot;
