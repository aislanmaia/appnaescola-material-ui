import React                from 'react';
import { Row, Col }		      from 'react-flexbox-grid';
import { withStyles }				from 'material-ui/styles';
import styles               from './styles.js';
import { CircularProgress } from 'material-ui/Progress';
import Modal                from 'material-ui/Modal/Modal';

const Spinner = ({label, classes}) => (
	 <div>
		 <Modal
			 className={classes.root}
			 show={true}
			 >
			 <Row className={classes.contentSpinner} center="xs" middle="xs">
				 <Col xs md lg><CircularProgress className={classes.progress} size={150} />
					 <Row>
						 <Col>
							 <div className={classes.labelWrapper}> { label } </div>
						 </Col>
					 </Row>
				 </Col>
			 </Row>
		 </Modal>
	 </div>
);

Spinner.defaultProps = {
  label: "Carregando área do sistema...",
	classes: {}
}

export default withStyles(styles, { withTheme: true })( Spinner );
