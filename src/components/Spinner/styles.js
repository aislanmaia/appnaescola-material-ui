const styles = theme => ({
	root: {
		justifyContent: 'center',
    alignItems: 'center',
	},
	contentSpinner: {
		'&:focus': {
			outline: 'none',
		},
	},
	progress: {
    margin: `0 ${theme.spacing.unit * 2}px`,
  },
	labelWrapper: {
		display: 'block',
		fontSize: '1.5rem',
		color: '#ccc',
		textShadow: '0 1px 0 rgba(255, 255, 255, 0.4)',
	},
});

export default styles;
