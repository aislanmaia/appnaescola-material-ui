import React					from 'react';
import { withStyles } from 'material-ui/styles';
import styles         from './styles';
import Typography     from 'material-ui/Typography';

const ClassroomDetails = ({ classroom, classes }) =>
	<div>
		<div className={classes.infoName}>
			<div className={classes.content}>
				<Typography type="body1" align='left'>
					<b>Nome: &nbsp;</b>
				</Typography>
				<Typography type="body1" gutterBottom>
					<i>{classroom.name}</i>
				</Typography>
			</div>
		</div>
	</div>

export default withStyles(styles, { withTheme: true })(ClassroomDetails);
