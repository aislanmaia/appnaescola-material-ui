import { form, floatingActions } from 'styles/settings';

const styles = theme => (
   Object.assign({}, form(theme), {
		 actions: {
       paddingTop: '2rem'
		 },
		 tooltip: {
			 fontSize: '1rem',
			 padding: '.5rem',
			 zIndex: 4000,
		 },
		 nameSection: {
			 paddingBottom: '4rem',
		 },
		 fields: {
       paddingBottom: '2rem',
		 },
		 buttonSave: {
			 flip: false,
			 position: 'absolute',
			 bottom: 32,
			 right: 32,
		 }
   }, floatingActions)
);

export default styles;
