import React, { Component } from 'react';
import { connect }          from 'react-redux';
import { compose }					from 'redux';
import { withStyles }       from 'material-ui/styles';
import { Field }            from 'redux-form'; // imported Field
import Button               from 'material-ui/Button';
import Tooltip              from 'material-ui/Tooltip';
import { Row, Col }         from 'react-flexbox-grid';
import {
  TextField,
}                           from 'redux-form-material-ui';
import EditIcon							from 'material-ui-icons/Edit';
import CheckIcon						from 'material-ui-icons/Check';
import ErrorIcon						from 'material-ui-icons/Error';
import styles               from './styles';
import {
	setSubmitFailed,
	stopSubmit } from 'redux-form';
import _                    from 'lodash';
import {toastr}             from 'react-redux-toastr';

class ClassroomForm extends Component {
  constructor(props) {
    super(props)
    this.state =
    { unsaved: true
    , name: ''
    }
    this.updateClassroom = props.updateClassroom.bind(this);

		this.InputPropsClass = (classes) => {
			return {
				className: classes.input,
			};
		};
  }


  // componentDidMount() {
  //   const { form } = this.props;
  //   this.props.router.setRouteLeaveHook(this.props.route, () => {
  //     if (this.state.unsaved)
  //       // return this._handleToggleDialog();
  //       return "Você tem dados ainda não salvos! Tem certeza que deseja sair?";
  //   })
  // }


  componentWillUnmount() {
    const { resetActiveClassroom, resetNewClassroom } = this.props;
    resetNewClassroom();
    resetActiveClassroom();
  }

	componentDidUpdate() {
		const { errorsCreate } = this.props;
		if (errorsCreate) this.renderCreateErrors();
	}

  // _handleGoBackClick (e) {
  //   e.preventDefault();
  //   const { form } = this.props;
  //   console.log("form.ClassroomForm.submitSucceeded", this.props.form.ClassroomForm.submitSucceeded);
  //   if (form.ClassroomForm.submitSucceeded) {
  //     this.setState({unsaved: false});
  //   }
  //   this.props.router.goBack();
  // }


  handleClassesInput = (field) => {
    const { classes } = this.props;
    return (
       field.meta.touched
				? (!field.meta.error && field.meta.valid)
				? {underline: classes.inputUnderline}
       : {}
       : {}
    )
  }

  handleEndAdornment = (field) => {
    const { classes } = this.props;
    return (
			 field.meta.touched
        ? (!field.meta.error && field.meta.valid
           ? <CheckIcon className={classes.validIconStatus} />
           : <ErrorIcon className={classes.invalidIconStatus} />)
       : (!field.meta.error && field.meta.valid
          ? <CheckIcon className={classes.validIconStatus} /> : '')
    )
  }

  _handleChange = (name, value) => {
    this.setState({...this.state, [name]: value});
  }

  _handleSaveClassroomClick = (e) => {
    e.preventDefault();
    alert("salvar sala!");
  }

	dispatchClickSave() {
		const { requestedSaveForm} = this.props;
		const buttonSave = this.button;
		requestedSaveForm();
		buttonSave.click();
	}

	renderCreateErrors = () => {
    const { dispatch, errorsCreate, resetNewClassroom } = this.props;
		console.log("errorsCreate", errorsCreate);
    const messages = _.flatten(_.values(errorsCreate.response.errors));
		if (errorsCreate) {
			dispatch(stopSubmit("ClassroomForm", {name: messages[0]}));
			dispatch(setSubmitFailed("ClassroomForm", "name"));
			resetNewClassroom();
			toastr.removeByType("info");
			toastr.removeByType("success");
			toastr.warning("Erro ao criar nova sala!", "Verifique os campos do formulário", {timeout: 10});
		}
	}

  renderNameField = (field) => {
		const { classes } = this.props;
    return (
			 <div className={classes.containerField}>
				 <EditIcon className={classes.fieldIcon}/>
				 <TextField
					 type="text"
					 style={{textIndent: 0}}
					 label="Nome"
					 placeholder="Digite o nome e número da sala"
					 className={classes.textField}
					 //inputClassName={classes.input}
					 InputProps={{
						 endAdornment: this.handleEndAdornment(field),
						 classes: this.handleClassesInput(field),
						 inputProps: this.InputPropsClass(classes)
					 }}
					 error={field.meta.touched && field.meta.error ? !!field.meta.error : false}
					 helperText={field.meta.touched && field.meta.error ? field.meta.error : ''}
					 {...field.input}
				   autoFocus={true}
					 />
			 </div>
		);
	}

  render() {
    const {
			error,
			classes,
			initialValues,
			handleSubmit,
			classroomId,
      requestSave,
      requestedSaveForm,
		} = this.props;

		console.log("classroomId", classroomId);

		// eslint-disable-next-line
		{requestSave ? this.dispatchClickSave() : null}
		// {errorsCreate ? this.renderCreateErrors() : null}

    return (
      <div className="classroomform-section">
        <form onSubmit={
          classroomId ?
							handleSubmit(this.updateClassroom.bind(this, requestedSaveForm, initialValues))
						: handleSubmit(this.props.createClassroom.bind(this, requestedSaveForm))
				}
				>
					<div className="classroom-inputs-section">
						<Row className={classes.nameSection}>
							<Col xs={12} md={12} className={classes.fields}>
								<Field
									name="name"
									component={this.renderNameField}
									type="text"
									/>
								{error && <strong>{error}</strong>}
							</Col>
						</Row>

						<div >
							<Tooltip
								id="tooltip-icon-save" title="Salvar dados" placement="bottom"
								classes={{
								 	tooltipBottom: classes.tooltip,
								 }}
								>
								<Button fab rootRef={el => this.button = el} color="primary" aria-label="save" type="submit" className={classes.buttonSave}>
									<CheckIcon />
								</Button>
							</Tooltip>
						</div>
					</div>
        </form>
      </div>
    );
  }

}

const mapStateToProps = (state) => ({
  form: state.form,
	errorsCreate: state.classrooms.newClassroom.error,
	createSuccess: state.classrooms.newClassroom
});

const wrapper = compose(
	 connect(mapStateToProps),
	 withStyles(styles, {withTheme: true}),
);


export default wrapper(ClassroomForm);
