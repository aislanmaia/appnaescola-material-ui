import React, { Component } from 'react';
import { connect }					from 'react-redux';
// import { push }          from 'react-router-redux';
import { withStyles }				from 'material-ui/styles';
import { compose }					from 'redux';
import MUIGrid              from 'material-ui/Grid';
import Loadable							from 'containers/Loadable';
import styles								from './styles';

const ClassroomsTable = Loadable({loader: () => import('components/Classrooms/ClassroomList/ClassroomsTable')});


class ClassroomList extends Component {

	constructor(props) {
    super(props);

		this.state =
			{ dialogDeleteActive: false,
				classroom: '',
				currentPage: props.currentPage,
			};
  }

	_renderActions = (classroom) => {
		return (
      [
        <span key={classroom.id}>
          {/* <TooltipButton icon='visibility' tooltip='Ver detalhes' />
							<TooltipButton icon='edit' tooltip='Editar sala' onMouseUp={this._handleEditClick.bind(null, classroom.id)} />
							<TooltipButton icon='delete' tooltip='Remover sala' onMouseUp={this._handleToggleDeleteDialog.bind(null, classroom)} /> */}
        </span>
      ]
    )
  }

  render() {
		const { fetchMoreClassrooms,
						tableSearchClassrooms,
						currentPage,
						searchResults,
						setSelection,
						selection,
						searchDialogOpen,
						requestClearSelectedRows,
						toggleRequestClearSelectedRows,
						deleteDispatched,
					} = this.props;

		return (
		  <MUIGrid container>
			  <MUIGrid item xs={12} sm={12}>
					<div className="list-classrooms">
						<ClassroomsTable
							classrooms={this.props.classrooms}
							fetchMoreData={fetchMoreClassrooms}
							tableSearchClassrooms={tableSearchClassrooms}
							currentPage={currentPage}
							searchResults={searchResults}
							setSelection={setSelection}
							selection={selection}
							searchDialogOpen={searchDialogOpen}
							requestClearSelectedRows={requestClearSelectedRows}
							toggleRequestClearSelectedRows={toggleRequestClearSelectedRows}
							deleteDispatched={deleteDispatched}
						/>
					</div>
				</MUIGrid>
			</MUIGrid>
		);
	}
}

const mapStateToProps = (state) => (
	{
		/* currentPage: state.classrooms.classroomsList.classrooms.page_number,*/
    totalPages: state.classrooms.classroomsList.classrooms ? state.classrooms.classroomsList.classrooms.total_pages : 1
		, deleteError: state.classrooms.deletedClassroom ? state.classrooms.deletedClassroom.error : null
		, deleteSuccess: state.classrooms.deletedClassroom ? state.classrooms.deletedClassroom.success : null
  }
);

const mapDispatchToProps = (dispatch) => ({
  dispatch
});


const wrapper = compose(
	 connect(mapStateToProps, mapDispatchToProps),
   withStyles(styles, {withTheme: true}),
);

export default wrapper(ClassroomList);

