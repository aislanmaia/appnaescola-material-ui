import React from 'react';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Dialog, { DialogActions, DialogContent, DialogTitle, withMobileDialog } from 'material-ui/Dialog';
import PropTypes from 'prop-types';
import styles from './styles';
import Slide from 'material-ui/transitions/Slide';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
import CloseIcon from 'material-ui-icons/Close';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class DetailsDialog extends React.Component {
	state = {
		open: true,
	};

	handleClickOpen = () => {
		this.setState({ open: true });
	};


	handleClose = () => {
		this.setState({ open: false });
	};


	render() {

		const {
			classes,
			title,
			content,
			handleEntering,
			handleCancel,
			handleConfirm,
			buttonActions,
			onClose,
			open,
			...other
		} = this.props;

		return (
			<Dialog
				fullScreen
				open={open}
				onClose={this.handleClose}
				transition={Transition}
			>
			<AppBar className={classes.appBar}>
				<Toolbar>
					<IconButton color="contrast" onClick={handleCancel} aria-label="Close">
						<CloseIcon />
					</IconButton>
					<Typography type="title" color="inherit" className={classes.flex}>
						{title}
					</Typography>
					<Button color="contrast" onClick={handleCancel}>
						Cancelar
					</Button>
				</Toolbar>
			</AppBar>
			<DialogContent>
				{ content }
			</DialogContent>
			<DialogActions className={classes.dialogActions}>
				{ buttonActions ?
					buttonActions :
					( <div>
							<Button onClick={handleCancel} color="primary">
								Cancelar
							</Button>
							<Button onClick={handleConfirm} color="primary">
								Ok
							</Button>
						</div> )
 				}
				</DialogActions>
			</Dialog>
		)
	}
};
DetailsDialog.propTypes = {
  classes: PropTypes.object.isRequired,
	title: PropTypes.string,
	content: PropTypes.object.isRequired,
	handleCancel: PropTypes.func,
	handleConfirm: PropTypes.func,
};

/* DetailsDialog.defaultProps = {
 * 	title: "Exibindo detalhes",
 * 	content: "",
 * 	handleCancel: null,
 * 	handleEntering: null,
 * 	handleConfirm: null,
 * }
 * */
export default withStyles(styles, {withTheme: true}) ( DetailsDialog );
