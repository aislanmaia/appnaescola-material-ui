import React from 'react';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Dialog, { DialogActions, DialogContent, DialogTitle, withMobileDialog } from 'material-ui/Dialog';
import styles from './styles';

const ConfirmDialog = ({
	  classes,
		title,
		content,
		handleEntering,
		handleCancel,
		handleConfirm,
	  buttonActions,
	  fullscreen,
		...other
	}) => {
	return (
		<Dialog
			ignoreBackdropClick
			ignoreEscapeKeyUp
			//maxWidth="xs"
			fullscreen={fullscreen}
			onEntering={handleEntering}
			{...other}
		>
			<DialogTitle>{title}</DialogTitle>
				<DialogContent>
					{content}
				</DialogContent>
				<DialogActions className={classes.dialogActions}>
				{ buttonActions ?
					buttonActions :
					( <div>
							<Button onClick={handleCancel} color="primary">
								Cancelar
							</Button>
							<Button onClick={handleConfirm} color="primary">
								Ok
							</Button>
						</div> )
 				}
			</DialogActions>
		</Dialog>
	)
};


ConfirmDialog.defaultProps = {
	title: "Confirmar ação?",
	content: "",
	handleCancel: null,
	handleEntering: null,
	handleConfirm: null,
	buttonActions: null,
}

export default withMobileDialog()( withStyles(styles, {withTheme: true}) ( ConfirmDialog ) );
