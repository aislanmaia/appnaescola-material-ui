import React, { Component } from 'react';
import { withStyles }       from 'material-ui/styles';
import {
  withMobileDialog,
}														from 'material-ui/Dialog';
import FormDialog						from 'components/Dialogs/FormDialog';
import styles               from './styles';

class ResponsiveFormDialog extends Component {
	render(){
    return (
			<FormDialog {...this.props} ignoreBackdropClick={true} ignoreEscapeKeyUp={true} fullWidth={true}/>
		)
	}
}

export default withMobileDialog()( withStyles(styles, {withTheme: true}) (ResponsiveFormDialog) );
