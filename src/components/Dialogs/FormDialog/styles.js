const styles = theme => ({
	appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
	grid: {
    padding: '2rem 1rem',
	}
});

export default styles;
