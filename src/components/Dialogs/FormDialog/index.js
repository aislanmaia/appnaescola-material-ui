import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import AppBar from 'material-ui/AppBar';
import Dialog, {withMobileDialog} from 'material-ui/Dialog';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
import CloseIcon from 'material-ui-icons/Close';
import Slide from 'material-ui/transitions/Slide';
import styles from './styles';
import { Grid, Row, Col } from 'react-flexbox-grid';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class FormDialog extends Component{
	constructor(props) {
    super(props);

		this.state = {
			requestSave: false,
		};

		this.requestSave = this.requestSave.bind(this);
		this.requestedSaveForm = this.requestedSaveForm.bind(this);
	}

	requestedSaveForm(formRef) {
		// const { callback } = this.props;
		this.setState({ requestSave: false});
		this.props.requestClose();
		// if (callback) callback();
	}

  requestSave() {
    this.setState({ requestSave: true });
	}

	render() {
		const {
			action,
			classes,
			open,
			fullScreen,
			component: Component,
			//callback,
			resourceId,
			requestClose,
			...other,
		} = this.props;

		return (
			 <Dialog
				 fullScreen={fullScreen}
				 open={open}
				 onClose={requestClose}
				 transition={Transition}
				 {...other}
				 >
				 <AppBar className={classes.appBar}>
					 <Toolbar>
						 <IconButton color="contrast" onClick={requestClose} aria-label="Close">
							 <CloseIcon />
						 </IconButton>
						 <Typography type="title" color="inherit" className={classes.flex}>
							 {action}
						 </Typography>
						 <Button color="contrast" onClick={requestClose}>
							 Cancelar
						 </Button>
						 {/* <Button color="contrast" onClick={this.requestSave}>
								 Salvar
								 </Button> */}
					 </Toolbar>
				 </AppBar>
				 <Grid fluid className={classes.grid}>
					 <Row center="xs">
						 <Col xs>
							 <Row around="xs">
								 <Col xs sm md>
									 <Component
										 requestSave={this.state.requestSave}
										 requestedSaveForm={this.requestedSaveForm}
										 resourceId={resourceId}
									 />
								 </Col>
							 </Row>
						 </Col>
					 </Row>
				 </Grid>
			 </Dialog>
		);
	}
}


export default withMobileDialog()( withStyles(styles, {withTheme: true}) ( FormDialog ));
