import React            from 'react';
import { withRouter }   from 'react-router';
import { withStyles }   from 'material-ui/styles';
import classNames       from 'classnames';
import Drawer           from 'material-ui/Drawer';
import { NavLink }      from 'react-router-dom';
import List, {
  ListItem,
  ListItemIcon,
  ListItemText }        from 'material-ui/List';
/* import Collapse         from 'material-ui/transitions/Collapse';*/
/* import ExpandLess       from 'material-ui-icons/ExpandLess';*/
/* import ExpandMore       from 'material-ui-icons/ExpandMore';*/
import Typography       from 'material-ui/Typography';
/* import Divider          from 'material-ui/Divider';*/
import IconButton       from 'material-ui/IconButton';
/* import MenuIcon         from 'material-ui-icons/Menu';*/
import ChevronLeftIcon  from 'material-ui-icons/ChevronLeft';
import ChevronRightIcon from 'material-ui-icons/ChevronRight';

import styles from './Sidebar.styles';

const PersistentDrawer = (props) => {
  const { classes, theme } = props;
  const {
    handleDrawerClose,
    open,
  } = props;

  return (
    <Drawer
        type="persistent"
        classes={{
          paper: classes.drawerPaper,
        }}
        open={open}
    >
      <div className={classes.drawerInner}>
        <div className={classes.drawerHeader}>
          <Typography type="title" color="inherit">Menu Principal</Typography>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <div className={classes.drawerSubHeader}>
          <Typography type="subheading" color="inherit" className={classes.titleSubHeader}>
            Administrativo
          </Typography>
        </div>
        <List className={classes.root} >
					<NavLink
						to="/administrativo/alunos"
						className={classes.linkItem}
						activeClassName={classes.activeLink}
					>
						<ListItem button >
							<ListItemIcon className={classes.itemIcon} >
								<div className={classNames(classes.icon, classes.iconStudent)}></div>
							</ListItemIcon>
							<ListItemText primary={<p className={classes.listItemText}>Alunos</p>} disableTypography={true}/>
						</ListItem>
					</NavLink>
					<NavLink
						to="/administrativo/professores"
						className={classes.linkItem}
						activeClassName={classes.activeLink}
					>
						<ListItem button>
							<ListItemIcon className={classes.itemIcon} >
								<div className={classNames(classes.icon, classes.iconProfessor)}></div>
							</ListItemIcon>
							<ListItemText primary={<p className={classes.listItemText}>Professores</p>} disableTypography={true}/>
						</ListItem>
					</NavLink>
					<NavLink
						to="/administrativo/turmas-salas"
						className={classes.linkItem}
						activeClassName={classes.activeLink}
					>
						<ListItem button>
							<ListItemIcon className={classes.itemIcon} >
								<div className={classNames(classes.icon, classes.iconClassClassroom)}></div>
							</ListItemIcon>
							<ListItemText primary={<p className={classes.listItemText}>Turmas e Salas</p>} disableTypography={true}/>
						</ListItem>
					</NavLink>
					<NavLink
						to="/administrativo/materias"
						className={classes.linkItem}
						activeClassName={classes.activeLink}
					>
						<ListItem button>
            <ListItemIcon className={classes.itemIcon}>
              <div className={classNames(classes.icon, classes.iconSubject)}></div>
            </ListItemIcon>
            <ListItemText primary={<p className={classes.listItemText}>Matérias</p>} disableTypography={true}/>
          </ListItem>
					</NavLink>
          {/*<ListItemText inset primary="Inbox" />*/}
            {/*this.state.open ? <ExpandLess /> : <ExpandMore />*/}
          {/*<Collapse in={this.state.open} transitionDuration="auto" unmountOnExit>
            <ListItem button className={classes.nested}>
              <ListItemIcon>
                <StarBorder />
              </ListItemIcon>
              <ListItemText inset primary="Starred" />
            </ListItem>
          </Collapse>
          */}
        </List>

      </div>
    </Drawer>

  )
}

export default withRouter(withStyles(styles, { withTheme: true })(PersistentDrawer));
