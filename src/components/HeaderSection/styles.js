const styles = theme => ({
	infoSection: {
		paddingTop: '1rem',
		paddingBottom: '1rem',
		titleSection: {
			display: 'inline-flex',
			color: '#4d4d4d',
			small: {
				fontSize: '65%',
				alignSelf: 'center',
				lineHeight: '2.1rem',
				paddingLeft: '.6rem',
				color: '#808080',
			}
		},
	}
});

export default styles;
