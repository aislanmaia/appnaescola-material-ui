import React							from 'react';
import PropTypes          from 'prop-types';
import { /* Grid,*/ Row, Col } from 'react-flexbox-grid';
// import Breadcrumbs        from 'react-router-breadcrumbs';
import { withStyles }     from 'material-ui/styles';
import styles             from './styles';
import Typography         from 'material-ui/Typography';

const HeaderSection = (props) => {
  const { title, subtitle, classes, /*breadcrumbRoutes*/ } = props;
	const handleClick = () => {
    console.log("clicou na infoSection div");
	}
  return (
	 <Row middle="xs">
			<Col xs>
        <div className={classes.infoSection} onClick={handleClick}>
          <div className={classes.titleSection}>
						<Typography type="headline" component="h4">
              {title}
						</Typography>
						<Typography type="body1" component="div">
              <p style={{"color": "rgba(0,0,0,.50)", "margin": 0}}>{subtitle}</p>
						</Typography>
          </div>
        </div>
      </Col>

      {/* TODO: Port the Breadcrumbs to a new version/package
					to support the new React version and possibly the
					react-router v4.
			*/}
      {/* }<div className={`${flexboxgrid["col-xs-6"]}`}>
        <div className={`${flexboxgrid.row} ${flexboxgrid["end-xs"]}`}>
          <Breadcrumbs routes={breadcrumbRoutes} className={styles.breadcrumbs}/>
        </div>
      </div>
			*/}
		</Row>
  );
};

HeaderSection.propTypes = {
  title: PropTypes.string.isRequired,
	subtitle: PropTypes.string.isRequired,
};

export default withStyles(styles, { withTheme: true })(HeaderSection);
