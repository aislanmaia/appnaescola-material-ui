import React											from 'react';
import { Switch }					        from "react-router-dom";
import Loadable                   from 'containers/Loadable';
import AppliedRoute								from 'components/Routes/AppliedRoute';
import DashboardsRoute						from 'components/Routes/DashboardsRoute';
import {
	UserIsNotAuthenticated,
}                                 from 'containers/Authorization';

const LoginArea = Loadable({loader: () => import("views/Login")});
const AdministrativeArea = Loadable({loader: () => import("views/Administrative")});
const PedagogicArea = Loadable({loader: () => import("views/Pedagogic")});
const SubjectsArea = Loadable({loader: () => import("containers/Subjects/SubjectsList")});
const ClassClassroomsArea = Loadable({loader: () => import("views/Administrative/Classes_Classrooms")});
const ClassroomsArea = Loadable({loader: () => import("containers/Classrooms/ClassroomsList")});
const ClassesArea = Loadable({loader: () => import("containers/Classes/ClassesList")});



export default ({ childProps }) =>
	<Switch>
		<AppliedRoute exact path="/login" component={ UserIsNotAuthenticated( LoginArea ) } />
		<DashboardsRoute exact path="/" components={[AdministrativeArea, PedagogicArea]} props={childProps}/>
		<AppliedRoute path="/administrativo/materias" component={  SubjectsArea  } />
		<AppliedRoute path="/administrativo/turmas-salas" component={  ClassClassroomsArea  } />
		<AppliedRoute path="/administrativo/salas" component={  ClassroomsArea  } />
		<AppliedRoute path="/administrativo/turmas" component={  ClassesArea  } />
	</Switch>;
