import React     from "react";
import { Route,
         Redirect } from "react-router-dom";
import _         from 'lodash';

const rolesAdm = [ "Admin", "Secretario" ];

export default ({ components: Components, props: cProps, ...rest }) =>
  <Route {...rest} key="1" render={
		props => {
			const [Administrative, Pedagogic] = Components;
      return cProps.currentUser ?
						 _.includes(rolesAdm, cProps.currentUser.role)
		           ? <Administrative {...props} {...cProps} />
				       : <Pedagogic {...props} {...cProps} />
					 : <Redirect to={{
						 pathname: '/login',
						 state: { from: props.location }
					 }}/>
			}
		}
/>;
