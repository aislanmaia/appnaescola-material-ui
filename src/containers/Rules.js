import React from 'react';

const Roles = (allowedRoles) =>
  (WrappedComponent) =>
    class WithAuthorization extends React.Component {
      constructor(props) {
        super(props);
        console.log("props", props);
      }

      render() {
        const { role } = this.props
        if (allowedRoles.includes(role)) {
          return <WrappedComponent {...this.props} />
        } else {
          return <h1> Forbidden! </h1>
        }
      }
    }

export default Roles;
