import React, { Component }		from 'react';
import { connect }						from 'react-redux';
/* import { bindActionCreators } from 'redux';*/
import SubjectsList						from 'components/Subjects/SubjectsList';
import SubjectsActions				from 'actions/async/subjects';
import { fetchSubjects,
         fetchSubject }				from 'actions/sync/subjects';
import HeaderSection					from 'components/HeaderSection';
import { Grid, Row }					from 'react-flexbox-grid';
import Loadable								from 'containers/Loadable';
import AddIcon								from 'material-ui-icons/Add';
import Button									from 'material-ui/Button';
import styles									from './styles';
import { withStyles }					from 'material-ui/styles';

const ConfirmDialog = Loadable({loader: () => import("components/Dialogs/ConfirmDialog")});
const FormDialog = Loadable({loader: () => import("components/Dialogs/FormDialog")});
const SubjectFormContainer = Loadable({loader: () => import("containers/Subjects/SubjectForm")});
const SubjectDetails = Loadable({loader: () => import("components/Subjects/SubjectDetails")});

class SubjectsListContainer extends Component {
  constructor(props) {
    super(props);
    this.state =
      { dialogDeleteActive: false
			, showDetailsDialog: false
			, formDialogOpen: false
			, formDialogTitle: ''
			, action: ''
      , subject: ''
      };
  }

	componentDidMount() {
    console.log("component SubjectsListContainer mounted");
	}

  componentWillMount() {
    const { fetchSubjects } = this.props;
    fetchSubjects();
  }

	_handleAddSubject = () => {
    this._handleSetActionIntent(`Adicionando Nova Matéria`);
		this._handleRequestOpenFormDialog();
  }

	_handleShowDetailsClick = (subject) => {
    // DetailsDialog
    this.setState({showDetailsDialog: !this.state.showDetailsDialog});
    subject ? this.setState({subject: subject}) : this.setState({subject: ''});
		// this._handleToggleDetailsDialog();
	}

  _handleEditSubjectClick = (subject) => {
    const { fetchSubject } = this.props;
		this.setState({ subject: subject });
		this._handleRequestOpenFormDialog();
		this._handleSetActionIntent(`Editando matéria: ${subject.name}`);
		fetchSubject(subject);
  }

  _handleRemoveSubjectClick = (id) => {
		const { dispatch } = this.props;
    dispatch(SubjectsActions.delete(id));
		this._handleToggleDeleteDialog(null);
  }

  _handleToggleDeleteDialog = (subject) => {
    this.setState({dialogDeleteActive: !this.state.dialogDeleteActive});
    subject ? this.setState({subject: subject}) : this.setState({subject: ''});
  }

	_handleToggleDetailsDialog = (subject) => {
    this.setState({showDetailsDialog: !this.state.showDetailsDialog});
    subject ? this.setState({subject: subject}) : this.setState({subject: ''});
  }

	_handleSetActionIntent = (actionText) => {
		this.setState({ action: actionText });
	}

  _handleRequestCloseFormDialog = () => {
    this.setState({ formDialogOpen: false });
  };

	_handleRequestOpenFormDialog = () => {
		this.setState({ formDialogOpen: true });
	}


  render() {
    const {
			classes,
			subjects,
			fetchSubjects,
			loadingSubject,
		} = this.props;
		console.log("Lista de subjects atual", subjects);
    return (
      <Grid fluid>
				<HeaderSection
					title={"Matérias"}
					subtitle={"Lista de cadastros"}
				/>
				<SubjectsList
          subjects={subjects}
					handleShowDetailsClick={this._handleShowDetailsClick}
          handleEditSubjectClick={this._handleEditSubjectClick}
          handleRemoveSubjectClick={this._handleRemoveSubjectClick}
          handleToggleDeleteDialog={this._handleToggleDeleteDialog}
        />
				{ this.state.dialogDeleteActive ?
					<ConfirmDialog
						title="Confirmar exclusão?"
						content={<p>Tem certeza que deseja <strong>deletar</strong> a matéria <b>{this.state.subject.name}</b>?</p>}
						handleCancel={this._handleToggleDeleteDialog.bind(null, '')}
						handleConfirm={this._handleRemoveSubjectClick.bind(null, this.state.subject)}
						open={this.state.dialogDeleteActive}
					/> : null}
				{ this.state.showDetailsDialog && !loadingSubject ?
					<ConfirmDialog
						title={`Exibindo detalhes:`}
						content={<SubjectDetails subject={this.state.subject} />}
						handleCancel={this._handleToggleDetailsDialog}
						handleConfirm={this._handleEditSubjectClick.bind(null, this.state.subject)}
						open={this.state.showDetailsDialog}
						buttonActions={
							<Grid fluid>
								<Row between="xs">
									<Button onClick={this._handleToggleDetailsDialog} color="primary">
										Cancelar
									</Button>
									<Button onClick={this._handleEditSubjectClick.bind(null, this.state.subject)} color="primary">
										Editar
									</Button>
								</Row>
							</Grid>
						}
					/> : null}
				{ this.state.formDialogOpen && !loadingSubject ?
					<FormDialog
						action={`${ this.state.action }`}
						fullWidth={true}
						open={this.state.formDialogOpen}
						requestClose={this._handleRequestCloseFormDialog}
						component={SubjectFormContainer}
						resourceId={this.props.activeSubject ? this.props.activeSubject.id : ''}
						callback={fetchSubjects}
					/> : null }
					<div className={classes.floatingActions}>
						<Button
							fab
							color="primary"
							aria-label="add"
							className={classes.addActionButton}
							onClick={this._handleAddSubject}
							// onClick={() => toastr.success('The title', 'The message')}
						>
							<AddIcon />
						</Button>
					</div>
			</Grid>
    )
  }
}

const mapStateToProps = (state) => (
  { subjects: state.subjects.subjectsList.subjects
	, loadingSubject: state.subjects.activeSubject ? state.subjects.activeSubject.loading : false
	, activeSubject: ( state.subjects.activeSubject ?
												state.subjects.activeSubject.subject ?
												state.subjects.activeSubject.subject : null
												: null
											)
  }
);

// const mapDispatchToProps = (dispatch) => ({
//   dispatch,
//   fetchSubjects: () => dispatch(SubjectsActions.fetchSubjects()),
// 	fetchSubject: (id) => dispatch(SubjectsActions.fetchSubject(id)),
// });

const mapDispatchToProps = (dispatch) => {
	return {
		dispatch,
    /* fetchSubjects: bindActionCreators(fetchSubjects, dispatch),*/
    fetchSubjects: () => dispatch(fetchSubjects()),
		/* fetchSubject: bindActionCreators(fetchSubject, dispatch)*/
		fetchSubject: (subject) => dispatch(fetchSubject(subject))
	}
	// return bindActionCreators( {
	// 	fetchSubjects,
	// 	fetchSubject
	// }, dispatch );
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(SubjectsListContainer));

// import { Link } from 'react-router-dom';

// class TestThree extends Component {
//   componentDidMount() {
//     console.log("component TestThree mounted....");
// 	}

// 	render() {
//     return (
// 				<div>
// 				<p>Component TEST THREE</p>
// 				<Link to="/two">TWO</Link>
// 				<br/>
// 				<Link to="/">MAIN LAYOUT</Link>
// 				</div>
// 		);
// 	}
// }

// export default TestThree;
