const styles = theme => ({
	floatingActions: {
		position: 'fixed',
		right: '2rem',
		bottom: '2rem',
		zIndex: 2,
	},
});

export default styles;
