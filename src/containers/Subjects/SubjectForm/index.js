import { connect }         from 'react-redux';
import SubjectForm         from 'components/Subjects/SubjectsForm';
import { reduxForm }       from 'redux-form';
import ActionsSubject      from 'actions/async/subjects';
import { createSubject, resetActiveSubject, resetNewSubject } from 'actions/sync/subjects';
import {toastr}            from 'react-redux-toastr';
import uuid                from 'uuid/v1';

//Client side validation
function validate(values) {
  const errors = {};

  if (!values.name) {
    errors.name = 'Forneça um nome';
  }


  return errors;
}

const validateAndCreateSubject = (values, dispatch) => {
	console.log("Valores para salvar:", values);
	let subject = values;
	subject.uuid = uuid();

	return new Promise((resolve, reject) => {
		// dispatch(createSubject(subject))
		dispatch(ActionsSubject.create(subject))
			.then((response) => {
				console.log("should show toastr.success");
				// if (callback) callback();
				resolve(); //this is for redux-form itself
				toastr.removeByType("info");
				toastr.success(subject.name + ' criada com sucesso!', { progressBar: false } );
				//     // }
			})
			.catch((err) => console.log("err", err))

	});
};

const validateAndUpdateSubject = (initialValues, values, dispatch) => {
  console.log("Valores para atualizar:", values);
  console.log("Valores iniciais:", initialValues);
  return new Promise((resolve, reject) => {

		dispatch(ActionsSubject.update(initialValues, values))
			.then((response) => {
				// if (callback) callback();
				toastr.removeByType("info");
				toastr.success('Matéria', values.name + ' atualizada com sucesso!', { progressBar: false } );
				resolve(); //this is for redux-form itself
			});

	});
}

const mapStateToProps = (state, ownProps) => {
  console.log("ownProps from mapStateToProps", ownProps);
	return {
		// router: ownProps.router,
		subjectId: ownProps.resourceId ? ownProps.resourceId : undefined,
		// activeSubject: state.subjects.activeSubject,
		// newSubject: state.subjects.newSubject,
		// initialValues: state.subjects.activeSubject.subject
		initialValues: state.subjects.activeSubject.subject ? state.subjects.activeSubject.subject : {name: '', optional: false, uuid: ''}
	 }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    // fetchSubject: (id) => dispatch(ActionsSubject.fetchSubject(id)),
    resetActiveSubject: () => dispatch(resetActiveSubject()),
    resetNewSubject: () => dispatch(resetNewSubject()),
    createSubject: validateAndCreateSubject,
    updateSubject: validateAndUpdateSubject,
		//requestSave: ownProps.requestSave,
		//requestedSaveForm: ownProps.requestedSaveForm,
  }

  // resetMe: () => {
  //     dispatch(resetNewPost());
  //   }
  // dispatchCallAddSubject: data => dispatch(Actions.create(data)),
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
  form: 'SubjecForm',
  fields: ['name', 'optional', 'uuid'],
  validate,
  enableReinitialize: true
})(SubjectForm));

