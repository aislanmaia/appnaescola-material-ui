import Loadable       from 'react-loadable';
import LoadingSpinner from 'components/Spinner';

export default function MyLoadable(opts) {
  return Loadable(Object.assign({
		//loader: () => import(opts.pathComponent),
    loading: LoadingSpinner,
    delay: 200,
    timeout: 20,
  }, opts));
};
