import React                        from 'react';
import { connectedRouterRedirect }  from 'redux-auth-wrapper/history4/redirect';
import locationHelperBuilder				from 'redux-auth-wrapper/history4/locationHelper';
/* import { routerActions }            from 'redux';*/
import LoadingSpinner               from 'components/Spinner';
import authWrapper                  from 'redux-auth-wrapper/authWrapper';
import connectedAuthWrapper         from 'redux-auth-wrapper/connectedAuthWrapper';

const locationHelper = locationHelperBuilder({});

export const UserIsNotAuthenticated = connectedRouterRedirect({
  // This sends the user either to the query param route if we have one, or to the landing page if none is specified and the user is already logged in
  redirectPath: (state, ownProps) => locationHelper.getRedirectQueryParam(ownProps) || '/',

  // This prevents us from adding the query parameter when we send the user away from the login page
  allowRedirectBack: false,

  // If selector is true, wrapper will not redirect
  // So if there is no user data, then we show the page
  authenticatedSelector: state => state.user.data === null,

  // A nice display name for this check
  wrapperDisplayName: 'UserIsNotAuthenticated'
});

export const UserIsAuthenticated = connectedRouterRedirect({
  // The url to redirect user to if they fail
  redirectPath: '/login',

  // If selector is true, wrapper will not redirect
  // For example let's check that state contains user data
  authenticatedSelector: state => state.user !== null,

  // A nice display name for this check
  wrapperDisplayName: 'UserIsAuthenticated',

	// Returns true if the user auth state is loading
	authenticatingSelector: state => state.user.loading,

  // Render this component when the authenticatingSelector returns true
  AuthenticatingComponent: <LoadingSpinner label="Verificando informações de login..." />
});

export const UserIsAdmin = connectedRouterRedirect({
  redirectPath: '/',
  authenticatedSelector: state => state.user !== null && state.user.admin,
  wrapperDisplayName: 'UserIsAdmin',
  //redirectAction: routerActions.replace,
  allowRedirectBack: false
});

export const AdminOrElse = (Component, FailureComponent) => connectedAuthWrapper({
  authenticatedSelector: state => state.user !== null && state.user.admin,
  wrapperDisplayName: 'AdminOrElse',
  FailureComponent
})(Component);

export const VisibleOnlyByAdmin = authWrapper({
  wrapperDisplayName: 'VisibleOnlyByAdmin',
});

export default {
  UserIsNotAuthenticated,
	UserIsAuthenticated,
	VisibleOnlyByAdmin,
  AdminOrElse,
};
