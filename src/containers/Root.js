import React												from 'react';
import { Provider }									from 'react-redux';
import { BrowserRouter as Router }  from 'react-router-dom';
import ReduxToastr									from 'react-redux-toastr';
import MainLayout										from 'layouts/MainLayout';
import "../global.css";

/*
 * const propTypes =
 *   { routerHistory: PropTypes.object.isRequired
 *   , store: PropTypes.object.isRequired
 *   }*/

const Root = ({ store }) => {
  return (
    <Provider store={store}>
      <div>
        <Router>
					<div>
						<MainLayout/>
					</div>
				</Router>
        <ReduxToastr
          timeOut={4000}
          newestOnTop={false}
          preventDuplicates
          position="top-right"
          transitionIn="fadeIn"
          transitionOut="fadeOut"
          progressBar />
      </div>
    </Provider>
  );
}

/* Root.propTypes = propTypes;*/
export default Root;
