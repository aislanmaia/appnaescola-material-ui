import React, { Component } from 'react';
import Loadable							from 'containers/Loadable';

const ClassDetails = Loadable({loader: () => import("components/Classes/ClassDetails")});

class ClassDetailsContainer extends Component {
  constructor(props) {
		super(props);

		this.state =  {
			studentsTableColumns: [
				{ name: 'name', title: 'Nome'},
			],
			professorsTableColumns: [
				{ name: 'name', title: 'Nome'},
			],
		}
	}

	render() {
		const { _class } = this.props;
		return (
			<div>
				<ClassDetails
				  _class={_class}
					studentsTableColumns={this.state.studentsTableColumns}
					professorsTableColumns={this.state.professorsTableColumns}
				/>
			</div>
		)
	}
}


export default ClassDetailsContainer;
