import React, { Component } from 'react';
import { findDOMNode }      from 'react-dom';
import { connect }					from 'react-redux';
import { compose }				  from 'redux';
import { withStyles }			  from 'material-ui/styles';
import classNames           from 'classnames';
import HeaderSection				from 'components/HeaderSection';
import ClassesActions		    from 'actions/async/classes';
import {
	fetchClasses,
	/* clearDeleteState,*/
	/* setActiveClass,*/
}                           from 'actions/sync/classes';
import Loadable							from 'containers/Loadable';
import outy                 from 'outy';
import Grow                 from 'material-ui/transitions/Grow';
import { Grid, Row, Col }   from 'react-flexbox-grid';
import styles               from './styles';
import Paper							  from 'material-ui/Paper';
import Toolbar							from 'material-ui/Toolbar';
import Tooltip              from 'material-ui/Tooltip';
import Input								from 'material-ui/Input';
import Button               from 'material-ui/Button';
import IconButton           from 'material-ui/IconButton';
import AddIcon							from 'material-ui-icons/Add';
import EditIcon             from 'material-ui-icons/Edit';
import CancelIcon           from 'material-ui-icons/Cancel';
import ShowDetailsIcon      from 'material-ui-icons/RemoveRedEye';
import DeleteIcon           from 'material-ui-icons/Delete';
import SearchIcon           from 'material-ui-icons/Search';
import ReplayIcon           from 'material-ui-icons/Replay';
import _                    from 'lodash';
import {toastr}             from 'react-redux-toastr';

const ClassList = Loadable({loader: () => import("components/Classes/ClassList")});
const ConfirmDialog = Loadable({loader: () => import("components/Dialogs/ConfirmDialog")});
const DetailsDialog = Loadable({loader: () => import("components/Dialogs/DetailsDialog")});
// const FormDialog = Loadable({loader: () => import("components/Dialogs/FormDialog")});
const FormDialog = Loadable({loader: () => import('components/Dialogs/FormDialog')});
const ClassFormContainer = Loadable({loader: () => import("containers/Classes/ClassForm")});
const ClassDetailsContainer = Loadable({loader: () => import("containers/Classes/ClassDetails")});

class ClassesListContainer extends Component {

	constructor(props) {
    super(props);

		this.state =
			{	currentPage: 1
			, _class: null
			, selection: []
			, searchResults: []
			,	openSearchPopup: false
			, requestClearSelectedRows: false
			, formDialogTitle: ''
			, formDialogOpen: false
			, deleteDialogOpen: false
			, showDetailsOpen: false
			}

		this.setSelection = this.setSelection.bind(this);
		this.actionDisable = this.actionDisable.bind(this);
		this.setSearchResultsToState = this.setSearchResultsToState.bind(this);
		this.tableSearchClasses = this.tableSearchClasses.bind(this);
		this.requestSearchPopupOpen = this.requestSearchPopupOpen.bind(this);
		this.requestSearchPopupClose = this.requestSearchPopupClose.bind(this);
		this.handleSearchInputClickAway = this.handleSearchInputClickAway.bind(this);
		this.handleEditClassClick = this.handleEditClassClick.bind(this);
		this._handleToggleDeleteDialog = this._handleToggleDeleteDialog.bind(this);
		this.toggleRequestClearSelectedRows = this.toggleRequestClearSelectedRows.bind(this);
	}

	componentDidMount() {
    this._setOusideTap();
  }

  componentDidUpdate(lastProps, lastState) {
    if (lastState.openSearchPopup !== this.state.openSearchPopup) {
      setTimeout(() => this._setOusideTap());
    }
    if (this.inputSearchField && this.state.openSearchPopup) this.inputSearchField.focus();
  }

  componentWillMount() {
    const { dispatch, fetchClasses } = this.props;
		if (this.outsideTap) this.outsideTap.remove();
    // dispatch(fetchClasses(this.state.currentPage, dispatch));
		fetchClasses(this.state.currentPage, dispatch);
	}

	_setOusideTap = () => {
    const elements = [this.target];

    if (this.searchInput) {
      elements.push(this.searchInput);
    }

    if (this.outsideTap) {
      this.outsideTap.remove();
    }

    this.outsideTap = outy(
       elements,
       ['click', 'touchstart'],
       this._handleOutsideTap
    )
  }

	_handleOutsideTap = () => {
    this.setState({ openSearchPopup: false });
	}

  _fetchMoreClasses = (page) => {
    const { dispatch } = this.props;
    this.setState({currentPage: page});
    dispatch(ClassesActions.fetchClasses(page));
  }

	tableSearchClasses = (e) => {
    const { searchClasses } = this.props;
		console.log("deveria chamar async searchClasses com ", e.target.value);
		const term = e.target.value;
		if (!_.isEmpty(term)) {
			searchClasses(term)
				.then((res) => this.setSearchResultsToState(res.data))
		} else {
			this.setSearchResultsToState([])
		}
	}

	/* This wrapper function to _.debounce is necessary because
		 the event.target is null if using _.debounce directly and how
		 React treats synthetic event. So, using e.persist() the warning
		 regarding that treatment is cleared up and the event is passed
		 to the inner function.
	 */
	debouncedSearchClasses(...args) {
    const debouncedFn = _.debounce(...args);
		return function(e) {
      e.persist();
			return debouncedFn(e);
		};
	}

	actionDisable = () => {
    if (_.isEmpty( this.state.selection ) || _.size( this.state.selection ) > 1) {
			return true;
		} else {
      return false;
		}
	}

	handleSearchIconClick = () => {
		this.requestSearchPopupOpen();
		if (this.searchInput) {
			this.searchInput.focus();
			this.inputSearchField.value = "";
		}
	}

	handleAddClass = () => {
    this.setState({formDialogOpen: true});
		this._handleSetActionIntent(`Adicionando Nova Sala`);
	}

  handleEditClassClick = (e, _class) => {
		console.log("Calling handleEditClassClick with _class = ", _class);
    const { fetchClass, /* setActiveClass*/ } = this.props;
		let c;
		if (!_class) {
      c = this.state.selection[0];
		} else {
      c = _class;
		}
		fetchClass(c.id);
		// this.setState({ _class: c });
		/* setActiveClass(_class);*/
		this._handleSetActionIntent(`Editando: ${c.name}`);
		this._handleRequestOpenFormDialog();
    // push("/administrativo/materias/editar/" + id);
  }

  _handleRemoveClassClick = (_classSelected) => {
		console.log("Calling _handleRemoveClassClick with _class selected", _classSelected);
		const { dispatch } = this.props;
    /* dispatch(ClassesActions.delete(id));*/
		if (_.isArray(_classSelected) && !_.isEmpty(_classSelected)) {
			if (_.size(_classSelected) === 1) {
				console.log("_classSelected equal 1");
				let _class = _classSelected[0];
				if (_class) {
					dispatch(ClassesActions.delete(_class));
					this.setSelection([], true);
				}
			} else {
				dispatch(ClassesActions.batchDelete(_classSelected));
				this.setSelection([], true);
			}
 		}
		this._handleToggleDeleteDialog(null);
  }

	_handleSetActionIntent = (actionText) => {
		this.setState({ formDialogTitle: actionText });
	}

	_handleRequestOpenFormDialog = () => {
		this.setState({ formDialogOpen: true });
	}

	_handleRequestOpenFormDialog = () => {
		this.setState({ formDialogOpen: true });
	}

  _handleRequestCloseFormDialog = () => {
    this.setState({ formDialogOpen: false });
  };

	_handleToggleDeleteDialog = (_classSelected) => {
		this.setState({deleteDialogOpen: !this.state.deleteDialogOpen});
		if (_.isArray(_classSelected) && !_.isEmpty(_classSelected)) {
			if (_.size(_classSelected) === 1) {
				let _class = _classSelected[0];
				_class ? this.setState({_class: _class}) : this.setState({_class: ''});
			}
 		}
	}

	_handleShowDetailsClick = (e, _class) => {
    const { fetchClass, /* setActiveClass*/ } = this.props;
		let c;
		if (!_class) {
      c = this.state.selection[0];
		} else {
      c = _class;
		}
		fetchClass(c.id);
		this._handleToggleDetailsDialog();
	}

	_handleToggleDetailsDialog = (_class) => {
    this.setState({showDetailsOpen: !this.state.showDetailsOpen});
    _class ? this.setState({_class: _class}) : this.setState({_class: ''});
  }

	setSelection = (selection, replaceAll=false) => {
		console.log("selection in setSelection", selection);
		let selectionState = this.state.selection;
		let found = undefined;
		if (replaceAll) {
			if (_.isArray(selection))
				this.setState({selection: selection});
			else
				this.setState({selection: [selection]})
		} else {
			if (!_.isEmpty(selection) && !_.isArray(selection)) {
				found = selectionState.findIndex(row => row.id === selection.id) > -1;
				if (found) {
					/* If the element is already in selection state array, remove it
						 from selection state array by filtering out a new array.
					 */
					selectionState = selectionState.filter(row => row.id !== selection.id);
				} else {
					/* The element is new in selection state array */
					selectionState.push(selection);
				}
			} else {
				if (_.isArray(selection)) {
					selectionState = selectionState.concat(selection);
				}
			}
			this.setState({selection: selectionState});
		}
	}

	setSearchResultsToState = (results) => {
    this.setState({searchResults: results});
	}

	requestSearchPopupOpen = () => {
    this.setState({openSearchPopup: true});
	}

	requestSearchPopupClose = () => {
		console.log("calling requestSearchPopupClose")
    this.setState({openSearchPopup: false});
	}

  resetTableState = () => {
		this.resetSearchResults();
		this.resetSelectionInState();
	}

	resetSearchResults = () => {
		this.setSearchResultsToState([]);
	}

	resetSelectionInState = () => {
    this.setState({selection: [] });
		this.setState({requestClearSelectedRows: true});
	}

	toggleRequestClearSelectedRows = () => {
		this.setState({requestClearSelectedRows: !this.state.requestClearSelectedRows})
	}

	handleSearchInputClickAway = () => {
		this.requestSearchPopupClose();
	}

	renderConfirmDialogContent = () => {
		const _classSelected = this.state.selection;
		const _class = this.state._class;
		if (_classSelected.length === 1) {
			if (_class) {
				return <p>Tem certeza que deseja <strong>deletar</strong> a sala <b>{this.state._class.name}</b>?</p>;
			}
		} else {
			return <p>Tem certeza que deseja <strong>deletar</strong> as <strong>{_classSelected.length}</strong> salas selecionadas?</p>
		}
	}

	renderDeleteErrors = () => {
    const { deleteError, /* clearDeleteState*/ } = this.props;
		let messages = '';
		if (deleteError) {
      messages = _.flatten(_.values(deleteError.response.errors));
			toastr.removeByType("info");
			toastr.removeByType("success");
			toastr.warning("Erro ao deletar!", `Erros: ${JSON.stringify(messages)}`);
			// toastr.warning("Erro ao deletar!", `Erros: ${JSON.stringify(messages)}`,
			// 							 { component: (
			// 									() => {
			// 										messages.forEach((m) => (
			// 											<span>{ m }</span>
			// 										));
			// 									}
			// 							 ) }
			// 							);
      /* clearDeleteState();*/
		}
	}

	renderDeleteSuccess = () => {
    const { deleteSuccess, /* clearDeleteState*/ } = this.props;
		if (deleteSuccess) {
			toastr.removeByType("info");
			toastr.success(`Sala(s) deletada(s) com sucesso!`);
      /* clearDeleteState();*/
		}
	}

	renderCreateErrors = () => {
    const { createError, /* clearDeleteState*/ } = this.props;
		let messages = '';
		if (createError) {
      messages = _.flatten(_.values(createError.response.errors));
			toastr.removeByType("info");
			toastr.removeByType("success");
			toastr.warning("Erro ao criar!", `Erros: ${JSON.stringify(messages)}`);
			// toastr.warning("Erro ao deletar!", `Erros: ${JSON.stringify(messages)}`,
			// 							 { component: (
			// 									() => {
			// 										messages.forEach((m) => (
			// 											<span>{ m }</span>
			// 										));
			// 									}
			// 							 ) }
			// 							);
      /* clearDeleteState();*/
		}
	}

  render() {
    const _classes = this.props.classesList.classes;
    const {
			activeClass,
			classes,
			loadingClass,
			deleteError,
			deleteSuccess,
		} = this.props;
		const { openSearchPopup, selection } = this.state;

		console.log("selecionados: ", selection);
		// eslint-disable-next-line
		{ deleteError ? this.renderDeleteErrors() : null }
		// eslint-disable-next-line
		{ deleteSuccess ? this.renderDeleteSuccess() : null }

    return (
			 <div>
				<Grid fluid className={classes.headerSection}>
					<HeaderSection
						title={"Turmas"}
						subtitle={"Lista de cadastros"}
					/>
				</Grid>
				<Grid fluid className={classes.container}>
					<Row middle="xs" className={ classes.actionsList }>
						<Col xs={12}>

							<Toolbar style={{paddingLeft: 0}}>
								<Tooltip
									id="tooltip-icon-show" title="Exibir detalhes de turma selecionada" placement="bottom"
									classes={{
										tooltipBottom: classes.tooltip,
									}}
									>
									<div>
										<IconButton disabled={this.actionDisable()} onClick={this._handleShowDetailsClick}>
											<ShowDetailsIcon  />
										</IconButton>
									</div>
								</Tooltip>

								<Tooltip
									id="tooltip-icon-edit" title="Editar última sala selecionada" placement="bottom"
									classes={{
										tooltipBottom: classes.tooltip,
									}}
									>
									<div>
										<IconButton disabled={this.actionDisable()} onClick={this.handleEditClassClick}>
											<EditIcon/>
										</IconButton>
									</div>
								</Tooltip>

								<Tooltip
									id="tooltip-icon-remove" title="Excluir sala(s) selecionada(s)" placement="bottom"
									classes={{
										tooltipBottom: classes.tooltip,
									}}
									>
									<div>
										<IconButton disabled={_.isEmpty( this.state.selection )} onClick={() => this._handleToggleDeleteDialog(this.state.selection)}>
											<DeleteIcon/>
										</IconButton>
									</div>
								</Tooltip>
								<Tooltip
									id="tooltip-icon-search" title="Pesquisar por mais salas cadastradas" placement="bottom"
									classes={{
										tooltipBottom: classes.tooltip,
									}}
									>
									<div>
										<IconButton
											ref={c => (this.target = findDOMNode(c))}
											aria-label="Menu"
											aria-owns={openSearchPopup ? 'input-search' : null}
											aria-haspopup="true"
											onClick={this.handleSearchIconClick}
											>
											<SearchIcon/>
										</IconButton>
									</div>
								</Tooltip>
								<Tooltip
									id="tooltip-icon-reset" title="Resetar informações da tabela. Limpa filtros e/ou seleções." placement="bottom"
									classes={{
										tooltipBottom: classes.tooltip,
									}}
									>
									<div>
										<IconButton
											onClick={this.resetTableState}
											disabled={(_.isEmpty( this.state.searchResults) && _.isEmpty( this.state.selection ))}
											className={classNames({ [classes.iconResetSearch]: !_.isEmpty(this.state.searchResults) || !_.isEmpty(this.state.selection)})}
											>
											{/* <ReplayIcon className={classNames({ [classes.iconResetSearch]: !_.isEmpty(this.state.searchResults)}) }/> */}
											{/* <ReplayIcon className={classNames({ [classes.iconResetSearch]: !_.isEmpty(this.state.searchResults)}) }/> */}
											<ReplayIcon />
										</IconButton>
									</div>
								</Tooltip>

								<Grow
									in={openSearchPopup}
									id="input-search"
									style={{ transformOrigin: '0 0 0' }}
									>
									<Paper className={classNames({[classes.paperSearchDisable]: !openSearchPopup}, classes.paperSearch)}>
										<Input
											name="search"
											label='Nome'
											placeholder='Pesquisar...'
											onChange={this.debouncedSearchClasses(this.tableSearchClasses, 500)}
											classes={{
												root: classes.inputSearchRoot,
												inputSingleline: classes.inputSingleline,
											}}
											ref={c => (this.searchInput = findDOMNode( c ))}
											inputRef={field => this.inputSearchField = field}
											/>
									</Paper>
								</Grow>
							</Toolbar>
						</Col>
					</Row>
					<ClassList
						_classes={_classes ? _classes : {}}
						currentPage={this.state.currentPage}
						fetchMoreClasses={this._fetchMoreClasses}
						tableSearchClasses={this.tableSearchClasses}
						searchResults={this.state.searchResults}
						setSelection={this.setSelection}
						selection={this.state.selection}
						requestClearSelectedRows={this.state.requestClearSelectedRows}
						toggleRequestClearSelectedRows={this.toggleRequestClearSelectedRows}
						deleteDispatched={deleteError || deleteSuccess}
					/>

					{ this.state.deleteDialogOpen ?
						<ConfirmDialog
								title="Confirmar exclusão?"
								content={this.renderConfirmDialogContent()}
								handleCancel={this._handleToggleDeleteDialog.bind(null, '')}
								handleConfirm={this._handleRemoveClassClick.bind(null, this.state._class)}
								open={this.state.deleteDialogOpen}
								buttonActions={
									<Grid fluid>
										<Row between="xs">
											<Button onClick={() => this._handleToggleDeleteDialog(null)}>
												Cancelar
											</Button>
											<Button raised onClick={() => this._handleRemoveClassClick(this.state.selection)} className={classes.buttonWarning}>
												Remover
											</Button>
										</Row>
									</Grid>
								}
						/> : null}
						{ this.state.showDetailsOpen && !loadingClass && activeClass ?
							<DetailsDialog
									title={`Exibindo detalhes:`}
									content={<ClassDetailsContainer _class={activeClass} />}
									handleCancel={this._handleToggleDetailsDialog}
									handleConfirm={this.handleEditClassClick.bind(null, this.state._class)}
									open={this.state.showDetailsOpen}
									buttonActions={
										<Grid fluid>
											<Row between="xs">
												<div className={classes.floatingActions}>
													<div>
														<Tooltip
															id="tooltip-icon-save" title="Editar" placement="bottom"
															classes={{
																tooltipBottom: classes.tooltip,
															}}
														>
															<Button
																fab
																color="primary"
																aria-label="edit"
																onClick={() => this.handleEditClassClick(this.state.selection)}
															>
																<EditIcon />
															</Button>
														</Tooltip>
													</div>
												</div>
											</Row>
										</Grid>
									}
							/> : null}
							{ this.state.formDialogOpen && !loadingClass ?
								<FormDialog
										action={`${ this.state.formDialogTitle }`}
								    fullScreen={true}
										open={this.state.formDialogOpen}
										requestClose={this._handleRequestCloseFormDialog}
										component={ClassFormContainer}
										resourceId={this.props.activeClass ? this.props.activeClass.id : ''}
										//callback={ClassesActions.fetchClasses}
									/> : null }
								<div className={classes.floatingActions}>
									<Tooltip
										id="tooltip-icon-add" title="Adicionar Nova Sala" placement="bottom"
										classes={{
											tooltipBottom: classes.tooltip,
										}}
										>
										<Button
											fab
											color="primary"
											aria-label="add"
											className={classes.addActionButton}
											onClick={this.handleAddClass}
											// onClick={() => toastr.success('The title', 'The message')}
											>
											<AddIcon />
										</Button>
									</Tooltip>
								</div>
				</Grid>
			</div>
		);
  }
}

const mapStateToProps = (state) => (
	{ classesList: state.classes.classesList ? state.classes.classesList : {}
	, loadingClass: state.classes.activeClass ? state.classes.activeClass.loading : false
	, deleteError: state.classes.deletedClass ? state.classes.deletedClass.error : null
	, deleteSuccess: state.classes.deletedClass ? state.classes.deletedClass.success : null
	, activeClass: ( state.classes.activeClass ?
												state.classes.activeClass.class ?
												state.classes.activeClass.class : null
											 : null
										 )
  }
);

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  // fetchClasses: () => dispatch(ClassesActions.fetchClasses(1)),
  fetchClasses: () => dispatch(fetchClasses(1, dispatch)),
  fetchClass: (id) => dispatch(ClassesActions.fetchClass(id)),
	// searchClasses: (term) => dispatch(ClassesActions.tableSearchClasses(term)),
	searchClasses: (term) => dispatch(ClassesActions.tableSearchClasses(term)),
	/* setActiveClass: (_class) => dispatch(setActiveClass(_class)),*/
	/* clearDeleteState: () => dispatch(clearDeleteState()),*/
});

const wrapper = compose(
   connect(mapStateToProps, mapDispatchToProps),
	 withStyles(styles, { withTheme: true }),
);

export default wrapper(ClassesListContainer);
