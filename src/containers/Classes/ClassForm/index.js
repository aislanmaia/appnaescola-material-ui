import React, {Component } from 'react';
import { connect }         from 'react-redux';
import ClassForm           from 'components/Classes/ClassForm';
import { reduxForm }       from 'redux-form';
import { reset }           from 'redux-form';
import Actions             from 'actions/async/classes';
import ClassroomsActions   from 'actions/async/classrooms';
import {
  resetActiveClass,
  resetNewClass,
  resetSelectProfessors,
  resetSelectStudents
}                          from 'actions/sync/classes';
import { resetClassroomsList } from 'actions/sync/classrooms';
	// import { resetProfessorsList } from 'actions/sync/professors';
  // import { resetStudentsList } from 'actions/sync/students';

import { objIsEmpty }      from 'utils/helpers';
import { toastr }          from 'react-redux-toastr';

	//Client side validation
  function validate(values) {
    console.log("chamando validate com values:", values);
    let errors = {};

    if (!values.name) {
      errors.name = 'Forneça um nome';
    }

    return errors;
  }
  /* const asyncValidate = (values) => {*/
  /* return new Promise((resolve, reject) => {*/
  /* console.log("values: ", values);*/
  /* ClassroomsActions.searchClassrooms(values.classrooms)*/
  /* .then((res) => { alert("chamei aqui com: "+ values.classrooms + " e res: "+ res); })*/
  /* })*/
  /* .catch((err) => { throw { classrooms: "Não existente!" } })*/
  /* }
   * */
  // const fetchProfessors = () => {
  //   dispatch(ProfessorActions.fetchProfessors());
  // }

  // const fetchStudents = () => {
  //   dispatch(StudentsActions.fetchStudents());
  // }

  /* const fetchClassrooms = (dispatch) => {
   *   dispatch(ClassroomsActions.fetchClassrooms());
   * }*/

const validateAndCreateClass = (values, dispatch, callback) => {
    values.students = values.students.map((student, index) => (
      student.id.toString()
    ));
    values.studentsAdded = values.studentsAdded.map((student, index) => (
      parseInt( student.id )
    ));
    /* values.studentsRemoved = values.studentsRemoved.map((student, index) => (
		 *   parseInt( student.id )
		 * ));*/
    values.profsAdded = values.profsAdded.map((professor, index) => (
      parseInt( professor.id )
    ));
    /* values.profsRemoved = values.profsRemoved.map((professor, index) => (
		 *   parseInt( professor.id )
		 * ));
		 */
    values.professorIds = {
      added: values.profsAdded,
      removed: [],
      /* removed: values.profsRemoved*/
    }
    values.studentIds = {
      added: values.studentsAdded,
      removed: [],
      /* removed: values.studentsRemoved*/
    }

    delete values.professors;
    delete values.profesAdded;
    delete values.profesRemoved;
    delete values.studentsAdded;
    delete values.studentsRemoved;

    console.log("Valores para salvar:", values);

    return new Promise((resolve, reject) => {
      dispatch(Actions.create(values))
      .then((response) => {
        console.log("promise response", response);
        resolve(); //this is for redux-form itself
        dispatch(reset('ClassForm')); //reset the form
				dispatch(resetNewClass());
				dispatch(resetSelectProfessors());
				dispatch(resetSelectStudents());
				toastr.removeByType("info");
				toastr.success('Turma', values.name + ' criada com sucesso!', { progressBar: false } );
				if (callback) callback();
      });
    });
  };

	const validateAndUpdateClass = (initialValues, values, dispatch) => {
    /* console.log("Valores para atualizar:", values);*/
    console.log("values.students to save", values.students);
    values.students = values.students.map((student, index) => (
      student.id.toString()
    ));
    values.studentsAdded = values.studentsAdded.map((student, index) => (
      parseInt( student.id )
    ));
    values.studentsRemoved = values.studentsRemoved.map((student, index) => (
      parseInt( student.id )
    ));
    values.profsAdded = values.profsAdded.map((professor, index) => (
      parseInt( professor.id )
    ));
    values.profsRemoved = values.profsRemoved.map((professor, index) => (
      parseInt( professor.id )
    ));
    values.professorIds = {
      added: values.profsAdded,
      removed: values.profsRemoved
    }
    values.studentIds = {
      added: values.studentsAdded,
      removed: values.studentsRemoved
    }

    delete values.profesAdded;
    delete values.profesRemoved;
    delete values.studentsAdded;
    delete values.studentsRemoved;
    console.log("Valores para atualizar:", values);
    return new Promise((resolve, reject) => {

      dispatch(Actions.update(initialValues, values))
      .then((response) => {
        resolve(); //this is for redux-form itself
				toastr.removeByType("info");
				toastr.success('Turma', values.name + ' atualizada com sucesso!', { progressBar: false } );
      });
    });
  }

  const getInitialValues = (_class) => {
    if (_class) {
      return {
        id: _class.id,
        name: _class.name,
        classroom_id: _class.classroom_id,
        classroom: _class.classroom,
        /* classroomSelected: _class.classroom ? _class.classroom.name : "",*/
        profsAdded: [],
        profsRemoved: [],
        professors: _class.professors,
        studentsAdded: [],
        studentsRemoved: [],
        students: _class.students
      }
    } else {
      return {
        name: '',
        classroom_id: '',
        classroom: '',
        profsAdded: [],
        profsRemoved: [],
        professors: [],
        studentAdded: [],
        studentsRemoved: [],
        students: []
      }
    }
  }

  const loadResources = (source) => {
    let data = source;
    if (source && !objIsEmpty(source)) {
      if (source.data) {
        data = source.data;
      }
      const result = data.map((item, i) => {
        return {name: item.name, id: item.id};
      })

      return result;
    }
    else {
      return [];
    }
  }


  const mapStateToProps = (state, ownProps) => ({
    router: ownProps.router,
    route: ownProps.route,
    classId: ownProps.resourceId ? ownProps.resourceId : undefined,
    classrooms: loadResources(state.classrooms.classroomsList.classrooms),
		professors: loadResources(state.professors.professorsList.professors),
		selectedProfessors: state.classes.professorsSelected,
		removedProfessors: state.classes.professorsRemoved,
		students: loadResources(state.students.studentsList.students),
		selectedStudents: state.classes.studentsSelected,
		removedStudents: state.classes.removedSelected,
		classroomsError: state.classrooms.classroomsList.error,
		professorsError: state.professors.professorsList.error,
		studentsError: state.students.studentsList.error,
		activeClass: state.classes.activeClass.class,
		initialValues: getInitialValues(state.classes.activeClass.class)
	});

	const mapDispatchToProps = (dispatch, ownProps) => {
		return {
			dispatch,
			// fetchSubject: (id) => dispatch(ActionsSubject.fetchSubject(id)),
			resetActiveClass: () => dispatch(resetActiveClass()),
			resetNewClass: () => dispatch(resetNewClass()),
			resetClassrooms: () => dispatch(resetClassroomsList()),
			// resetProfessors: () => dispatch(resetProfessorsList()),
			// resetStudents: () => dispatch(resetStudentsList()),
			// resetSelectStudents: () => dispatch(resetSelectStudents()),
      // resetSelectProfessors: () => dispatch(resetSelectProfessors()),
      createClass: validateAndCreateClass,
      updateClass: validateAndUpdateClass,
      // fetchProfessors: () => dispatch(ProfessorActions.fetchProfessors()),
      // fetchStudents: () => dispatch(StudentsActions.fetchStudents()),
      //searchClassrooms: (text) => dispatch(ClassroomsActions.searchClassrooms(""))
    }

    // resetMe: () => {
    //     dispatch(resetNewPost());
    //   }
    // dispatchCallAddSubject: data => dispatch(Actions.create(data)),
  };

  export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
    form: 'ClassForm',
      fields: ['name', 'classroom', 'professors', 'students', 'classroom_id'],
      validate,
      /* asyncValidate: ClassroomsActions.searchClassrooms,*/
      /* asyncValidate,*/
      /* asyncBlurFields: [ 'classrooms' ],*/
      enableReinitialize: true
  })(ClassForm));
