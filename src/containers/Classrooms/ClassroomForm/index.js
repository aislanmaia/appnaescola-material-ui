// import React, {Component }	from 'react';
import { connect }					from 'react-redux';
// import ClassroomForm     from 'components/Classrooms/ClassroomForm';
import { reduxForm }				from 'redux-form';
import Actions							from 'actions/async/classrooms';
import {
	createClassroom,
  resetActiveClassroom,
  resetNewClassroom }				from 'actions/sync/classrooms';
import Loadable							from 'containers/Loadable';
import {toastr}             from 'react-redux-toastr';
import uuid                 from 'uuid/v1';

const ClassroomForm = Loadable({loader: () => import("components/Classrooms/ClassroomForm")});

//Client side validation
function validate(values) {
  let errors = {};

  if (!values.name) {
    errors.name = 'Forneça um nome';
  }

  return errors;
}

const validateAndCreateClassroom = (callback, values, dispatch) => {
  console.log("Valores para salvar:", values);
	let classroom = values;
	classroom.uuid = uuid();
  // return new Promise((resolve, reject) => {
		dispatch(Actions.create(classroom))
		// dispatch(createClassroom(classroom, dispatch))
			// .then((response) => {
			// 	console.log("Response", response);
			// // 	// console.log("Err", err);
			// // 	// if (callback) callback();
			// 	resolve(); //this is for redux-form itself
			// // 	toastr.removeByType("info");
			// // 	toastr.success(classroom.name + ' criada com sucesso!', { progressBar: false } );
			// })
		  // .catch((err) => console.log("err", err))
	// });
};

const validateAndUpdateClassroom = (callback, initialValues, values, dispatch) => {
  console.log("Valores para atualizar:", values);
  return new Promise((resolve, reject) => {
		dispatch(Actions.update(initialValues, values))
			.then(
				 (response) => {
					 if (callback) callback();
					 resolve();
					 toastr.removeByType("info");
					 toastr.success('Sala ', values.name + ' atualizada com sucesso!', { progressBar: false } );
				 }
			 // ({ errors }) => {
			 // 	 if (callback) callback();
			 // 	 toastr.removeByType("info");
			 // 	 toastr.success('Sala ', values.name + ' atualizada com sucesso!', { progressBar: false } );
			 // 	 return errors | null
			 // }
		).catch(() => null)
  /* .then((response) => {
   *   if (response[0].errors) {
   *     console.log("houve erro")
   *     throw new SubmissionError({name: "Este nome já foi definido anteriormente", _error: "Update failed!"})
   *     reject(values);
   *   } else
   *    resolve(); //this is for redux-form itself
   * })
   * .catch((err) => {
   *   reject(values);
   * })*/

  });
}


const mapStateToProps = (state, ownProps) => ({
  // router: ownProps.router,
  // route: ownProps.route,
  classroomId: ownProps.resourceId ? ownProps.resourceId : undefined,
  // activeSubject: state.subjects.activeSubject,
  // newSubject: state.subjects.newSubject,
  // initialValues: state.subjects.activeSubject.subject
  initialValues: state.classrooms.activeClassroom.classroom ? state.classrooms.activeClassroom.classroom : {name: ''}
});

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    // fetchSubject: (id) => dispatch(ActionsSubject.fetchSubject(id)),
    resetActiveClassroom: () => dispatch(resetActiveClassroom()),
    resetNewClassroom: () => dispatch(resetNewClassroom()),
    createClassroom: validateAndCreateClassroom,
    updateClassroom: validateAndUpdateClassroom,
		requestSave: ownProps.requestSave,
		requestedSaveForm: ownProps.requestedSaveForm,
  }

  // resetMe: () => {
  //     dispatch(resetNewPost());
  //   }
  // dispatchCallAddSubject: data => dispatch(Actions.create(data)),
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
  form: 'ClassroomForm',
  fields: ['name'],
  validate,
  enableReinitialize: true
})(ClassroomForm));
