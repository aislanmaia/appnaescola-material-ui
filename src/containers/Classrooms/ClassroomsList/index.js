import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import { connect }					from 'react-redux';
import { compose }				  from 'redux';
import { withStyles }			  from 'material-ui/styles';
import classNames           from 'classnames';
import HeaderSection				from 'components/HeaderSection';
import ClassroomsActions		from 'actions/async/classrooms';
import {
	fetchClassrooms,
	clearDeleteState,
	setActiveClassroom,
}                           from 'actions/sync/classrooms';
import {
  TextField,
}                           from 'redux-form-material-ui';
import Loadable							from 'containers/Loadable';
// import ClickAwayListener    from 'material-ui/utils/ClickAwayListener';
import outy                 from 'outy';
import Grow                 from 'material-ui/transitions/Grow';
import { Grid, Row, Col }   from 'react-flexbox-grid';
import styles               from './styles';
import Paper							  from 'material-ui/Paper';
import Toolbar							from 'material-ui/Toolbar';
import Tooltip              from 'material-ui/Tooltip';
import Input								from 'material-ui/Input';
import Button               from 'material-ui/Button';
import IconButton           from 'material-ui/IconButton';
import AddIcon							from 'material-ui-icons/Add';
import EditIcon             from 'material-ui-icons/Edit';
import DeleteIcon           from 'material-ui-icons/Delete';
import SearchIcon           from 'material-ui-icons/Search';
import ReplayIcon           from 'material-ui-icons/Replay';
import _                    from 'lodash';
import {toastr}             from 'react-redux-toastr';

const ClassroomList = Loadable({loader: () => import("components/Classrooms/ClassroomList")});
const ConfirmDialog = Loadable({loader: () => import("components/Dialogs/ConfirmDialog")});
// const FormDialog = Loadable({loader: () => import("components/Dialogs/FormDialog")});
const ResponsiveFormDialog = Loadable({loader: () => import('components/Dialogs/FormDialog/Responsive')});
const ClassroomFormContainer = Loadable({loader: () => import("containers/Classrooms/ClassroomForm")});
const ClassroomDetails = Loadable({loader: () => import("components/Classrooms/ClassroomDetails")});

class ClassroomsListContainer extends Component {

	constructor(props) {
    super(props);

		this.state =
			{	currentPage: 1
			, selection: []
			, searchResults: []
			,	openSearchPopup: false
			, requestClearSelectedRows: false
			, formDialogTitle: ''
			, formDialogOpen: false
			, deleteDialogOpen: false
			}

		this.setSelection = this.setSelection.bind(this);
		this.actionDisable = this.actionDisable.bind(this);
		this.setSearchResultsToState = this.setSearchResultsToState.bind(this);
		this.tableSearchClassrooms = this.tableSearchClassrooms.bind(this);
		this.requestSearchPopupOpen = this.requestSearchPopupOpen.bind(this);
		this.requestSearchPopupClose = this.requestSearchPopupClose.bind(this);
		this.handleSearchInputClickAway = this.handleSearchInputClickAway.bind(this);
		this.handleEditClassroomClick = this.handleEditClassroomClick.bind(this);
		this._handleToggleDeleteDialog = this._handleToggleDeleteDialog.bind(this);
		this.toggleRequestClearSelectedRows = this.toggleRequestClearSelectedRows.bind(this);
	}

	componentDidMount() {
    this._setOusideTap();
  }

  componentDidUpdate(lastProps, lastState) {
    if (lastState.openSearchPopup !== this.state.openSearchPopup) {
      setTimeout(() => this._setOusideTap());
    }
    if (this.inputSearchField && this.state.openSearchPopup) this.inputSearchField.focus();
  }

  componentWillMount() {
    const { dispatch, fetchClassrooms } = this.props;
		if (this.outsideTap) this.outsideTap.remove();
    // dispatch(fetchClassrooms(this.state.currentPage, dispatch));
		fetchClassrooms(this.state.currentPage, dispatch);
	}

	_setOusideTap = () => {
    const elements = [this.target];

    if (this.searchInput) {
      elements.push(this.searchInput);
    }

    if (this.outsideTap) {
      this.outsideTap.remove();
    }

    this.outsideTap = outy(
       elements,
       ['click', 'touchstart'],
       this._handleOutsideTap
    )
  }

	_handleOutsideTap = () => {
    this.setState({ openSearchPopup: false });
	}

  _fetchMoreClassrooms = (page) => {
    const { dispatch } = this.props;
    this.setState({currentPage: page});
    dispatch(ClassroomsActions.fetchClassrooms(page));
  }

	tableSearchClassrooms = (e) => {
    const { searchClassrooms } = this.props;
		console.log("deveria chamar async searchClassrooms com ", e.target.value);
		const term = e.target.value;
		if (!_.isEmpty(term)) {
			searchClassrooms(term)
				.then((res) => this.setSearchResultsToState(res.data))
		} else {
			this.setSearchResultsToState([])
		}
	}

	/* This wrapper function to _.debounce is necessary because
		 the event.target is null if using _.debounce directly and how
		 React treats synthetic event. So, using e.persist() the warning
		 regarding that treatment is cleared up and the event is passed
		 to the inner function.
	 */
	debouncedSearchClassrooms(...args) {
    const debouncedFn = _.debounce(...args);
		return function(e) {
      e.persist();
			return debouncedFn(e);
		};
	}

	actionDisable = () => {
    if (_.isEmpty( this.state.selection ) || _.size( this.state.selection ) > 1) {
			return true;
		} else {
      return false;
		}
	}

	handleSearchIconClick = () => {
		this.requestSearchPopupOpen();
		if (this.searchInput) {
			this.searchInput.focus();
			this.inputSearchField.value = "";
		}
	}

	handleAddClassroom = () => {
    this.setState({formDialogOpen: true});
		this._handleSetActionIntent(`Adicionando Nova Sala`);
	}

  handleEditClassroomClick = (e, classroom) => {
		console.log("Calling handleEditClassroomClick with classroom = ", classroom);
    const { fetchClassroom, setActiveClassroom } = this.props;
		let c;
		if (!classroom) {
      c = this.state.selection[0];
		} else {
      c = classroom;
		}
		fetchClassroom(c.id);
		// this.setState({ classroom: c });
		setActiveClassroom(c);
		this._handleSetActionIntent(`Editando: ${c.name}`);
		this._handleRequestOpenFormDialog();
    // push("/administrativo/materias/editar/" + id);
  }

  _handleRemoveClassroomClick = (classroomsSelected) => {
		console.log("Calling _handleRemoveClassroomClick with classrooms selected", classroomsSelected);
		const { dispatch } = this.props;
    /* dispatch(ClassroomsActions.delete(id));*/
		if (_.isArray(classroomsSelected) && !_.isEmpty(classroomsSelected)) {
			if (_.size(classroomsSelected) === 1) {
				console.log("classroomsSelected equal 1");
				let classroom = classroomsSelected[0];
				if (classroom) {
					dispatch(ClassroomsActions.delete(classroom));
					this.setSelection([], true);
				}
			} else {
				dispatch(ClassroomsActions.batchDelete(classroomsSelected));
				this.setSelection([], true);
			}
 		}
		this._handleToggleDeleteDialog(null);
  }

	_handleSetActionIntent = (actionText) => {
		this.setState({ formDialogTitle: actionText });
	}

	_handleRequestOpenFormDialog = () => {
		this.setState({ formDialogOpen: true });
	}

	_handleRequestOpenFormDialog = () => {
		this.setState({ formDialogOpen: true });
	}

  _handleRequestCloseFormDialog = () => {
    this.setState({ formDialogOpen: false });
  };

	_handleToggleDeleteDialog = (classroomsSelected) => {
		this.setState({deleteDialogOpen: !this.state.deleteDialogOpen});
		if (_.isArray(classroomsSelected) && !_.isEmpty(classroomsSelected)) {
			if (_.size(classroomsSelected) === 1) {
				let classroom = classroomsSelected[0];
				classroom ? this.setState({classroom: classroom}) : this.setState({classroom: ''});
			}
 		}
	}


	setSelection = (selection, replaceAll=false) => {
		/* console.log("selection in setSelection", selection);*/
		let selectionState = this.state.selection;
		let found = undefined;
		if (replaceAll) {
			if (_.isArray(selection))
				this.setState({selection: selection});
			else
				this.setState({selection: [selection]})
		} else {
			if (!_.isEmpty(selection) && !_.isArray(selection)) {
				found = selectionState.findIndex(row => row.id === selection.id) > -1;
				if (found) {
					/* If the element is already in selection state array, remove it
						 from selection state array by filtering out a new array.
					 */
					selectionState = selectionState.filter(row => row.id !== selection.id);
				} else {
					/* The element is new in selection state array */
					selectionState.push(selection);
				}
			} else {
				if (_.isArray(selection)) {
					selectionState = selectionState.concat(selection);
				}
			}
			this.setState({selection: selectionState});
		}
	}

	setSearchResultsToState = (results) => {
    this.setState({searchResults: results});
	}

	requestSearchPopupOpen = () => {
    this.setState({openSearchPopup: true});
	}

	requestSearchPopupClose = () => {
		console.log("calling requestSearchPopupClose")
    this.setState({openSearchPopup: false});
	}

  resetTableState = () => {
		this.resetSearchResults();
		this.resetSelectionInState();
	}

	resetSearchResults = () => {
		this.setSearchResultsToState([]);
	}

	resetSelectionInState = () => {
    this.setState({selection: [] });
		this.setState({requestClearSelectedRows: true});
	}

	toggleRequestClearSelectedRows = () => {
		this.setState({requestClearSelectedRows: !this.state.requestClearSelectedRows})
	}

	handleSearchInputClickAway = () => {
		this.requestSearchPopupClose();
	}

	renderConfirmDialogContent = () => {
		const classroomsSelected = this.state.selection;
		const classroom = this.state.classroom;
		if (classroomsSelected.length === 1) {
			if (classroom) {
				return <p>Tem certeza que deseja <strong>deletar</strong> a sala <b>{this.state.classroom.name}</b>?</p>;
			}
		} else {
			return <p>Tem certeza que deseja <strong>deletar</strong> as <strong>{classroomsSelected.length}</strong> salas selecionadas?</p>
		}
	}

	renderDeleteErrors = () => {
    const { deleteError, clearDeleteState } = this.props;
		let messages = '';
		if (deleteError) {
      messages = _.flatten(_.values(deleteError.response.errors));
			toastr.removeByType("info");
			toastr.removeByType("success");
			toastr.warning("Erro ao deletar!", `Erros: ${JSON.stringify(messages)}`);
			// toastr.warning("Erro ao deletar!", `Erros: ${JSON.stringify(messages)}`,
			// 							 { component: (
			// 									() => {
			// 										messages.forEach((m) => (
			// 											<span>{ m }</span>
			// 										));
			// 									}
			// 							 ) }
			// 							);
      clearDeleteState();
		}
	}

	renderDeleteSuccess = () => {
    const { deleteSuccess, clearDeleteState } = this.props;
		if (deleteSuccess) {
			toastr.removeByType("info");
			toastr.success(`Sala(s) deletada(s) com sucesso!`);
      clearDeleteState();
		}
	}

	renderCreateErrors = () => {
    const { createError, clearDeleteState } = this.props;
		let messages = '';
		if (createError) {
      messages = _.flatten(_.values(createError.response.errors));
			toastr.removeByType("info");
			toastr.removeByType("success");
			toastr.warning("Erro ao criar!", `Erros: ${JSON.stringify(messages)}`);
			// toastr.warning("Erro ao deletar!", `Erros: ${JSON.stringify(messages)}`,
			// 							 { component: (
			// 									() => {
			// 										messages.forEach((m) => (
			// 											<span>{ m }</span>
			// 										));
			// 									}
			// 							 ) }
			// 							);
      clearDeleteState();
		}
	}

  render() {
    const { classrooms } = this.props.classroomsList;
    const {
			classes,
			loadingClassroom,
			deleteError,
			deleteSuccess,
		} = this.props;
		const { openSearchPopup, selection } = this.state;

		console.log("selecionados: ", selection);
		// eslint-disable-next-line
		{ deleteError ? this.renderDeleteErrors() : null }
		// eslint-disable-next-line
		{ deleteSuccess ? this.renderDeleteSuccess() : null }

    return (
			 <div>
				<Grid fluid className={classes.headerSection}>
					<HeaderSection
						title={"Salas de Aula"}
						subtitle={"Lista de cadastros"}
					/>
				</Grid>
				<Grid fluid className={classes.container}>
					<Row middle="xs" className={ classes.actionsList }>
						<Col xs={12}>
							<Toolbar style={{paddingLeft: 0}}>
								<Tooltip
									id="tooltip-icon-edit" title="Editar última sala selecionada" placement="bottom"
									classes={{
										tooltipBottom: classes.tooltip,
									}}
									>
									<div>
										<IconButton disabled={this.actionDisable()} onClick={this.handleEditClassroomClick}>
											<EditIcon/>
										</IconButton>
									</div>
								</Tooltip>

								<Tooltip
									id="tooltip-icon-remove" title="Excluir sala(s) selecionada(s)" placement="bottom"
									classes={{
										tooltipBottom: classes.tooltip,
									}}
									>
									<div>
										<IconButton disabled={_.isEmpty( this.state.selection )} onClick={() => this._handleToggleDeleteDialog(this.state.selection)}>
											<DeleteIcon/>
										</IconButton>
									</div>
								</Tooltip>
								<Tooltip
									id="tooltip-icon-search" title="Pesquisar por mais salas cadastradas" placement="bottom"
									classes={{
										tooltipBottom: classes.tooltip,
									}}
									>
									<div>
										<IconButton
											ref={c => (this.target = findDOMNode(c))}
											aria-label="Menu"
											aria-owns={openSearchPopup ? 'input-search' : null}
											aria-haspopup="true"
											onClick={this.handleSearchIconClick}
											>
											<SearchIcon/>
										</IconButton>
									</div>
								</Tooltip>
								<Tooltip
									id="tooltip-icon-reset" title="Resetar informações da tabela. Limpa filtros e/ou seleções." placement="bottom"
									classes={{
										tooltipBottom: classes.tooltip,
									}}
									>
									<div>
										<IconButton
											onClick={this.resetTableState}
											disabled={(_.isEmpty( this.state.searchResults) && _.isEmpty( this.state.selection ))}
											className={classNames({ [classes.iconResetSearch]: !_.isEmpty(this.state.searchResults) || !_.isEmpty(this.state.selection)})}
											>
											{/* <ReplayIcon className={classNames({ [classes.iconResetSearch]: !_.isEmpty(this.state.searchResults)}) }/> */}
											{/* <ReplayIcon className={classNames({ [classes.iconResetSearch]: !_.isEmpty(this.state.searchResults)}) }/> */}
											<ReplayIcon />
										</IconButton>
									</div>
								</Tooltip>

								<Grow
									in={openSearchPopup}
									id="input-search"
									style={{ transformOrigin: '0 0 0' }}
									>
									<Paper className={classNames({[classes.paperSearchDisable]: !openSearchPopup}, classes.paperSearch)}>
										<Input
											name="search"
											label='Nome'
											placeholder='Pesquisar...'
											onChange={this.debouncedSearchClassrooms(this.tableSearchClassrooms, 500)}
											classes={{
												root: classes.inputSearchRoot,
												inputSingleline: classes.inputSingleline,
											}}
											ref={c => (this.searchInput = findDOMNode( c ))}
											inputRef={field => this.inputSearchField = field}
											/>
									</Paper>
								</Grow>
							</Toolbar>
						</Col>
					</Row>
					<ClassroomList
						classrooms={classrooms ? classrooms : {}}
						currentPage={this.state.currentPage}
						fetchMoreClassrooms={this._fetchMoreClassrooms}
						tableSearchClassrooms={this.tableSearchClassrooms}
						searchResults={this.state.searchResults}
						setSelection={this.setSelection}
						selection={this.state.selection}
						requestClearSelectedRows={this.state.requestClearSelectedRows}
						toggleRequestClearSelectedRows={this.toggleRequestClearSelectedRows}
						deleteDispatched={deleteError || deleteSuccess}
					/>

					{ this.state.deleteDialogOpen ?
						<ConfirmDialog
								title="Confirmar exclusão?"
								content={this.renderConfirmDialogContent()}
								handleCancel={this._handleToggleDeleteDialog.bind(null, '')}
								handleConfirm={this._handleRemoveClassroomClick.bind(null, this.state.classroom)}
								open={this.state.deleteDialogOpen}
								buttonActions={
									<Grid fluid>
										<Row between="xs">
											<Button onClick={() => this._handleToggleDeleteDialog(null)}>
												Cancelar
											</Button>
											<Button raised onClick={() => this._handleRemoveClassroomClick(this.state.selection)} className={classes.buttonWarning}>
												Remover
											</Button>
										</Row>
									</Grid>
								}
						/> : null}
						{ this.state.showDetailsOpen ?
							<ConfirmDialog
									title={`Exibindo detalhes:`}
									content={<ClassroomDetails classroom={this.state.classroom} />}
									handleCancel={this._handleToggleDetailsDialog}
									handleConfirm={this.handleEditClassroomClick.bind(null, this.state.classroom)}
									open={this.state.showDetailsDialog}
									buttonActions={
											<Grid fluid>
													<Row between="xs">
															<Button onClick={this._handleToggleDetailsDialog} color="primary">
																	Cancelar
																</Button>
																<Button onClick={this.handleEditClassroomClick.bind(null)} color="primary">
																		Editar
																	</Button>
														</Row>
												</Grid>
												}
							/> : null}
							{ this.state.formDialogOpen && !loadingClassroom ?
								<ResponsiveFormDialog
										action={`${ this.state.formDialogTitle }`}
										open={this.state.formDialogOpen}
										requestClose={this._handleRequestCloseFormDialog}
										component={ClassroomFormContainer}
										resourceId={this.props.activeClassroom ? this.props.activeClassroom.id : ''}
										//callback={ClassroomsActions.fetchClassrooms}
									/> : null }
								<div className={classes.floatingActions}>
									<Tooltip
										id="tooltip-icon-add" title="Adicionar Nova Sala" placement="bottom"
										classes={{
											tooltipBottom: classes.tooltip,
										}}
										>
										<Button
											fab
											color="primary"
											aria-label="add"
											className={classes.addActionButton}
											onClick={this.handleAddClassroom}
											// onClick={() => toastr.success('The title', 'The message')}
											>
											<AddIcon />
										</Button>
									</Tooltip>
								</div>
				</Grid>
			</div>
		);
  }
}

const mapStateToProps = (state) => (
	{ classroomsList: state.classrooms.classroomsList ? state.classrooms.classroomsList : {}
	, loadingClassroom: state.classrooms.activeClassroom ? state.classrooms.activeClassroom.loading : false
	, deleteError: state.classrooms.deletedClassroom ? state.classrooms.deletedClassroom.error : null
	, deleteSuccess: state.classrooms.deletedClassroom ? state.classrooms.deletedClassroom.success : null
	, activeClassroom: ( state.classrooms.activeClassroom ?
												state.classrooms.activeClassroom.classroom ?
												state.classrooms.activeClassroom.classroom : null
											 : null
										 )
  }
);

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  // fetchClassrooms: () => dispatch(ClassroomsActions.fetchClassrooms(1)),
  fetchClassrooms: () => dispatch(fetchClassrooms(1, dispatch)),
  fetchClassroom: (id) => dispatch(ClassroomsActions.fetchClassroom(id)),
	// searchClassrooms: (term) => dispatch(ClassroomsActions.tableSearchClassrooms(term)),
	searchClassrooms: (term) => dispatch(ClassroomsActions.tableSearchClassrooms(term)),
	setActiveClassroom: (classroom) => dispatch(setActiveClassroom(classroom)),
	clearDeleteState: () => dispatch(clearDeleteState()),
});

const wrapper = compose(
   connect(mapStateToProps, mapDispatchToProps),
	 withStyles(styles, { withTheme: true }),
);

export default wrapper(ClassroomsListContainer);
