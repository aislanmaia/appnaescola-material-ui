import { headerStyles, floatingActions, form } from 'styles/settings';
import { shake } from 'react-animations';

const styles = theme => (
	 Object.assign({}, form(theme), headerStyles(theme), floatingActions, {
		headerSection: {
      ...headerStyles(theme).headerSection,
      marginTop: '-1.5rem',
		},
		container: {
			paddingTop: '6.5rem',
			paddingBottom: '3rem',
		},
		actionsList: {
			padding: '1rem 0',
		},
		tooltip: {
			fontSize: '1rem',
			padding: '.5rem',
			zIndex: 4000,
		},
		popper: {
			// top: '10px !important',
			left: '10px !important',
		},
		popperArrow: {
			borderWidth: '0 10px 10px 10px',
			borderColor: 'transparent transparent #fff transparent',
			top: '-10px',
			left: 'calc(50% - 10px)',
			marginTop: 0,
			marginBottom: 0,
			width: 0,
			height: 0,
			borderStyle: 'solid',
			position: 'absolute',
			margin: 5,
			filter: 'drop-shadow(0 -3px 1px rgba(0, 0, 0, 0.15))',
		},
		openSearchDisabled: {
			pointerEvents: 'none',
		},
		inputSearchRoot: {
			paddingTop: '.5rem',
			width: '100%',
		},
		inputSingleline: { height: '2rem' },
		paperSearch: {
      zIndex: 100,
			position: 'absolute',
			left: '10rem',

			'@media (min-width: 320px) and (max-width: 460px)': {
				left: 0,
				width: '100%',
			},
		},
		paperSearchDisable: {
			pointerEvents: 'none',
		},

		iconResetSearch: {
			/* Ripple magic */
			position: 'relative',
			overflow: 'hidden',

			filter: 'drop-shadow(0 1px 2px rgba(0, 0, 0, .4))',

			'&:after': {
				content: '""',
				'mix-blend-mode': 'multiply',
				position: 'absolute',
				/* top: 8,*/
				/* left: 14,*/
				width: 16,
				height: 16,
				/* background: 'rgba(51, 102, 204, .5)',*/
				background: 'rgba(233, 228, 228, 0.3)',
				opacity: 0,
				borderRadius: '100%',
				transformOrigin: '50% 50%',

				animation: 'ripple 1.2s ease-out infinite',
				animationDelay: '1s',
			},
		},

		'@keyframes ripple': {
			'0%': {
				transform: 'scale(0)',
				opacity: 1,
			},
			'35%': {
				transform: 'scale(0)',
				opacity: 1,
			},
			'50%': {
				transform: 'scale(1.5)',
				opacity: 0.8,
			},
			'100%': {
				opacity: 0,
				transform: 'scale(4)',
			},
		},


    '@keyframes pulsate': {
			/* '0%' :  { boxShadow: '0 0 0 rgba(0, 0, 0, 0)' },
				 '50%':  { boxShadow: '0 0 4px rgba(0, 0, 0, .4)' },
				 '100%': { boxShadow: '0 0 0 rgba(0, 0, 0, 0.5)' },*/
			'0%':	{ transition: 'background-color: 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms' },
			'50%':	{ transition: 'background-color: 350ms cubic-bezier(0, 0, 0, 0.5) 0ms' },
			'100%':	{ transition: 'background-color: 350ms cubic-bezier(0, 0, 0, 1) 1ms' },
		},

		'@keyframes shake': shake,
		shake: {
			animationName: 'shake',
			animationDuration: '1s',
		}
	})
);

export default styles;
