import React, { Component } from 'react';
import { Link }							from 'react-router-dom';
import { connect }					from 'react-redux';
import { compose }					from 'redux';
import { withStyles }       from 'material-ui/styles';
// import BoxCard           from 'components/widgets/box/box_card/BoxCard';
import { Grid, Row, Col }   from 'react-flexbox-grid';
import Card, {
			 CardContent,
			 CardActions }		    from 'material-ui/Card';
import Button               from 'material-ui/Button';
import Typography           from 'material-ui/Typography';
import HeaderSection        from 'components/HeaderSection';
import "./styles.css";
import styles               from './styles';

class AdministrativeView extends Component {

	componentDidMount() {
    console.log("AdministrativeView did mount");
	}

	render() {
		const { classes } = this.props;
		return (
			 <div>
				 <Grid fluid className={classes.headerSection}>
					 <HeaderSection
						 title={"Administrativo"}
						 subtitle={"Painel Inicial"}
						 />
				 </Grid>
				 <Grid fluid className={classes.container}>
					 <div className="administrativeSection">
						 <Row center="sm">
							 <Col sm={6} md={3} center="md" className={classes.container}>
								 <Link ref={el => this.linkStudents = el} to="/administrativo/alunos" className="cardLink hvr-grow">
									 <Card className={classes.card}>
										 <CardContent className={classes.cardContent}>
											 <Row center="xs">
												 <Col xs md>
													 <div className="imageCard">
														 <div className='icon iconStudent'></div>
													 </div>
												 </Col>
											 </Row>
											 <Row center="xs">
												 <Typography type="headline" component="h3">
													 Alunos
												 </Typography>
											 </Row>
											 <Row middle="xs">
												 <Col xs md>
													 <Row middle="xs" style={{height: '80px'}}>
														 <Typography component="p">
															 Gerencie os alunos cadastrados, cadastre informações, pesquise por nomes
															 e visualize todos os alunos já cadastrados.
														 </Typography>
													 </Row>
												 </Col>
											 </Row>
										 </CardContent>
										 <Row center="xs" end="md">
											 <CardActions>
												 <Col xs md>
													 <Button dense color="primary" className={classes.button}>
														 Entrar
													 </Button>
												 </Col>
											 </CardActions>
										 </Row>
									 </Card>
								 </Link>
							 </Col>

							 <Col sm={6} md={3} center="md" className={classes.container}>
								 <Link ref={el => this.linkProfessors = el} to="/administrativo/professores" className="cardLink hvr-grow">
									 <Card className={classes.card}>
										 <CardContent className={classes.cardContent}>
											 <Row center="xs">
												 <Col xs md>
													 <div className="imageCard">
														 <div className='icon iconProfessor'></div>
													 </div>
												 </Col>
											 </Row>
											 <Row center="xs">
												 <Typography type="headline" component="h3">
													 Professores
												 </Typography>
											 </Row>
											 <Row middle="xs" style={{height: '80px'}}>
												 <Typography component="p">
													 Gerencie os professores cadastrados, visualize informações, atribua turmas e matérias.
												 </Typography>
											 </Row>
										 </CardContent>
										 <Row center="xs" end="md">
											 <CardActions>
												 <Col xs md>
													 <Button dense color="primary" className={classes.button}>
														 Entrar
													 </Button>
												 </Col>
											 </CardActions>
										 </Row>
									 </Card>
								 </Link>
							 </Col>

							 <Col sm={6} md={3} center="md" className={classes.container}>
								 <Link ref={el => this.linkClassClassrooms = el} to="/administrativo/turmas-salas" className="cardLink hvr-grow">
									 <Card className={classes.card}>
										 <CardContent className={classes.cardContent}>
											 <Row center="xs">
												 <Col xs md>
													 <div className="imageCard">
														 <div className='icon iconClassClassroom'></div>
													 </div>
												 </Col>
											 </Row>
											 <Row center="xs">
												 <Typography type="headline" component="h3">
													 Turmas e Salas
												 </Typography>
											 </Row>
											 <Row middle="xs" style={{height: '80px'}}>
												 <Typography component="p">
													 Gerencie as turmas e salas, Cada turma com uma correspondente sala.
												 </Typography>
											 </Row>
										 </CardContent>
										 <Row center="xs" end="md">
											 <CardActions>
												 <Col xs md>
													 <Button dense color="primary" className={classes.button}>
														 Entrar
													 </Button>
												 </Col>
											 </CardActions>
										 </Row>
									 </Card>
								 </Link>
							 </Col>

							 <Col sm={6} md={3} center="md" className={classes.container}>
								 <Link ref={el => this.linkSubjects = el} to="/administrativo/materias" className="cardLink hvr-grow">
									 <Card className={classes.card}>
										 <CardContent className={classes.cardContent}>
											 <Row center="xs">
												 <Col xs md>
													 <div className="imageCard">
														 <div className='icon iconSubjects'></div>
													 </div>
												 </Col>
											 </Row>
											 <Row center="xs">
												 <Typography type="headline" component="h3">
													 Matérias
												 </Typography>
											 </Row>
											 <Row middle="xs" style={{height: '80px'}}>
												 <Typography component="p">
													 Gerencie as matérias escolares. Cadastre quais serão utilizadas em sala de aula.
												 </Typography>
											 </Row>
										 </CardContent>
										 <Row center="xs" end="md">
											 <CardActions>
												 <Col xs md>
													 <Button dense color="primary" className={classes.button}>
														 Entrar
													 </Button>
												 </Col>
											 </CardActions>
										 </Row>
									 </Card>
								 </Link>
							 </Col>
						 </Row>
					 </div>
				 </Grid>
			 </div>
		);
	}
}

const mapStateToProps = (state) => (
	{}
);

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const wrapper = compose(
  connect(mapStateToProps, mapDispatchToProps),
	withStyles(styles, { withTheme: true }),
);

export default wrapper(AdministrativeView);
