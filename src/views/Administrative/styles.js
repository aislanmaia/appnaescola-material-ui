import { headerStyles } from 'styles/settings';

const styles = theme => (
	 Object.assign({}, headerStyles(theme), {
		 container: {
			 paddingTop: '4rem',
			 paddingBottom: '3rem',
		 },
		 card: {
			 maxWidth: '22rem',
			 maxHeight: '22rem',
		 },
		 cardContent: {
			 paddingBottom: 0,
		 },
		 button: {
			 minWidth: 90,
			 minHeight: 40,
			 fontSize: '1rem'
		 },
	 })
);

export default styles;
