import React, {Component} from 'react';
import { connect }        from 'react-redux';
import { compose }				from 'redux';
import { withStyles }     from 'material-ui/styles';
import classnames         from 'classnames';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Card, {
	CardContent,
	CardActions }						from 'material-ui/Card';
import Button             from 'material-ui/Button';
import Typography         from 'material-ui/Typography';
import { Link }           from 'react-router-dom';
import HeaderSection      from 'components/HeaderSection';

import styles             from './styles';
import 'hover.css/css/hover.css';

class ClassroomClass extends Component {

	render() {
		const { classes } = this.props;
		return (
			 <div>
				 <Grid fluid className={classes.headerSection}>
					 <HeaderSection
						 title={"Administrativo"}
						 subtitle={"Turmas e Salas"}
						 />
				 </Grid>
				 <Grid fluid className={classes.container}>
					 <div className={ classes.classesClassroomsSection }>
						 <Row around="sm">
							 <Col sm={6} md={3} center="md" className={classes.container}>
								 <Link ref={el => this.classes = el} to="/administrativo/turmas" className={classnames(classes.cardLink, classes["hvr-grow"])}>
									 <Card className={classes.card}>
										 <CardContent className={classes.cardContent}>
											 <Row center="xs">
												 <Col xs md>
													 <div className={ classes.imageCard }>
														 <div className={classnames(classes.icon, classes.iconClasses)}></div>
													 </div>
												 </Col>
											 </Row>
											 <Row center="xs">
												 <Typography type="headline" component="h3">
													 Turmas
												 </Typography>
											 </Row>
											 <Row middle="xs">
												 <Col xs md>
													 <Row middle="xs" center="xs" style={{height: '80px'}}>
														 <Typography component="p">
															 Gerencie as turmas cadastrados, adicione informações, pesquise por nomes
															 e visualize todos os itens relacionados.
														 </Typography>
													 </Row>
												 </Col>
											 </Row>
										 </CardContent>
										 <Row center="xs" end="md">
											 <CardActions>
												 <Col xs md>
													 <Button dense color="primary" className={classes.button}>
														 Entrar
													 </Button>
												 </Col>
											 </CardActions>
										 </Row>
									 </Card>
								 </Link>
							 </Col>

							 <Col sm={6} md={3} center="md" className={classes.container}>
								 <Link ref={el => this.classrooms = el} to="/administrativo/salas" className={classnames(classes.cardLink, classes["hvr-grow"])}>
									 <Card className={classes.card}>
										 <CardContent className={classes.cardContent}>
											 <Row center="xs">
												 <Col xs md>
													 <div className={ classes.imageCard }>
														 <div className={classnames(classes.icon, classes.iconClassrooms)}></div>
													 </div>
												 </Col>
											 </Row>
											 <Row center="xs">
												 <Typography type="headline" component="h3">
													 Salas
												 </Typography>
											 </Row>
											 <Row middle="xs">
												 <Col xs md>
													 <Row middle="xs" center="xs" style={{height: '80px'}}>
														 <Typography component="p">
															 Gerencie as salas cadastrados, registre turmas, pesquise por nomes
															 e visualize todos os itens relacionados.
														 </Typography>
													 </Row>
												 </Col>
											 </Row>
										 </CardContent>
										 <Row center="xs" end="md">
											 <CardActions>
												 <Col xs md>
													 <Button dense color="primary" className={classes.button}>
														 Entrar
													 </Button>
												 </Col>
											 </CardActions>
										 </Row>
									 </Card>
								 </Link>
							 </Col>
						 </Row>
					 </div>

				 </Grid>
			 </div>
		);

	}
};

const mapStateToProps = (state) => (
	{}
);

const mapDispatchToProps = (dispatch) => ({
});

const wrapper = compose(
   connect(mapStateToProps, mapDispatchToProps),
	 withStyles(styles, { withTheme: true }),
);

export default wrapper(ClassroomClass);
