import React, { Component } from 'react';
import { withRouter }       from 'react-router'
import { connect }          from 'react-redux';
import HeaderSection        from 'components/widgets/headerSection';
import cssModules           from 'react-css-modules';
import styles               from './styles.styl';
import SubjectFormContainer from 'containers/subjects/SubjectFormContainer';
import ActionsSubject       from 'actions/async/subjects';
import Loading              from 'components/widgets/loading';

@withRouter
@cssModules(styles, {allowMultiple: true})
class SubjectsFormView extends React.Component {

    componentDidMount() {
      const { dispatch } = this.props;
      const subjectId = this.props.params.id;
      if (subjectId) {
        console.log("loading subject by id", subjectId);
        dispatch(ActionsSubject.fetchSubject(subjectId));
      }

      this.props.router.setRouteLeaveHook(this.props.route, () => {
        if (this.state.unsaved)
          // return this._handleToggleDialog();
          return "Você tem dados ainda não salvos! Tem certeza que deseja sair?";
      })
    }

    state =
    { unsaved: true
    };

    render() {
      const { loadingActiveSubject, loadingNewSubject } = this.props;

      return (
        <div className="subjects-new-section">
          { loadingActiveSubject ? <Loading label="Carregando matéria selecionada ..."/> : "" }
          { loadingNewSubject ? <Loading label="Salvando matéria ..."/> : "" }
          <HeaderSection
            title={this.props.route.name}
            subtitle={"Cadastro"}
            breadcrumbRoutes={this.props.routes}
          />
          <SubjectFormContainer
            router={this.props.router}
            subjectId={
              this.props.params.id ? this.props.params.id : ''
            }
            //dispatch={this.props.dispatch}
          />
        </div>
      );
    }
}

const mapStateToProps = (state) => ({
  loadingActiveSubject: state.subjects.activeSubject.loading,
  loadingNewSubject: state.subjects.newSubject.loading
});

export default connect(mapStateToProps)(SubjectsFormView);
