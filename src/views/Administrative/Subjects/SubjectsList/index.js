import React, { Component }   from 'react';
import { connect }						from 'react-redux';
import HeaderSection					from 'components/HeaderSection';
import SubjectsListContainer	from 'containers/Subjects/SubjectsListContainer';
import SubjectsActions				from 'actions/async/subjects';

class SubjectsListView extends React.Component {

  componentWillMount() {
    const { dispatch, fetchSubjects } = this.props;
    console.log("componente irá chamar fetchSubjects()");
    fetchSubjects();
  }

  _handleAddSubject = () => {
    const { dispatch, history: { push } } = this.props;
    dispatch(push("/administrativo/materias/novo"));
  }

  render() {
    const { subjects } = this.props.subjects;
    return (
      <div className="subjects-section">
        <HeaderSection
          title={"Matérias"}
          subtitle={"Lista de cadastros"}
        />
        <div className="subjects-list">
          <SubjectsListContainer />
        </div>
        {/*<div styleName="footerActions">
          <TooltipButton
            icon='add'
            tooltip='Adicionar Matéria'
            onMouseUp={::this._handleAddSubject}
            floating accent mini />
        </div>
				 */}
      </div>
    );
  }

}

const mapStateToProps = (state) => (
  { subjects: state.subjects.subjectsList
  }
);

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  fetchSubjects: () => dispatch(SubjectsActions.fetchSubjects())
});

export default connect(mapStateToProps, mapDispatchToProps)(SubjectsListView);
