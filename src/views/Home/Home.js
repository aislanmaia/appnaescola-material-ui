import React, {Component} from 'react';
//import cssModules       from 'react-css-modules';
import { connect }        from 'react-redux';
import { push }           from 'react-router-redux';
//import styles           from './styles/Home.styl';
import * as flexboxgrid   from 'flexboxgrid/dist/flexboxgrid.css';
// import file1           from './images/2.png';
import { Link }           from 'react-router';
/* import Layout             from 'react-toolbox/lib/layout/Layout';
 * import Panel              from 'react-toolbox/lib/layout/Panel';
 * import Drawer             from 'react-toolbox/lib/drawer/Drawer';
 * */
import { Grid, Row, Col } from 'react-flexbox-grid';
import styles             from "./Home.styles";
import { withStyles }     from 'material-ui/styles';

/* import Card from 'react-toolbox/lib/card/Card';
 * import CardTitle from 'react-toolbox/lib/card/CardTitle';
 * import CardMedia from 'react-toolbox/lib/card/CardMedia';
 * import CardActions from 'react-toolbox/lib/card/CardActions';
 * import AppBar from 'react-toolbox/lib/app_bar/AppBar';
 * import Actions from 'actions/drawer';
 * */
import UserActions            from 'actions/async/user';

class Home extends Component {
  /* const { todos, dispatchCallAddTodo, dispatchDrawerOpen, isDrawerOpen } = props;
   */
  componentDidMount() {
    const { dispatch, userRole, loginDest } = this.props;
    dispatch(UserActions.verifyUserRole(userRole));
    this._dispatchUserToRouteDestination(loginDest);
  }

  _dispatchUserToRouteDestination(loginDest) {
    const { dispatch } = this.props;
    if (loginDest) {
      dispatch(push(loginDest));
    }
  }

  componentWillReceiveProps(props, nextProps) {
    const { loginDest } = props;
    this._dispatchUserToRouteDestination(loginDest);
  }

  render() {
    console.log("logging in Home component");
    return (
      <Grid fluid>
        <div className={styles.homeSection}>
          <Row around="sm">
            <Col sm={3} md={5} center="sm">
              { "My content" }
              <p>Teste</p>
            </Col>
          </Row>
        </div>
      </Grid>
    );
  }

};

Home.propTypes = {
  // todos: React.PropTypes.array.isRequired,
  // dispatchCallAddTodo: React.PropTypes.func.isRequired,
  // dispatchDrawerOpen: React.PropTypes.func.isRequired,
};

const mapStateToProps = (state) => (
  { loginDest: state.login.urlDest
  , userRole: state.user.role
  }
);
const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

//export default connect(mapStateToProps, mapDispatchToProps)(cssModules(Home, styles));
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true})(Home));
