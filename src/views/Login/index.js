import React																	from "react";
//import styles																from "./styles.styl";
import { connect }														from 'react-redux';
import { compose }														from 'redux';
import { reduxForm, Field, change }						from 'redux-form'; // imported Field
import UserActions														from 'actions/async/user';
import Loading																from 'components/Spinner';
import EditIcon																from 'material-ui-icons/Edit';
import KeyIcon																from 'material-ui-icons/VpnKey';
import CheckIcon															from 'material-ui-icons/Check';
import ErrorIcon															from 'material-ui-icons/Error';
import { withStyles }													from 'material-ui/styles';
// import Input, { InputLabel, InputAdornment }	from 'material-ui/Input';
// import { FormControl, FormHelperText }				from 'material-ui/Form';
import Button																	from 'material-ui/Button';
import { TextField }													from 'redux-form-material-ui';
import styles																	from './styles';

// validation functions
// const required = value => (value == null ? 'Required' : undefined);
// const email = value =>
//   value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
//     ? 'Forneça um endereço de email válido!'
// 		: undefined;

class LoginView extends React.Component {
	componentDidMount() {
    console.log("LoginView did mount");
	}
  constructor(props) {
    super(props);

    this._handleSubmit = this._handleSubmit.bind(this);
  }

  _handleSubmit(values, dispatch) {
    return dispatch(UserActions.loginUser(values))
      .catch((err) => {
				throw err;
      });
  }

  handleChangeEmail = (event) => {
    const { dispatch } = this.props;
    dispatch(change('loginForm', 'email', event.target.value));
  }

  handleChangePassword = (event) => {
    const { dispatch } = this.props;
    dispatch(change('loginForm', 'password', event.target.value));
  }

  handleClassesInput = (field) => {
    const { classes } = this.props;
    return (
      field.meta.touched
      ? (!field.meta.error && field.meta.valid)
       ? {underline: classes.inputUnderline}
       : {}
      : {}
    )
  }

  handleEndAdornment = (field) => {
    const { classes } = this.props;
    return (
    field.meta.touched
            ? (!field.meta.error && field.meta.valid
              ? <CheckIcon className={classes.validIconStatus} />
              : <ErrorIcon className={classes.invalidIconStatus} />)
            : (!field.meta.error && field.meta.valid
              ? <CheckIcon className={classes.validIconStatus} /> : '')
    )
  }

	/* Demonstration how to render a TextField but using its internal components like
	 * FormControl, InputLabel, Input and FormHelperText for more manual control.
	 */
  // renderEmailField = (field) => {
  //   const { classes } = this.props;
  //   return (
  //     <FormControl className={classes.formControl} error={field.meta.touched && field.meta.error ? field.meta.error : ''}>
  //       <EditIcon className={classes.fieldIcon} />
  //       <InputLabel htmlFor="email" className={classes.inputLabel}>Email</InputLabel>
  //       <Input
  //         id="email"
  //         onChange={this.handleChangeEmail}
  //         className={classes.textField}
  //         autoFocus={true}
  //         endAdornment={
  //           field.meta.touched
  //           ? (!field.meta.error && field.meta.valid
  //             ? <CheckIcon className={classes.validIconStatus} />
  //             : <ErrorIcon className={classes.invalidIconStatus} />)
  //           : (!field.meta.error && field.meta.valid
  //             ? <CheckIcon className={classes.validIconStatus} /> : '')
  //         }
  //           classes={this.handleClassesInput(field)}
  //           //classes={{
  //             //inkbar: classes.inkbar,
  //           //  underline: classes.inputUnderline,
  //           //}}
  //         {...field.input}
  //       />
  //       <FormHelperText className={classes.formHelperText}>
  //         {field.meta.touched && field.meta.error ? field.meta.error : ''}
  //       </FormHelperText>
  //     </FormControl>
  //   )
  // }

  renderEmailField = (field) => {
    const { classes } = this.props;
    return (
      <div className={classes.containerField}>
        <EditIcon className={classes.fieldIcon}/>
        <TextField
            type="email"
            style={{textIndent: 0}}
            label="Email"
            placeholder="Digite seu email"
            className={classes.textField}
            inputClassName={classes.input}
            InputProps={{
              endAdornment: this.handleEndAdornment(field),
              classes: this.handleClassesInput(field)
            }}
            autoFocus={true}
            error={field.meta.touched && field.meta.error ? !!field.meta.error : false}
            helperText={field.meta.touched && field.meta.error ? field.meta.error : ''}
            {...field.input}
        />
      </div>
    );
  }

	/* Demonstration how to render a TextField but using its internal components like
	 * FormControl, InputLabel, Input and FormHelperText for more manual control.
	 */
  // renderPasswordField = (field) => {
  //   const { classes } = this.props;
  //   return (
  //     <FormControl className={classes.formControl} error={field.meta.touched && field.meta.error ? field.meta.error : ''}>
  //       <KeyIcon className={classes.fieldIcon} />
  //       <InputLabel htmlFor="password" className={classes.inputLabel}>Senha</InputLabel>
  //       <Input
  //         id="password"
  //         type="password"
  //         //value={this.state.password}
  //         onChange={this.handleChangePassword}
  //         //startAdornment={<InputAdornment position="start">$</InputAdornment>}
  //         className={classes.textField}
  //         endAdornment={
  //           field.meta.touched
  //           ? (!field.meta.error && field.meta.valid
  //             ? <CheckIcon className={classes.validIconStatus} />
  //             : <ErrorIcon className={classes.invalidIconStatus} />)
  //           : (!field.meta.error && field.meta.valid
  //             ? <CheckIcon className={classes.validIconStatus} /> : '')
  //         }
  //         {...field.input}
  //       />
  //       <FormHelperText className={classes.formHelperText}>
  //         {field.meta.touched && field.meta.error ? field.meta.error : ''}
  //       </FormHelperText>
  //     </FormControl>
  //   )
  // }

  renderPasswordField = (field) => {
    const { classes } = this.props;
    return (
      <div className={classes.containerField}>
        <KeyIcon className={classes.fieldIcon}/>
        <TextField
            type="password"
            style={{textIndent: 0}}
            label="Senha"
            placeholder="Digite sua senha"
            className={classes.textField}
            inputClassName={classes.input}
            InputProps={{
              endAdornment: this.handleEndAdornment(field),
              classes:this.handleClassesInput(field)
            }}
            //autoFocus={true}
            error={field.meta.touched && field.meta.error ? !!field.meta.error : false}
            helperText={field.meta.touched && field.meta.error ? field.meta.error : ''}
            {...field.input}
        />
      </div>
    );
  }

  render() {
    const {
			error,
			handleSubmit,
			submitting,
			loadingUser,
			classes,
			// loginError,
		} = this.props;

    return (
      <div>
        { loadingUser ? <Loading label="Verificando informações de usuário ..."/> : ""}
        {error && <strong>{error}</strong>}
        <form onSubmit={handleSubmit(this._handleSubmit)}>
          <Field
              name="email"
              type="email"
              component={this.renderEmailField}
              label="Email"
          />
          <Field
              name="password"
              type="password"
              component={this.renderPasswordField}
              label="Senha"
          />
          <div className={classes.containerField}>
            <Button type="submit" raised color="primary" className={classes.button} disabled={submitting}>
              Entrar
            </Button>
          </div>
          {/* <div>
              <Button
              classes={{
              root: classes.root, // className, e.g. `OverridesClasses-root-X`
              label: classes.label, // className, e.g. `OverridesClasses-label-X`
              }}
              >
              {this.props.children ? this.props.children : 'my button customized'}
              </Button>
              </div> */}
        </form>
      </div>
    )
  }
}

const validate = (values) => {
  let errors = {};
  // const requiredFields = [
  //   'email',
  //   'password',
  // ]
  if (!values.email) {
    errors.email = 'Forneça um email';
  }
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Endereço de email inválido!';
  }
  if (!values.password) {
    errors.password = 'Forneça uma senha';
  }
  return errors;
}

const mapStateToProps = (state, ownProps) => ({
  loadingUser: state.user.loading,
  // loginError: state.login.error,
  loginDest: state.login.urlDest
})

const wrapper = compose(
  connect(mapStateToProps),
  withStyles(styles, {withTheme: true}),
  reduxForm({
    form: 'loginForm',
    fields: ['email', 'password'],
    initialValues: {
      email: '',
      password: '',
    },
    validate
  }),
);

export default wrapper(LoginView);

/* export default connect(mapStateToProps)(reduxForm({
 *   form: 'loginForm',
 *   fields: ['email', 'password'],
 *   validate
 * })(LoginView)*/
