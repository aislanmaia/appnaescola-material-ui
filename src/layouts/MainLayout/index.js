/* eslint-disable flowtype/require-valid-file-annotation */
/* eslint-disable no-restricted-globals */

import React, { Component }       from 'react';
import { BrowserRouter as Router} from 'react-router-dom';
import PropTypes                  from 'prop-types';
import { withStyles }             from 'material-ui/styles';
import { connect }                from 'react-redux';
// import { compose }                from 'redux';
import withRoot                   from 'components/withRoot';
import classNames                 from 'classnames';
import AppBar                     from 'components/AppBar';
import Sidebar                    from 'components/Sidebar';
import UserActions                from 'actions/async/user';
import { Grid, Row, Col }         from 'react-flexbox-grid';
import { UserIsNotAuthenticated } from 'containers/Authorization';
import Loadable                   from 'containers/Loadable';
// import Loadable                from 'react-loadable';
import Routes											from "components/Routes";
import AppliedRoute								from 'components/Routes/AppliedRoute';
import styles											from './index.styles';

const LoginArea = Loadable({loader: () => import("views/Login")});

class MainLayout extends Component {
  constructor(props) {
    super(props);

		this.state = {
			openDrawer: false,
			openRightMenu: false,
			anchorEl: null,
		};
  }

  componentDidMount() {
		console.log("MainLayout mounted");
    this.props.dispatch(UserActions.loggedUser());
  }

  handleRightMenuOpen = (event) => {
    this.setState({ openRightMenu: true });
    this.setState({ anchorEl: event.currentTarget });
  }
  handleRightMenuClose = () => {
    this.setState({ openRightMenu: false });
    this.setState({ anchorEl: null });
  };

  handleDrawerOpen = () => {
    this.setState({ openDrawer: true });
  };

  handleDrawerClose = () => {
    this.setState({ openDrawer: false });
  };

  render() {
    const { classes } = this.props;
    const { openDrawer, openRightMenu } = this.state;
    /* const open = Boolean(this.state.anchorEl);*/

    const currentUser = this.props.user;
    if (!currentUser.email) {
      return (
        <Grid fluid>
          <Row center='xs' middle='xs' className={classes.fullHeight}>
              <Col center="sm" sm={6} md={6}>
								<LoginArea />
								<AppliedRoute exact path="/login" component={ UserIsNotAuthenticated( LoginArea ) } />
              </Col>
          </Row>
        </Grid>
      )
    }
    return (
			 <Router>
				 <div className={classes.root}>
					 <div className={classes.appFrame}>
						 <AppBar
							 openDrawer={openDrawer}
							 openRightMenu={openRightMenu}
							 handleRightMenuClose={this.handleRightMenuClose}
							 handleRightMenuOpen={this.handleRightMenuOpen}
							 handleDrawerOpen={this.handleDrawerOpen}
							 />

						 <Sidebar
							 open={openDrawer}
							 handleDrawerClose={this.handleDrawerClose}
							 />
						 {/*
						 <Grid fluid>
							 <Row xs="12">
								 <header className={classes.header}>OI</header>
							 </Row>
						 </Grid>
						 */}

						 <main className={classNames(classes.content, this.state.openDrawer && classes.contentShift)}>
							 <Routes childProps={{currentUser}} />
						 </main>

					 </div>
				 </div>
			</Router>
		)
	}
};

MainLayout.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => (
  { loginDest: state.login.urlDest
  , user: state.user
  }
);

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

// const wrapper = compose(
//    connect(mapStateToProps, mapDispatchToProps),
// 	 withRoot(
// 			withStyles(styles, {withTheme: true}),
// 	 )
// );

export default connect(mapStateToProps, mapDispatchToProps)( withRoot( withStyles(styles, { withTheme: true })(MainLayout) ) );
// export default wrapper(MainLayout);
