import Constants from 'constants/subjects';
import {toastr} from 'react-redux-toastr';
const API_HOST = process.env.REACT_APP_API_HOST || 'http://localhost:4000';

export const createSubject = (subject) => {
  toastr.info("Salvando matéria ...", { timeOut: 0 });
	return {
		type: Constants.SUBJECT_CREATE,
		payload: {subject},
		meta: {
			offline: {
				effect: { url: `${API_HOST}/api/subjects`,
									opts: {
										method: "POST",
										headers: {
											Accept: "application/json",
											"Content-Type": "application/json"
										},
										body: JSON.stringify({
											subject
										})
									}
								},
				commit: { type: Constants.SUBJECT_CREATE_SUCCESS, meta: {subject} },
				rollback: { type: Constants.SUBJECT_CREATE_FAILURE, meta: {subject} }
			}
		}
	};
};

export const createSubjectSuccess = (data) => ({
  type: Constants.SUBJECT_CREATE_SUCCESS,
  payload: data
});

export const createSubjectFailure = () => ({
  type: Constants.SUBJECT_CREATE_FAILURE,
  payload: {message: "Falhou para salvar a matéria"}
})

export const resetNewSubject = () => ({
  type: Constants.SUBJECT_RESET_NEW
})

export const fetchSubjects = () => ({
  type: Constants.SUBJECTS_FETCH,
	meta: {
    offline: {
			effect: { url: `${API_HOST}/api/subjects`,
								method: 'GET',
								headers: {
									Accept: "application/json",
									"Content-Type": "application/json"
								},
							},
			commit: { type: Constants.SUBJECTS_FETCH_SUCCESS },
			rollback: { type: Constants.SUBJECTS_FETCH_FAILURE }
		}
	}
})

export const fetchSubjectsSuccess = (data) => {
  const resp = data.subjects ? data.subjects : data;
  return {
    type: Constants.SUBJECTS_FETCH_SUCCESS,
    payload: resp
  }
};

export const fetchSubjectsFailure = () => ({
  type: Constants.SUBJECTS_FETCH_FAILURE,
  payload: {message: "Falhou para buscar matérias!"}
});

export const resetSubjectsList = () => ({
  type: Constants.SUBJECTS_RESET_LIST
});

export const fetchSubject = (data) => {
	console.log("fetchSubject param = ", data);
	return {
		type: Constants.SUBJECT_FETCH,
		payload: data,
		meta: {
			offline: {
				effect: { url: `${API_HOST}/api/subjects/${data.id}`, method: 'GET' },
				commit: { type: Constants.SUBJECT_FETCH_SUCCESS, meta: { data } },
				rollback: { type: Constants.SUBJECT_FETCH_FAILURE }
			}
		}
	}
};

export const fetchSubjectSuccess = (subject) => ({
  type: Constants.SUBJECT_FETCH_SUCCESS,
  payload: subject
});

export const searchSubjects = () => ({
  type: Constants.SUBJECTS_SEARCH
})

export const selectSubject = (subject) => ({
  type: Constants.subjects.SUBJECT_SELECT,
  payload: subject
});

/**
 * All fetching related functions to professor's subjects into a specific class
 */

export const fetchProfessorClassSubjects = () => ({
  type: Constants.SUBJECTS_FETCH
});

export const fetchProfessorClassSubjectsSuccess = (data) => ({
  type: Constants.SUBJECTS_FETCH_SUCCESS,
  payload: data
});

export const fetchProfessorClassSubjectsFailure = (err) => ({
  type: Constants.subjects.SUBJECTS_FETCH_FAILURE,
  payload: err
});

export const resetProfessorClassSubjects = (err) => ({
  type: Constants.subjects.SUBJECTS_RESET_LIST
});


export const fetchSubjectFailure = () => ({
  type: Constants.SUBJECT_FETCH_FAILURE,
  payload: {message: "Falhou para buscar a matéria!"}
});

export const resetActiveSubject = () => ({
  type: Constants.SUBJECT_RESET_SUBJECT,
});

export const updateSubject = (oldSubject, newSubject) => {
  toastr.info('Atualizando matéria...', { timeOut: 0 })
  return {
    type: Constants.SUBJECT_UPDATE,
		payload: { oldSubject, newSubject },
		meta: {
			offline: {
				effect: { url: `${API_HOST}/api/subjects/${oldSubject.id}`,
									opts: {
										method: 'PATCH',
										headers: {
											Accept: "application/json",
											"Content-Type": "application/json"
										},
										body: JSON.stringify({subject: newSubject}) },
								},
				commit: { type: Constants.SUBJECT_UPDATE_SUCCESS, meta: {newSubject} },
				rollback: { type: Constants.SUBJECT_UPDATE_FAILURE, meta: {oldSubject} }
			}
		}

  }
};

export const updateSubjectSuccess = (data) => {
  return {
    type: Constants.SUBJECT_UPDATE_SUCCESS,
    payload: data
  }
};

export const updateSubjectFailure = () => ({
  type: Constants.SUBJECT_UPDATE_FAILURE,
  payload: {message: "Falhou para atualizar a matéria"}
});

export const resetUpdateSubject = () => ({
  type: Constants.SUBJECT_RESET_UPDATE
});

export const deleteSubject = () => ({
  type: Constants.SUBJECT_DELETE
});

export const deleteSubjectSuccess = (data) => ({
  type: Constants.SUBJECT_DELETE_SUCCESS,
  payload: data
});

export const deleteSubjectFailure = () => ({
  type: Constants.SUBJECT_DELETE_FAILURE,
  payload: {message: "Falhou para deletar a matéria"}
});

export const resetDeletedSubject = () => ({
  type: Constants.SUBJECTS_RESET_DELETED
});
