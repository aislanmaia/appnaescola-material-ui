/**
 * Sync functions for professors actions such
 * remote API calls and responses, or event UI actions.
 * @name professors.js<sync>
 */
import Constants from 'constants/professors';
import {toastr} from 'react-redux-toastr';

/**
 * All fetching related functions to professor's classes
 */
export const createProfessor = () => {
  toastr.info("Criando professor ...", { timeOut: 0 });
  return {
    type: Constants.PROFESSOR_CREATE,
  };
};

export const createProfessorSuccess = (data) => ({
  type: Constants.PROFESSOR_CREATE_SUCCESS,
  payload: data
});

export const createProfessorFailure = () => ({
  type: Constants.PROFESSOR_CREATE_FAILURE,
  payload: {message: "Falhou para salvar o professor"}
});

export const resetNewProfessor = () => ({
  type: Constants.PROFESSOR_RESET_NEW
});


/**
 * All fetching related functions to professors
 */
export const fetchProfessors = () => ({
  type: Constants.PROFESSORS_FETCH
});

export const fetchProfessorsSuccess = (data) => ({
  type: Constants.PROFESSORS_FETCH_SUCCESS,
  payload: data
});

export const fetchProfessorsFailure = (err) => ({
  type: Constants.PROFESSORS_FETCH_FAILURE,
  payload: err
});


export const searchProfessors = () => ({
  type: Constants.PROFESSORS_SEARCH
});

export const resetProfessorsList = () => ({
  type: Constants.PROFESSORS_RESET_LIST
});


/**
 * All fetching related functions to a single professor
 */
export const fetchProfessor = (id) => ({
  type: Constants.PROFESSOR_FETCH,
  payload: id
});

export const fetchProfessorSuccess = (professor) => ({
  type: Constants.PROFESSOR_FETCH_SUCCESS,
  payload: professor
});

export const fetchProfessorFailure = () => ({
  type: Constants.professors.PROFESSOR_FETCH_FAILURE,
  payload: {message: "Falhou para buscar o professor!"}
});

export const resetActiveProfessor = () => ({
  type: Constants.PROFESSOR_RESET_PROFESSOR,
});


/**
 * All updating related functions to a single professor
 */
export const updateProfessor = () => {
  toastr.info('Atualizando professor ...', { timeOut: 0 });
  return {
    type: Constants.PROFESSOR_UPDATE
  };
};

export const updateProfessorSuccess = (data) => ({
  type: Constants.PROFESSOR_UPDATE_SUCCESS,
  payload: data
});

export const updateProfessorFailure = () => ({
  type: Constants.PROFESSOR_UPDATE_FAILURE,
  payload: {message: "Falhou para atualizar o professor"}
});

export const resetUpdateProfessor = () => ({
  type: Constants.PROFESSOR_RESET_UPDATE
});


/**
 * All deleting related functions to a single professor
 */
export const deleteProfessor = () => {
  toastr.info('Deletando professor ...', { timeOut: 0 });
  return {
    type: Constants.PROFESSOR_DELETE
  };
};

export const deleteProfessorSuccess = (data) => ({
  type: Constants.PROFESSOR_DELETE_SUCCESS,
  payload: data
});

export const deleteProfessorFailure = () => ({
  type: Constants.PROFESSOR_DELETE_FAILURE,
  payload: {message: "Falhou para deletar o professor"}
});

export const resetDeletedProfessor = () => ({
  type: Constants.PROFESSOR_RESET_DELETED
});
