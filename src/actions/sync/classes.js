import Constants from 'constants/classes';
import {toastr} from 'react-redux-toastr';
const API_HOST = process.env.REACT_APP_API_HOST || 'http://localhost:4000';

export const createClass = () => {
  toastr.info("Salvando turma ...", { timeOut: 0 });
  return {
    type: Constants.CLASS_CREATE,
  }
};

export const createClassSuccess = (data) => ({
  type: Constants.CLASS_CREATE_SUCCESS,
  payload: data
});

export const createClassFailure = () => {
  toastr.error("Falhou para salvar a turma");
  return {
    type: Constants.CLASS_CREATE_FAILURE,
    payload: {message: "Falhou para salvar a turma"}
  }
}

export const resetNewClass = () => ({
  type: Constants.CLASS_RESET_NEW
})

export const fetchClasses = (page, dispatch) => {
	console.log("page to request in fetchClasses", page);
	return {
		type: Constants.CLASSES_FETCH,
		meta: {
			offline: {
				effect: { url: `${API_HOST}/api/classes?page=${page}`,
									opts: {
										method: 'GET',
										headers: {
											Accept: "application/json",
											"Content-Type": "application/json"
										},
									}
								},
				commit: { type: Constants.CLASSES_FETCH_SUCCESS },
				rollback: { type: Constants.CLASSES_FETCH_FAILURE }
			}
		}
	}
}

export const fetchClassesSuccess = (data) => ({
  type: Constants.CLASSES_FETCH_SUCCESS,
  payload: data
});

export const fetchClassesFailure = () => {
  toastr.error("Falhou para buscar turmas!");
  return {
    type: Constants.CLASSES_FETCH_FAILURE,
    payload: {message: "Falhou para buscar turmas!"}
  }
};

export const fetchClass = (id) => ({
  type: Constants.CLASS_FETCH,
  payload: id
});

export const fetchClassSuccess = (data) => ({
  type: Constants.CLASS_FETCH_SUCCESS,
  payload: data
});

export const fetchClassFailure = () => {
  toastr.error("Falhou para buscar a turma!");
  return {
    type: Constants.CLASS_FETCH_FAILURE,
    payload: {message: "Falhou para buscar a turma!"}
  }
};

export const fetchClassProfessors = () => {
  return {
    type: Constants.CLASS_PROFESSORS_FETCH
  }
}

export const fetchClassProfessorsSuccess = (data) => {
  return {
    type: Constants.CLASS_PROFESSORS_FETCH_SUCCESS,
    payload: data
  }
}


/**
 * All fetching related functions to professor's classes
 */
export const fetchProfessorClasses = () => ({
  type: Constants.PROFESSOR_CLASSES_FETCH
});

export const fetchProfessorClassesSuccess = (data) => ({
  type: Constants.PROFESSOR_CLASSES_FETCH_SUCCESS,
  payload: data
});

export const fetchProfessorClassesFailure = (err) => ({
  type: Constants.PROFESSOR_CLASSES_FETCH_FAILURE,
  payload: err
});

export const fetchProfessorClassesReset = () => ({
  type: Constants.PROFESSOR_CLASSES_RESET
});

export const fetchClassSubjects = () => {
  return {
    type: Constants.CLASS_SUBJECTS_FETCH
  }
}

export const fetchClassSubjectsSuccess = (subjects, class_id) => {
  return {
    type: Constants.CLASS_SUBJECTS_FETCH_SUCCESS,
    payload: {subjects: subjects.data, id: class_id}
  }
}

export const searchClasses = () => ({
  type: Constants.CLASSES_SEARCH
})

export const resetActiveClass = () => ({
  type: Constants.CLASS_RESET_CLASS,
});

export const updateClass = (oldClass, newClass) => {
  toastr.info("Atualizando turma ...", { timeOut: 0});
  return {
    type: Constants.CLASS_UPDATE,
		payload: {newClass},
		meta: {
			offline: {
				effect: { url: `${API_HOST}/api/classes/${oldClass.id}`,
									opts: {
										method: "PUT",
										headers: {
											Accept: "application/json",
											"Content-Type": "application/json"
										},
										body: JSON.stringify({
											class: newClass
										})
									}
								},
				commit: { type: Constants.CLASS_UPDATE_SUCCESS, meta: {newClass} },
				rollback: { type: Constants.CLASS_UPDATE_FAILURE, meta: {oldClass} }
			}
		}
  }
};

export const updateClassSuccess = (data) => ({
  type: Constants.CLASS_UPDATE_SUCCESS,
  payload: data
});

export const updateClassFailure = () => {
  toastr.error("Falhou para atualizar a turma!");
  return {
    type: Constants.CLASS_UPDATE_FAILURE,
    payload: {message: "Falhou para atualizar a turma"}
  };
};

export const resetUpdateClass = () => ({
  type: Constants.CLASS_RESET_UPDATE
});

export const deleteClass = () => {
  toastr.info('Deletando turma ...', { timeOut: 0 });
  return {
    type: Constants.CLASS_DELETE
  }
};

export const deleteClassSuccess = (data) => ({
  type: Constants.CLASS_DELETE_SUCCESS,
  payload: data
});

export const deleteClassFailure = () => {
  toastr.error("Falhou para deletar a turma!");
  return {
    type: Constants.CLASS_DELETE_FAILURE,
    payload: {message: "Falhou para deletar a turma!"}
  }
};

export const resetDeletedClass = () => ({
  type: Constants.CLASS_RESET_DELETED
});

export const selectProfessors = (professors, profAdded) => {
  toastr.info("Adicionado para a turma o professor: " + profAdded.name);
	console.log("SELECTING PROFESSOR WITH ALL = ", professors);
	console.log("SELECTING PROFESSOR WITH ADDED = ", profAdded);
  return {
    type: Constants.CLASS_SELECT_PROFESSORS,
    payload: {
      all: professors,
      added: profAdded
    }
  }
};

export const resetSelectProfessors = () => ({
  type: Constants.CLASS_RESET_SELECT_PROFESSORS
})

export const selectStudents = (students, studentAdded) => {
  toastr.info("Adicionado para a turma o estudante: " + studentAdded.name);
  return {
    type: Constants.CLASS_SELECT_STUDENTS,
    payload: {
      all: students,
      added: studentAdded
    }
  }
};

export const resetSelectStudents = () => ({
  type: Constants.CLASS_RESET_SELECT_STUDENTS
})

export const removeProfessors = (removedSet) => ({
  type: Constants.CLASS_REMOVE_PROFESSORS,
  payload: removedSet
});

export const removeStudents = (removedSet) => ({
  type: Constants.CLASS_REMOVE_STUDENTS,
  payload: removedSet
});

export const classSubjectSelect = (subject) => ({
  type: Constants.CLASS_SUBJECT_SELECT,
  payload: {...subject, professor: ''}
});

export const updateClassSubjects = () => {
  toastr.info("Salvando alterações da grade de matérias!!");
  return {
    type: Constants.CLASS_SUBJECTS_SAVE
  };
};

export const selectClass = (classSelected) => {
  return {
    type: Constants.CLASS_SELECT,
    payload: { ...classSelected }
  }
}

export const setSubjectsForClass = (class_id, subjects) => {
  return {
    type: Constants.CLASS_SUBJECTS_SET,
    payload: { class_id, subjects }
  }
}

/* Actions to get the count of classes saved */
export const countClass = () => ({
  type: Constants.CLASS_COUNT
})

export const countClassSuccess = (data) => ({
  type: Constants.CLASS_COUNT_SUCCESS,
  payload: data
})

export const countClassFailure = (err) => ({
  type: Constants.CLASS_COUNT_FAILURE,
  payload: {message: err}
})


