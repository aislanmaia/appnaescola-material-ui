import Constants from 'constants/classrooms';
import {toastr} from 'react-redux-toastr';
const API_HOST = process.env.REACT_APP_API_HOST || 'http://localhost:4000';

export const createClassroom = (classroom, dispatch) => {
  toastr.info("Salvando sala ...", { timeOut: 0 });
  console.log("classroom to save", classroom);
  return {
    type: Constants.CLASSROOM_CREATE,
		payload: {classroom},
		meta: {
			offline: {
				effect: { url: `${API_HOST}/api/classrooms`,
									dispatch: dispatch,
									opts: {
										method: "POST",
										headers: {
											Accept: "application/json",
											"Content-Type": "application/json"
										},
										body: JSON.stringify({
											classroom
										})
									}
								},
				commit: { type: Constants.CLASSROOM_CREATE_SUCCESS, meta: {classroom} },
				rollback: { type: Constants.CLASSROOM_CREATE_FAILURE, meta: {classroom} }
			}
		}
  };
};

export const createClassroomSuccess = (data) => ({
  type: Constants.CLASSROOM_CREATE_SUCCESS,
  payload: data
});

export const createClassroomFailure = (err) => {
  toastr.error("Falhou para salvar a sala!\n" + err.errors.name);
  return {
    type: Constants.CLASSROOM_CREATE_FAILURE,
    payload: {message: "Falhou para salvar a sala"}
  };
}

export const resetNewClassroom = () => ({
  type: Constants.CLASSROOM_RESET_NEW
})

export const fetchClassrooms = (page, dispatch) => {
	console.log("dispatch from sync action", dispatch);
	return {
  type: Constants.CLASSROOMS_FETCH,
	meta: {
    offline: {
			effect: { url: `${API_HOST}/api/classrooms?page=${page}`,
								method: 'GET',
								headers: {
									Accept: "application/json",
									"Content-Type": "application/json"
								},
							},
			commit: { type: Constants.CLASSROOMS_FETCH_SUCCESS },
			rollback: { type: Constants.CLASSROOMS_FETCH_FAILURE }
		}
	}
	}
}

export const fetchClassroomsSuccess = (data) => ({
  type: Constants.CLASSROOMS_FETCH_SUCCESS,
  payload: data
});

export const fetchClassroomsFailure = (err) => {
  toastr.error("Falhou para carregar as salas!");
  return {
    type: Constants.CLASSROOMS_FETCH_FAILURE,
    payload: err
  }
};

export const searchClassrooms = () => ({
  type: Constants.CLASSROOMS_SEARCH
});


export const searchClassroomsSuccess = (data) => ({
  type: Constants.CLASSROOMS_SEARCH_SUCCESS,
	payload: data
});

export const searchClassroomsFailure = (err) => ({
  type: Constants.CLASSROOMS_SEARCH_FAILURE,
	payload: err
});

export const fetchClassroom = (id) => ({
  type: Constants.CLASSROOM_FETCH,
	meta: {
    offline: {
			effect: { url: `${API_HOST}/api/classrooms/${id}`,
								method: 'GET',
								headers: {
									Accept: "application/json",
									"Content-Type": "application/json"
								},
							},
			commit: { type: Constants.CLASSROOM_FETCH_SUCCESS },
			rollback: { type: Constants.CLASSROOM_FETCH_FAILURE }
		}
	}
});

export const fetchClassroomSuccess = (classroom) => ({
  type: Constants.CLASSROOM_FETCH_SUCCESS,
  payload: classroom
});

export const fetchClassroomFailure = () => {
  toastr.error("Falhou para carregar a sala!");
  return {
    type: Constants.CLASSROOM_FETCH_FAILURE,
    payload: {message: "Falhou para buscar a sala!"}
  }
 };

export const resetActiveClassroom = () => ({
  type: Constants.CLASSROOM_RESET_CLASSROOM,
});

export const resetClassroomsList = () => ({
  type: Constants.CLASSROOMS_RESET_LIST
});

export const updateClassroom = (oldClassroom, newClassroom) => {
  toastr.info("Atualizando sala ...", { timeOut: 0});
  return {
    type: Constants.CLASSROOM_UPDATE,
		payload: {newClassroom},
		meta: {
			offline: {
				effect: { url: `${API_HOST}/api/classrooms/${oldClassroom.id}`,
									opts: {
										method: "PUT",
										headers: {
											Accept: "application/json",
											"Content-Type": "application/json"
										},
										body: JSON.stringify({
											classroom: newClassroom
										})
									}
								},
				commit: { type: Constants.CLASSROOM_UPDATE_SUCCESS, meta: {newClassroom} },
				rollback: { type: Constants.CLASSROOM_UPDATE_FAILURE, meta: {oldClassroom} }
			}
		}
  }
};

export const updateClassroomSuccess = (data) => ({
  type: Constants.CLASSROOM_UPDATE_SUCCESS,
  payload: data
});

export const updateClassroomFailure = (err) => {
  toastr.error("Falhou para atualizar a sala!\n" + err.errors.name);
  return {
    type: Constants.CLASSROOM_UPDATE_FAILURE,
    payload: {message: "Falhou para atualizar a sala"}
  }
};

export const resetUpdateClassroom = () => ({
  type: Constants.CLASSROOM_RESET_UPDATE
});

export const deleteClassroom = (classroom) => {
  toastr.info('Deletando sala(s) ...', { timeOut: 0 });
  return {
    type: Constants.CLASSROOM_DELETE,
		payload: {classroom},
		meta: {
			offline: {
				effect: { url: `${API_HOST}/api/classrooms/${classroom.id}`,
									opts: {
										method: "DELETE",
										headers: {
											Accept: "application/json",
											"Content-Type": "application/json"
										}
									}
								},
				commit: { type: Constants.CLASSROOM_DELETE_SUCCESS, meta: {classroom} },
				rollback: { type: Constants.CLASSROOM_DELETE_FAILURE, meta: {classroom} }
			}
		}
  }
};

export const deleteClassroomSuccess = (data) => ({
  type: Constants.CLASSROOM_DELETE_SUCCESS,
  payload: data
});

export const deleteClassroomFailure = () => {
  toastr.error("Falhou para deletar a sala!");
  return {
    type: Constants.CLASSROOM_DELETE_FAILURE,
    payload: {message: "Falhou para deletar a sala"}
  }
};

export const resetDeletedClassroom = () => ({
  type: Constants.CLASSROOM_RESET_DELETED
});

export const batchDelete = (classrooms) => {
	toastr.info('Deletando salas selecionadas', { timeOut: 0 });
	return {
		type: Constants.CLASSROOMS_BATCH_DELETE,
		payload: {classrooms},
		meta: {
			offline: {
				effect: { url: `${API_HOST}/api/classrooms/batch_delete/`,
									opts: {
										method: "POST",
										headers: {
											Accept: "application/json",
											"Content-Type": "application/json"
										},
										body: JSON.stringify({
											classrooms
										})
									}
								},
				commit: { type: Constants.CLASSROOMS_BATCH_DELETE_SUCCESS, meta: {classrooms} },
				rollback: { type: Constants.CLASSROOMS_BATCH_DELETE_FAILURE, meta: {classrooms} },
			}
		}
	}
};

export const clearDeleteState = () => {
  return {
    type: Constants.CLASSROOMS_CLEAR_DELETE_STATE,
	}
}

export const setActiveClassroom = (classroom) => ({
	type: Constants.CLASSROOM_SET_ACTIVE_CLASSROOM,
	payload: { classroom }
})
