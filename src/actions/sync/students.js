import Constants from 'constants/students';
import {toastr} from 'react-redux-toastr';

export const createStudent = () => ({
  type: Constants.STUDENT_CREATE,
});

export const createStudentSuccess = (data) => ({
  type: Constants.STUDENT_CREATE_SUCCESS,
  payload: data
});

export const createStudentFailure = () => ({
  type: Constants.STUDENT_CREATE_FAILURE,
  payload: {message: "Falhou para salvar o aluno"}
})

export const resetNewStudent = () => ({
  type: Constants.STUDENT_RESET_NEW
})

export const addResponsible = () => ({
  type: Constants.STUDENT_ADD_RESPONSIBLE
})

export const addResponsibleSuccess = (data) => ({
  type: Constants.STUDENT_ADD_RESPONSIBLE_SUCCESS,
  payload: data
})

export const addResponsibleFailure = (message) => ({
  type: Constants.STUDENT_ADD_RESPONSIBLE_FAILURE,
  payload: message
})

export const addResponsibleReset = () => ({
  type: Constants.STUDENT_RESET_ADD_RESPONSIBLE
})

export const fetchStudents = () => ({
  type: Constants.STUDENTS_FETCH
})

export const fetchStudentsSuccess = (data) => ({
  type: Constants.STUDENTS_FETCH_SUCCESS,
  payload: data
});

export const fetchStudentsFailure = (data) => ({
  type: Constants.STUDENTS_FETCH_FAILURE,
  payload: {message: data.message}
});

export const searchStudents = () => ({
  type: Constants.STUDENTS_SEARCH
});

export const resetStudentsList = () => ({
  type: Constants.STUDENTS_RESET_LIST
});

export const fetchStudent = (id) => ({
  type: Constants.STUDENT_FETCH,
  payload: id
});

export const fetchStudentSuccess = (student) => ({
  type: Constants.STUDENT_FETCH_SUCCESS,
  payload: student
});

export const fetchStudentFailure = () => ({
  type: Constants.STUDENT_FETCH_FAILURE,
  payload: {message: "Falhou para buscar o aluno!"}
});

export const resetActiveStudent = () => ({
  type: Constants.STUDENT_RESET_STUDENT,
});

export const updateStudent = () => {
  toastr.info('Atualizando aluno ...', { timeOut: 0 })
  return {
    type: Constants.STUDENT_UPDATE
  }
};

export const updateStudentSuccess = (data) => ({
  type: Constants.STUDENT_UPDATE_SUCCESS,
  payload: data
});

export const updateStudentFailure = () => ({
  type: Constants.STUDENT_UPDATE_FAILURE,
  payload: {message: "Falhou para atualizar o aluno"}
});

export const resetUpdateStudent = () => ({
  type: Constants.STUDENT_RESET_UPDATE
});

export const deleteStudent = () => {
  toastr.info('Deletando aluno ...', { timeOut: 0 })
  return {
    type: Constants.STUDENT_DELETE
  }
};

export const deleteStudentSuccess = (data) => ({
  type: Constants.STUDENT_DELETE_SUCCESS,
  payload: data
});

export const deleteStudentFailure = (data) => ({
  type: Constants.STUDENT_DELETE_FAILURE,
  payload: data
});

export const resetDeletedStudent = () => ({
  type: Constants.STUDENT_RESET_DELETED
});

export const deleteStudentResponsible = () => ({
  type: Constants.STUDENT_DELETE_RESPONSIBLE
});

export const deleteStudentResponsibleSuccess = (data) => ({
  type: Constants.STUDENT_DELETE_RESPONSIBLE_SUCCESS,
  payload: data
});

export const deleteStudentResponsibleFailure = (data) => ({
  type: Constants.STUDENT_DELETE_RESPONSIBLE_FAILURE,
  payload: {message: data.message}
});

export const deleteStudentResponsibleReset = () => ({
  type: Constants.STUDENT_RESET_DELETE_RESPONSIBLE
});
