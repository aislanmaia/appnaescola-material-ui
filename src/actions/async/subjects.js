/*
 * action creators
 */
import { toastr } from 'react-redux-toastr';
import * as sync from 'actions/sync/subjects';
import { objIsEmpty } from 'utils/helpers';
const API_HOST = process.env.REACT_APP_API_HOST || 'http://localhost:4000';


export const middleware = ({url, dispatch, opts}, action) => {
  if (action.type === "SUBJECTS_FETCH") {
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				return Promise.resolve(res);
			})
			.catch(err => {
        // effect.dispatch(fetchClassroomsFailure(err));
				return Promise.reject(err);
			})
	}

  if (action.type === "SUBJECT_FETCH") {
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				return res;
			})
			.catch(err => {
        // effect.dispatch(fetchClassroomsFailure(err));
				return err;
			})
	}

  if (action.type === "SUBJECT_CREATE") {
		console.log("Branch CREATE")
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				// return Promise.resolve(res);
				if (res.errors) {
					// Promise.reject(res);
					return res;
				} else {
					// Promise.resolve(res);
					return res;
				}
			})
			.catch(err => {
				console.log("catching error: ", err);
        // effect.dispatch(fetchClassroomsFailure(err));
				Promise.reject(err);
			})
	}

  if (action.type === "SUBJECT_UPDATE") {
		console.log("Branch UPDATE")
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				if (res.errors) {
					return res;
				} else {
					return res;
				}
			})
			.catch(err => {
				console.log("catching error: ", err);
				return err;
			})
	}

  if (action.type === "SUBJECT_DELETE") {
		console.log("Branch DELETE")
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				// return Promise.resolve(res);
				if (res.errors) {
					Promise.reject(res);
				} else {
					Promise.resolve(res);
				}
			})
			.catch(err => {
				console.log("logging from catch", err);
        // effect.dispatch(fetchClassroomsFailure(err));
				Promise.reject(err);
			})
	}
}

const Actions = {
  fetchSubjects: () => {
		console.log("loggin from async fetchSubjects()")
    return async dispatch => {
      dispatch(sync.fetchSubjects(dispatch));
      // fetch(`${API_HOST}/api/subjects`, {
      //   method: "GET",
      //   headers: {
      //     Accept: "application/json",
      //     "Content-Type": "application/json"
      //   }
      // })
      // .then((res) => { return res.json() })
      // .then((res) => {
      //   console.log("res", res);
      //   dispatch(sync.fetchSubjectsSuccess(res));
      // })
      // .catch((err) => {
      //   console.log("err", err);
      //   dispatch(sync.fetchSubjectsFailure());
      // })
    }
  },

  fetchSubject: (subject) => {
    return async dispatch => {
      dispatch(sync.fetchSubject({ data: subject }));
      // fetch(`${API_HOST}/api/subjects/${subject.id}`, {
      //   method: "GET",
      //   headers: {
      //     Accept: "application/json",
      //     "Content-Type": "application/json"
      //   }
      // })
      // .then((res) => { return res.json() })
      // .then((res) => {
      //   console.log("res", res);
      //   dispatch(sync.fetchSubjectSuccess(res));
      // })
      // .catch((err) => {
      //   console.log("err", err);
      //   dispatch(sync.fetchSubjectFailure());
      // })
    }
  },

  searchSubjects: (term) => {
    console.log("term", term);
    return async dispatch => {
      dispatch(sync.searchSubjects());
      return fetch(`${API_HOST}/api/search/subjects/${term}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        if (objIsEmpty(res.data))
          throw {message: "Matéria não encontrada ou inexistente!"}; // eslint-disable-line no-throw-literal
        else
          dispatch(sync.fetchSubjectsSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.fetchSubjectsFailure(err));
        throw {message: "Matéria não encontrada ou inexistente!"}; // eslint-disable-line no-throw-literal
      })
    }
  },

  create: (subject) => {
    return async ( dispatch, getState ) => {
      dispatch(sync.createSubject(subject));
			const { subject: newSubject, error } = getState().subjects.newSubject;
			if (newSubject && !error) {
				toastr.removeByType('info');
				toastr.success(subject.name + ' criada com sucesso!', { progressBar: false } );
				Promise.resolve(newSubject);
			} else {
				Promise.reject(error);
			}
      // fetch(`${API_HOST}/api/subjects`, {
      //   method: "POST",
      //   headers: {
      //     Accept: "application/json",
      //     "Content-Type": "application/json"
      //   },
      //   body: JSON.stringify({
      //     subject
      //   })
      // })
      // .then((res) => { return res.json() })
      // .then((res) => {
      //   console.log("subject", res);
      //   dispatch(sync.createSubjectSuccess(subject));
      //   toastr.removeByType("info");
      //   toastr.success('Nova Matéria', subject.name + ' criada com sucesso!', { progressBar: false } );
      // })
      // .catch((err) => {
      //   console.log("err", err);
      //   dispatch(sync.createSubjectFailure());
      // })
    }
  },

  update: (oldSubject, newSubject) => {
		console.log("loggin from async update subject");
    return async dispatch => {
      dispatch(sync.updateSubject(oldSubject, newSubject));
      // fetch(`${API_HOST}/api/subjects/${subject.id}`, {
      //   method: "PATCH",
      //   headers: {
      //     Accept: "application/json",
      //     "Content-Type": "application/json"
      //   },
      //   body: JSON.stringify({
      //     subject
      //   })
      // })
      // .then((res) => { return res.json() })
      // .then((res) => {
      //   console.log("res", res);
      //   dispatch(sync.updateSubjectSuccess(res))
      //   toastr.removeByType("info");
      //   toastr.success('Matéria', subject.name + ' atualizada com sucesso!', { progressBar: false } );
      // })
      // .catch((err) => {
      //   console.log("err", err);
      //   dispatch(sync.updateSubjectFailure());
      // })
    }
  },

  delete: (subject) => {
    return async dispatch => {
      dispatch(sync.deleteSubject());
      fetch(`${API_HOST}/api/subjects/${subject.id}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.deleteSubjectSuccess(subject));
        toastr.success('Matéria', subject.name + ' deletada com sucesso!', { progressBar: false } );
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.deleteSubjectFailure()); // eslint-disable-line no-throw-literal
      })
    }
  }

};

export default Actions;
