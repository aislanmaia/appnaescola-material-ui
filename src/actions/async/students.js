import * as sync from 'actions/sync/students';
/* import * as responsiblesSync from 'actions/sync/responsibles';*/
/* import responsiblesAsync from 'actions/async/responsibles';*/
import { pick, pickStartWith, objHasData, objIsEmpty } from 'utils/helpers';
import _ from 'lodash';
import { toastr } from 'react-redux-toastr';
const API_HOST = process.env.REACT_APP_API_HOST || 'http://localhost:4000';


export const middleware = ({url, dispatch, opts}, action) => {
  if (action.type === "STUDENTS_FETCH") {
		console.log("Branch STUDENTS_FETCH");
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				return Promise.resolve(res);
			})
			.catch(err => {
				return Promise.reject(err);
			})
	}


  if (action.type === "STUDENTS_SEARCH") {
		console.log("Branch STUDENTS_SEARCH");
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				return Promise.resolve(res);
			})
			.catch(err => {
				return Promise.reject(err);
			})
	}


  if (action.type === "STUDENT_FETCH") {
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				return res;
			})
			.catch(err => {
				return err;
			})
	}

}

const Actions = {
  fetchStudents: () => {
    return async dispatch => {
      dispatch(sync.fetchStudents());
      fetch(`${API_HOST}/api/students`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.fetchStudentsSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.fetchStudentsFailure(err));
      })
    }
  },

  fetchStudent: (id) => {
    return async dispatch => {
      dispatch(sync.fetchStudent());
      fetch(`${API_HOST}/api/students/${id}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.fetchStudentSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.fetchStudentFailure());
      })
    }
  },


  searchStudents: (term, options) => {
    console.log("term", term);
    console.log("options", options);
    return async dispatch => {
      dispatch(sync.searchStudents());
      return fetch(`${API_HOST}/api/search/students/`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          term: term, options: options
        })
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        if (objIsEmpty(res.data))
          throw {message: "Nenhum aluno disponível ou inexistente com os termos de pesquisa!"}
        else
          dispatch(sync.fetchStudentsSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.fetchStudentsFailure(err));
        throw {message: "Nenhum aluno disponível ou inexistente com os termos de pesquisa!"}
      })
    }
  },

  create: (data) => {
    return async dispatch => {
      dispatch(sync.createStudent());
      /* const [student, resp1, resp2] = prepareFieldsToSave(data);*/
      /* console.log("student", student);*/
      const student = data;
      /* console.log(student);
       */
      fetch(`${API_HOST}/api/students/`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          student
        })
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.createStudentSuccess(res));
        toastr.success('Novo Aluno(a)', student.name + ' criado com sucesso!', { progressBar: false } );
        /* console.log("resp1", resp1);*/
        /* console.log("resp2", resp2);*/
        /* dispatchResponsibles(resp1, resp2, res.data.id, res.data.name, dispatch, "create");*/
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.createStudentFailure());
      })
    }
  },

  update: (data) => {
    return async dispatch => {
      dispatch(sync.updateStudent());
      let [student, resp1, resp2] = prepareFieldsToSave(data);

      fetch(`${API_HOST}/api/students/${student.id}`, {
        method: "PATCH",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          student
        })
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.updateStudentSuccess(res))
        toastr.removeByType("info");
        toastr.success('Aluno', student.name + ' atualizado com sucesso!', { progressBar: false } );
        /* dispatchResponsibles(resp1, resp2, student.id, student.name, dispatch, "update");*/
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.updateStudentFailure(err));
      })
    }
  },

  addResponsible: (responsible) => {
    return async dispatch => {
      dispatch(sync.addResponsible());
      /* if (result) {
			 *   dispatch(sync.addResponsibleSuccess(responsible));
			 * } else {
			 *   console.log("result from server: ", result);
			 *   dispatch(sync.addResponsibleFailure());
			 * }*/
    }
  },

  delete: (student) => {

    return async dispatch => {
      dispatch(sync.deleteStudent());
      /* let deletedResponsibleResult = null;
       * deletedResponsibleResult = dispatch(Actions.deleteResponsible(student, student.responsibles[0]));
       * deletedResponsibleResult = dispatch(Actions.deleteResponsible(student, student.responsibles[1]));
       */
      fetch(`${API_HOST}/api/students/${student.id}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.deleteStudentSuccess(res));
        toastr.removeByType("info");
        toastr.success('Aluno', student.name + ' deletado com sucesso!', { progressBar: false } );
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.deleteStudentFailure(err));
      })
    }
  }

};

export default Actions;

const prepareFieldsToSave = (data) => {
  console.log("data", data);
  let student = {};
  data.responsibles = [];
  if (objHasData(data.resp1)) {
    data.responsibles[0] = data.resp1;
  }
  if (objHasData(data.resp2)) {
    data.responsibles[1] = data.resp2;
  }
  /* If updating student it will have id */
  if (data.id) {
    student = pick(
      data,
      "id", "name", "birthdate", "phone", "email", "class_id", "grade", "sex", "responsibles"
    )
  } else {
    student = pick(
      data,
      "name", "birthdate", "phone", "email", "class_id", "grade", "sex"
    )
  }

  const resp1 = pickStartWith(data, 'resp1');
  const resp2 = pickStartWith(data, 'resp2');

  return [student, resp1, resp2];
}

/* const dispatchResponsibles = (resp1, resp2, studentId, studentName, dispatch, method) => {
 *   if (objHasData(resp1)) {
 *     dispatch(responsiblesAsync[method]({...resp1, studentId: studentId }))
 *   }
 *   if (objHasData(resp2)) {
 *     dispatch(responsiblesAsync[method]({...resp2, dependents: [{id: studentId, name: studentName}] }))
 *   }
 * }*/
