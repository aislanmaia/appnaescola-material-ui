/*
 * action creators
 */

import Constants from 'constants/user';
import { SubmissionError } from 'redux-form';
// import * as sync from 'actions/sync/users';
const API_HOST = process.env.REACT_APP_API_HOST || 'http://localhost:4000';

const Actions = {
  loginUser: (user) => {
    return async dispatch => {
      dispatch({ type: Constants.USER_LOGGING });
      return fetch(`${API_HOST}/auth/identity/callback`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: user.email,
          password: user.password
        })
      })
			.then((res) => { return res.json(); })
      .then((res) => {
        if (res.error) {
          if (res.error === "invalid password") {
						dispatch({
							type: "LOGIN_FAILED",
							payload: {email: "Combinação inválida de email e senha!", _error: "Login Falhou!", password: "Combinação inválida de email e senha!"}
						});
						dispatch({
							type: Constants.USER_LOGIN_FAILED
						});
						return Promise.reject(res.error);
					}
          return;

        } else {

          /* If success, log the user in */
          localStorage.token = res.data.token;
          /* Then send action to reducer */
          dispatch({
            type: Constants.USER_LOGIN,
            payload: {
              user: res.data
            }
          });
          dispatch({type: "LOGIN_SUCCESS"});
          dispatch(Actions.loggedUser());
          dispatch(Actions.verifyUserRole(user.role));
        }
      })
      .catch((err) => {
				throw new SubmissionError(
					 {email: "Combinação inválida de email e senha!", _error: "Login Falhou!", password: "Combinação inválida de email e senha!"}
				);
      })
    }
  },

  loggedUser: () => {
    return async dispatch => fetch(`${API_HOST}/auth/me`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.token}` || ""
      }
    })
    .then((res) => { return res.json() })
    .then((res) => {
      if (!res.errors) {
        dispatch({
          type: Constants.USER_IS_LOGGED_IN,
          payload: {
            user: res.data
          }
        })
      }
    })
    .catch((err) => {
      console.warn(err);
    })
  },

  logoutUser: () => {
    localStorage.removeItem("token");
    return async dispatch => {
      dispatch({
        type: Constants.USER_LOGOUT
      })
    }
  },

  verifyUserRole: (role) => {
    return async dispatch => {
      if (role === "Professor") {
        dispatch({
          type: "LOGIN_SET_DESTINATION",
          payload: "/pedagogico"
        })
      }
      if (role === "Admin") {
        dispatch({
          type: "LOGIN_SET_DESTINATION",
          payload: "/administrativo"
        })
      }
    }
  },


};

export default Actions;
