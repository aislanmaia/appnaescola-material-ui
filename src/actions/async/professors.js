import * as sync         from 'actions/sync/professors';
import * as ClassesSync  from 'actions/sync/classes';
import * as SubjectsSync from 'actions/sync/subjects';
import { objIsEmpty }    from 'utils/helpers';
import { toastr }        from 'react-redux-toastr';
const API_HOST = process.env.REACT_APP_API_HOST || 'http://localhost:4000';


export const middleware = ({url, dispatch, opts}, action) => {
  if (action.type === "PROFESSORS_FETCH") {
		console.log("Branch PROFESSORS_FETCH");
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				return Promise.resolve(res);
			})
			.catch(err => {
				return Promise.reject(err);
			})
	}


  if (action.type === "PROFESSORS_SEARCH") {
		console.log("Branch PROFESSORS_SEARCH");
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				return Promise.resolve(res);
			})
			.catch(err => {
				return Promise.reject(err);
			})
	}


  if (action.type === "PROFESSOR_FETCH") {
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				return res;
			})
			.catch(err => {
				return err;
			})
	}

}

const Actions = {
  fetchProfessors: () => {
    return async dispatch => {
      dispatch(sync.fetchProfessors());
      fetch(`${API_HOST}/api/professors`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.fetchProfessorsSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.fetchProfessorsFailure());
      })
    }
  },

  fetchProfessor: (id) => {
    return async dispatch => {
      dispatch(sync.fetchProfessor());
      fetch(`${API_HOST}/api/professors/${id}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.fetchProfessorSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.fetchProfessorFailure());
      })
    }
  },

  /**
   * Fetches the professor logged by the user id
   * @param {string} userId - User id of the actual logged user.
   * @return {Object} professor - A object representing the professor
   * with id and name.
   */
  fetchProfessorByUserId: (userId) => {
    return async dispatch => {
      dispatch(sync.fetchProfessor());
      fetch(`${API_HOST}/api/users/${userId}/professor`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.fetchProfessorSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.fetchProfessorFailure(err));
      })
    }
  },


  /**
   * Fetches all classes which a specific professor is assigned for.
   * @params {string} id - The professor's id
   * @returns {Array.} - An array of Classes
   */
  fetchProfessorClasses: (id) => {
    return async dispatch => {
      dispatch(ClassesSync.fetchProfessorClasses());
      fetch(`${API_HOST}/api/professors/${id}/classes`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(ClassesSync.fetchProfessorClassesSuccess(res));
      })
      .catch((err) => {
        dispatch(ClassesSync.fetchProfessorClassesFailure());
      })
    }
  },

  /**
   * Fetches all Subjects which a specific professor is assigned for in a determined class.
   * @params {string} professrId - The professor's id
   * @params {string} classId - The class's id
   * @returns {Array.} - An array of Subjects
   */
  fetchProfessorClassSubjects: (professorId, classId) => {
    return async dispatch => {
      dispatch(SubjectsSync.fetchProfessorClassSubjects());
      fetch(`${API_HOST}/api/professors/${professorId}/classes/${classId}/subjects`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(SubjectsSync.fetchProfessorClassSubjectsSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(SubjectsSync.fetchProfessorClassSubjectsFailure(err));
      })
    }
  },

  searchProfessors: (term) => {
    console.log("term", term);
    return async dispatch => {
      dispatch(sync.searchProfessors());
      return fetch(`${API_HOST}/api/search/professors/${term}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        if (objIsEmpty(res.data))
          throw {message: "Professor não encontrado ou inexistente!"}
        else
          dispatch(sync.fetchProfessorsSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.fetchProfessorsFailure(err));
        throw {message: "Professor não encontrado ou inexistente!"}
      })
    }
  },

  create: (professor) => {
    return async dispatch => {
      dispatch(sync.createProfessor());
      fetch(`${API_HOST}/api/professors/`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          professor
        })
      })
      .then((res) => { return res.json() })
      .then((res) => {
        dispatch(sync.createProfessorSuccess(res));
        toastr.removeByType("info");
        toastr.success('Novo Professor', professor.name + ' criado com sucesso!', { progressBar: false } );
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.createProfessorFailure());
      })
    }
  },

  update: (professor) => {
    return async dispatch => {
      dispatch(sync.updateProfessor());
      fetch(`${API_HOST}/api/professors/${professor.id}`, {
        method: "PATCH",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          professor
        })
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.updateProfessorSuccess(res))
        toastr.removeByType("info");
        toastr.success('Professor', professor.name + ' atualizado com sucesso!', { progressBar: false } );
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.updateProfessorFailure());
      })
    }
  },

  delete: (professor) => {
    return async dispatch => {
      dispatch(sync.deleteProfessor());
      fetch(`${API_HOST}/api/professors/${professor.id}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.deleteProfessorSuccess(professor));
        toastr.removeByType("info");
        toastr.success('Professor', professor.name + ' deletado com sucesso!', { progressBar: false } );
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.deleteProfessorFailure());
      })
    }
  }

};

export default Actions;
