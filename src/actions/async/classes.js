import * as sync      from 'actions/sync/classes';
import { objIsEmpty } from 'utils/helpers';
import { toastr }     from 'react-redux-toastr';
import { SubmissionError } from 'redux-form';
import _              from 'lodash';
import * as SubjectsSync from 'actions/sync/subjects';
const API_HOST = process.env.REACT_APP_API_HOST || 'http://localhost:4000';

export const middleware = ({url, dispatch, opts}, action) => {
  if (action.type === "CLASSES_FETCH") {
		console.log("Branch CLASSES_FETCH");
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				return Promise.resolve(res);
			})
			.catch(err => {
        // effect.dispatch(fetchClassroomsFailure(err));
				return Promise.reject(err);
			})
	}

  if (action.type === "CLASS_FETCH") {
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				return res;
			})
			.catch(err => {
				return err;
			})
	}

  if (action.type === "CLASS_CREATE") {
		console.log("Branch CREATE")
		console.log("url", url);
		console.log("opts", opts);
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				if (res.errors) {
					return Promise.reject(res);
				} else {
					// dispatch(setSubmitSucceeded('SubjectForm'));
					// toastr.removeByType("info");
					// toastr.success(res.data.name + ' criada com sucesso!', { progressBar: false } );
					return Promise.resolve(res);
				}
			})
			.catch(err => {
				console.log("logging from catch", err);
        // effect.dispatch(fetchClassroomsFailure(err));
				return Promise.reject(err);
			})
	}

  if (action.type === "CLASS_UPDATE") {
		console.log("Branch UPDATE");
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				if (res.errors) {
					return res;
				} else {
					// dispatch(setSubmitSucceeded('SubjectForm'));
					// toastr.removeByType("info");
					// toastr.success(res.data.name + ' criada com sucesso!', { progressBar: false } );
					// return Promise.resolve(res);
					return res;
				}
			})
			.catch(err => {
				console.log("catching error...", err);
				return Promise.reject(err);
			})
	}

  if (action.type === "CLASS_DELETE") {
		console.log("Branch DELETE")
		console.log("url", url);
		console.log("opts", opts);
		return fetch(url, opts)
			// .then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				if (res.errors) {
					return res;
				} else {
					// dispatch(setSubmitSucceeded('SubjectForm'));
					// toastr.removeByType("info");
					// toastr.success(res.data.name + ' criada com sucesso!', { progressBar: false } );
					// return Promise.resolve(res);
					return res;
				}
			})
			.catch(err => {
				console.log("catching error...", err);
				return Promise.reject(err);
			})
	}


  if (action.type === "CLASSES_BATCH_DELETE") {
		console.log("Branch DELETE")
		console.log("url", url);
		console.log("opts", opts);
		return fetch(url, opts)
		// .then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				if (res.errors) {
					return res;
				} else {
					// dispatch(setSubmitSucceeded('SubjectForm'));
					// toastr.removeByType("info");
					// toastr.success(res.data.name + ' criada com sucesso!', { progressBar: false } );
					// return Promise.resolve(res);
					return res;
				}
			})
			.catch(err => {
				console.log("catching error...", err);
				return Promise.reject(err);
			})
	}
}

const Actions = {
  fetchClasses: () => {
    return async dispatch => {
      dispatch(sync.fetchClasses());
      fetch(`${API_HOST}/api/classes`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.fetchClassesSuccess(res));
      })
      .catch((err) => {
        console.log("error", err);
        dispatch(sync.fetchClassesFailure());
      })
    }
  },

  fetchClass: (id) => {
    return async dispatch => {
      dispatch(sync.fetchClass());
      fetch(`${API_HOST}/api/classes/${id}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.fetchClassSuccess(res));
      })
      .catch((err) => {
        console.log("error", err);
        dispatch(sync.fetchClassFailure());
      })

    }
  },

  fetchClassProfessors: (id) => {
    return async dispatch => {
      dispatch(sync.fetchClassProfessors());
      fetch(`${API_HOST}/api/classes/${id}/professors`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.fetchClassProfessorsSuccess(res));
      })
      .catch((err) => {
        console.log("error", err);
        // dispatch(sync.fetchClassProfessorsFailure());
      })
    }
  },

  fetchClassSubjects: (id) => {
    return async dispatch => {
      dispatch(sync.fetchClassSubjects());
      fetch(`${API_HOST}/api/classes/${id}/subjects`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.fetchClassSubjectsSuccess(res, id));
        dispatch(SubjectsSync.fetchSubjectsSuccess(res));
      })
      .catch((err) => {
        console.log("error", err);
        /* dispatch(sync.fetchClassSubjectsFailure());*/
      })
    }
  },

  searchClasses: (term) => {
    console.log("term", term);
    return async dispatch => {
      dispatch(sync.searchClasses());
      return fetch(`${API_HOST}/api/search/classes/${term}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        if (objIsEmpty(res.data))
          throw {message: "Turma não encontrada ou inexistente!"}
        else
          dispatch(sync.fetchClassesSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.fetchClassesFailure(err));
        throw {message: "Turma não encontrada ou inexistente!"}
      })
    }
  },

	/* Async fetch for input filter in table component */
	tableSearchClasses: (term) => {
    console.log("term", term);
    return async dispatch => {
      dispatch(sync.searchClasses());
      return fetch(`${API_HOST}/api/search/classes/${term}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
				.then((res) => { return res.json() })
				.then((res) => {
					console.log("res", res);
					if (objIsEmpty(res.data))
						throw {message: "Turma não encontrada ou inexistente!"} // eslint-disable-line no-throw-literal
					else
						dispatch(sync.fetchClassesSuccess(res));
					return Promise.resolve(res);
				})
				.catch((err) => {
					console.log("err", err);
					dispatch(sync.fetchClassesFailure(err));
					throw {message: "Turma não encontrada ou inexistente!"} // eslint-disable-line no-throw-literal
				})
    }
  },


  create: (_class) => {
    return async dispatch => {
      /* _class.professors = {..._class.professors};*/
      /* _class.students = {..._class.students};*/
      console.log("class", _class);
      dispatch(sync.createClass());
      fetch(`${API_HOST}/api/classes`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          class: _class
        })
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.createClassSuccess(res));
        toastr.removeByType("info");
        toastr.success('Nova Turma', _class.name + ' criada com sucesso!', { progressBar: false } );
      })
      .catch((err) => {
        console.log("error", err);
        dispatch(sync.createClassFailure());
      })
    }
  },

  update: (oldClass, newClass) => {
    return async dispatch => {
      dispatch(sync.updateClass(oldClass, newClass));
      // fetch(`${API_HOST}/api/classes/${_class.id}`, {
      //   method: "PUT",
      //   headers: {
      //     Accept: "application/json",
      //     "Content-Type": "application/json"
      //   },
      //   body: JSON.stringify({
      //     class: _class
      //   })
      // })
      //   .then((res) => { return res.json() })
      //   .then((res) => {
      //     console.log("res", res);
      //     if (res.errors) {
      //       throw new SubmissionError({name: "Este nome já foi definido anteriormente", _error: "Update failed!"})
      //     }
      //     toastr.removeByType("info");
      //     toastr.success('Turma', _class.name + ' atualizada com sucesso!', { progressBar: false } );
      //     dispatch(sync.updateClassSuccess(res.data))
      //   })
      //   .catch((err) => {
      //     console.log("error", err);
      //     dispatch(sync.updateClassFailure());
      //   })
    }
  },

  delete: (_class) => {
    return async dispatch => {
      dispatch(sync.deleteClass());
      fetch(`${API_HOST}/api/classes/${_class.id}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => {
        console.log("res", res);
        dispatch(sync.deleteClassSuccess(_class));
        toastr.removeByType("info");
        toastr.success('Turma', _class.name + ' deletada com sucesso!', { progressBar: false } );
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.deleteClassFailure(err));
      })
    }
  },

  count: () => {
    return async dispatch => {
      dispatch(sync.countClass());
      fetch(`${API_HOST}/api/classes/count`, {
        method: 'POST',
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json(); })
      .then((res) => {
        dispatch(sync.countClassSuccess(res));
      })
      .catch((err) => {
        dispatch(sync.countClassFailure(err));
      })
    }
  },


  updateClassSubjects: (classId, class_subjects) => {
    return async dispatch => {
      dispatch(sync.updateClassSubjects());
      let array = [];
      class_subjects.forEach((cs) => {
        let result = prepareClassSubject(cs);
        if (result) array.push(result); else return;
      });
      if (_.isEmpty(array)) {
        toastr.removeByType("info");
        toastr.warning("", "Nenhuma alteração feita! Por favor, altere ou adicione antes de salvar!", { progressBar: false });
      } else {
       fetch(`${API_HOST}/api/classes/${classId}/subjects/`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          class_subject: array
        })
      })
        .then((res) => { return res.json(); })
        .then((res) => {
          console.log("res", res);
          toastr.removeByType("info");
          toastr.success('Grade de Matérias', ' salva com sucesso!', { progressBar: false } );
        })
        .catch((err) => {
          console.log("err", err);
        })
      }
    }
  },

  addSubjectToClass: (classId, subject) => {
    return async dispatch => {
      // dispatch(sync.addSubjectToClass());
      fetch(`${API_HOST}/api/classes/${classId}/subjects/${subject.id}`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          subject: subject
        })
      })
      .then((res) => { return res.json(); })
      .then((res) => {
        console.log("res", res);
        /* dispatch(sync.countClassSuccess(res));*/
      })
      .catch((err) => {
        console.log("err", err);
        /* dispatch(sync.countClassFailure(err));*/
      })

    }
  },

};

const prepareClassSubject = (class_subject) => {
  console.log("preparing class_subject", class_subject);
  if (class_subject.id && !class_subject.modified && !class_subject.remove) return;
  let cs = {};

  if (class_subject.professor.id) {
    cs.professor_id = class_subject.professor.id
  }

  if (class_subject.subject.id) {
    cs.subject_id = class_subject.subject.id
  }

  if (class_subject.id) cs.id = class_subject.id;
  if (class_subject.remove) cs.remove = class_subject.remove;

  return cs;
}

export default Actions;
