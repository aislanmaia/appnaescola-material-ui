// import Constants from 'constants';
import * as sync from 'actions/sync/classrooms';
import { setSubmitSucceeded } from 'redux-form';
import { objIsEmpty } from 'utils/helpers';
import {toastr} from 'react-redux-toastr';
const API_HOST = process.env.REACT_APP_API_HOST || 'http://localhost:4000';

export const middleware = ({url, dispatch, opts}, action) => {
  if (action.type === "CLASSROOMS_FETCH") {
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				return Promise.resolve(res);
			})
			.catch(err => {
        // effect.dispatch(fetchClassroomsFailure(err));
				return Promise.reject(err);
			})
	}

  if (action.type === "CLASSROOM_FETCH") {
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				return res;
			})
			.catch(err => {
				return err;
			})
	}

  if (action.type === "CLASSROOM_CREATE") {
		console.log("Branch CREATE")
		console.log("url", url);
		console.log("opts", opts);
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				if (res.errors) {
					return Promise.reject(res);
				} else {
					// dispatch(setSubmitSucceeded('SubjectForm'));
					// toastr.removeByType("info");
					// toastr.success(res.data.name + ' criada com sucesso!', { progressBar: false } );
					return Promise.resolve(res);
				}
			})
			.catch(err => {
				console.log("logging from catch", err);
        // effect.dispatch(fetchClassroomsFailure(err));
				return Promise.reject(err);
			})
	}

  if (action.type === "CLASSROOM_UPDATE") {
		console.log("Branch UPDATE")
		return fetch(url, opts)
			.then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				if (res.errors) {
					return res;
				} else {
					// dispatch(setSubmitSucceeded('SubjectForm'));
					// toastr.removeByType("info");
					// toastr.success(res.data.name + ' criada com sucesso!', { progressBar: false } );
					// return Promise.resolve(res);
					return res;
				}
			})
			.catch(err => {
				console.log("catching error...", err);
				return Promise.reject(err);
			})
	}

  if (action.type === "CLASSROOM_DELETE") {
		console.log("Branch DELETE")
		console.log("url", url);
		console.log("opts", opts);
		return fetch(url, opts)
			// .then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				if (res.errors) {
					return res;
				} else {
					// dispatch(setSubmitSucceeded('SubjectForm'));
					// toastr.removeByType("info");
					// toastr.success(res.data.name + ' criada com sucesso!', { progressBar: false } );
					// return Promise.resolve(res);
					return res;
				}
			})
			.catch(err => {
				console.log("catching error...", err);
				return Promise.reject(err);
			})
	}


  if (action.type === "CLASSROOMS_BATCH_DELETE") {
		console.log("Branch DELETE")
		console.log("url", url);
		console.log("opts", opts);
		return fetch(url, opts)
		// .then((res) => { return res.json() })
			.then((res) => {
				console.log("res", res);
				// dispatch(fetchClassroomsSuccess(res));
				if (res.errors) {
					return res;
				} else {
					// dispatch(setSubmitSucceeded('SubjectForm'));
					// toastr.removeByType("info");
					// toastr.success(res.data.name + ' criada com sucesso!', { progressBar: false } );
					// return Promise.resolve(res);
					return res;
				}
			})
			.catch(err => {
				console.log("catching error...", err);
				return Promise.reject(err);
			})
	}
}

// export default { actions };

const Actions = {
  fetchClassrooms: (page) => {
    return async dispatch => {
      dispatch(sync.fetchClassrooms(page, dispatch));
      // fetch(`${API_HOST}/api/classrooms?page=${page}`, {
      //   method: "GET",
      //   headers: {
      //     Accept: "application/json",
      //     "Content-Type": "application/json"
      //   }
      // })
      //   .then((res) => { return res.json() })
      //   .then((res) => {
      //     /* console.log("classrooms: ", res);*/
      //     dispatch(sync.fetchClassroomsSuccess(res));
      //   })
      //   .catch((err) => {
      //     console.log("error", err);
      //     dispatch(sync.fetchClassroomsFailure());
      //   })
      /* const classroomArray = [];*/
      /* console.log("classroomArray", classroomArray);*/
      /* if (classroomArray) {
       * } else {
       *   dispatch(sync.fetchClassroomsFailure());
       * }*/
    }
  },

  searchClassrooms: (term) => {
    console.log("term", term);
    return async dispatch => {
      dispatch(sync.searchClassrooms());
      return fetch(`${API_HOST}/api/search/classrooms/${term}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then((res) => { return res.json() })
      .then((res) => {
        console.log("res", res);
        if (objIsEmpty(res.data))
          throw {message: "Sala não encontrada ou inexistente!"} // eslint-disable-line no-throw-literal
        else
          dispatch(sync.fetchClassroomsSuccess(res));
      })
      .catch((err) => {
        console.log("err", err);
        dispatch(sync.fetchClassroomsFailure(err));
        throw {message: "Sala não encontrada ou inexistente!"} // eslint-disable-line no-throw-literal
      })
    }
  },

	/* Async fetch for input filter in table component */
	tableSearchClassrooms: (term) => {
    console.log("term", term);
    return async dispatch => {
      dispatch(sync.searchClassrooms());
      return fetch(`${API_HOST}/api/search/classrooms/${term}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
				.then((res) => { return res.json() })
				.then((res) => {
					console.log("res", res);
					if (objIsEmpty(res.data))
						throw {message: "Sala não encontrada ou inexistente!"} // eslint-disable-line no-throw-literal
					else
						dispatch(sync.fetchClassroomsSuccess(res));
						return Promise.resolve(res);
				})
				.catch((err) => {
					console.log("err", err);
					dispatch(sync.fetchClassroomsFailure(err));
					throw {message: "Sala não encontrada ou inexistente!"} // eslint-disable-line no-throw-literal
				})
    }
  },

  fetchClassroom: (id) => {
    return async dispatch => {
      dispatch(sync.fetchClassroom(id));

      // fetch(`${API_HOST}/api/classrooms/${id}`, {
      //   method: "GET",
      //   headers: {
      //     Accept: "application/json",
      //     "Content-Type": "application/json"
      //   }/* ,*/
      //   /* body: JSON.stringify({
      //    *   id
      //    * })*/
      // })
      // .then((res) => { return res.json() })
      // .then((res) => {
      //   console.log("classrooms: ", res);
      //   dispatch(sync.fetchClassroomSuccess(res.data));
      // })
      // .catch((err) => {
      //   console.log("error", err);
      //   dispatch(sync.fetchClassroomFailure());
      // })
    }
  },

  create: (classroom) => {
		console.log("classroom in async create...", classroom);
    return async ( dispatch, getState ) => {
      dispatch(sync.createClassroom(classroom));
			console.log("getting state: classrooms = ", getState().classrooms);
			const { classroom: newClassroom, error } = getState().classrooms.newClassroom;
			if (newClassroom && !error) {
				// return newClassroom;
				toastr.removeByType('info');
				toastr.success(classroom.name + ' criada com sucesso!', { progressBar: false } );
				Promise.resolve(newClassroom);
			} else {
				// return error;
				Promise.reject(error);
			}
    //   fetch(`${API_HOST}/api/classrooms`, {
    //     method: "POST",
    //     headers: {
    //       Accept: "application/json",
    //       "Content-Type": "application/json"
    //     },
    //     body: JSON.stringify({
    //       classroom
    //     })
    //   })
    //   .then((res) => { return res.json() })
    //   .then((res) => {
    //     console.log("res", res);
    //     if (res.errors) {
    //       /* return Promise.resolve(res);*/
    //        throw new SubmissionError({name: "Este nome já foi definido anteriormente", _error: "Update failed!"})
    //     }
    //     dispatch(sync.createClassroomSuccess(res.data));
    //     toastr.removeByType("info");
    //     toastr.success('Nova Sala', classroom.name + ' criada com sucesso!', { progressBar: false } );
    //   })
    //   .catch((err) => {
    //     toastr.removeByType("info");
    //     dispatch(sync.createClassroomFailure(err));
    //   })
    // }
    // return dispatch => asteroid.call('addClassroom', data)
    //   .then(result => dispatch(sync.addClassroomSuccess({_id: result, data})));
		}
  },

  update: (oldClassroom, newClassroom) => {
    return async dispatch => {
      dispatch(sync.updateClassroom(oldClassroom, newClassroom));
      // return fetch(`${API_HOST}/api/classrooms/${classroom.id}`, {
      //   method: "PUT",
      //   headers: {
      //     Accept: "application/json",
      //     "Content-Type": "application/json"
      //   },
      //   body: JSON.stringify({
      //     classroom
      //   })
      // })
      // .then((res) => { return res.json() })
      // .then((res) => {
      //   console.log("res", res);
      //   /* TODO: verificar se há alguma forma de lançar estes errors para react-redux tratar adequadamente no campo.
      //    *       Caso não for possível, pode-se testar uma nova lib para tratar forms com react: react-redux-form.
      //    */
      //   if (res.errors) {
      //     return Promise.reject(res);
      //     throw new SubmissionError({name: "Este nome já foi definido anteriormente", _error: "Update failed!"})
      //   }
      //   dispatch(sync.updateClassroomSuccess(res.data))
      //   toastr.removeByType("info");
      //   toastr.success('Sala', classroom.name + ' atualizada com sucesso!', { progressBar: false } );
      // })
      // .catch((err) => {
      //   toastr.removeByType("info");
      //   dispatch(sync.updateClassroomFailure(err));
      //   return Promise.reject(err);
      // })
    }
  },

  delete: (classroom) => {
    return async dispatch => {
      dispatch(sync.deleteClassroom(classroom));

      // fetch(`${API_HOST}/api/classrooms/${classroom.id}`, {
      //   method: "DELETE",
      //   headers: {
      //     Accept: "application/json",
      //     "Content-Type": "application/json"
      //   }
      // })
      // .then((res) => { /* return res.json()*/ })
      // .then((res) => {
      //   dispatch(sync.deleteClassroomSuccess(classroom));
      // })
      // .catch((err) => {
      //   console.log("err", err);
      //   dispatch(sync.deleteClassroomFailure(err));
      //   toastr.removeByType("info");
      //   toastr.success('Deletada a sala: ' + classroom.name + ' com sucesso!', { progressBar: false } );
      // })
    }
  },

  batchDelete: (classrooms) => {
		return async dispatch => {
			dispatch(sync.batchDelete(classrooms));
		}
	}

};

export default Actions;
