import { startsWith } from 'lodash';
import { middleware as ClassroomsActions} from 'actions/async/classrooms';
import { middleware as ClassesActions} from 'actions/async/classes';
import { middleware as SubjectsActions} from 'actions/async/subjects';
import { middleware as ProfessorsActions} from 'actions/async/professors';
import { middleware as StudentsActions} from 'actions/async/students';

const send = ({url, dispatch, opts}, action) => {

  if ( startsWith (action.type, "CLASSROOM")) {
		return ClassroomsActions({url, dispatch, opts}, action);
	}

  if ( startsWith (action.type, "CLASS")) {
		return ClassesActions({url, dispatch, opts}, action);
	}

  if ( startsWith (action.type, "SUBJECT")) {
		return SubjectsActions({url, dispatch, opts}, action);
	}

  if ( startsWith (action.type, "PROFESSOR")) {
		return ProfessorsActions({url, dispatch, opts}, action);
	}

  if ( startsWith (action.type, "STUDENT")) {
		return StudentsActions({url, dispatch, opts}, action);
	}

}

export default {
	send
};
