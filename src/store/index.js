/*
  Basically we are configuring the Store with three middlewares:

	(Not using this anymore as the react-router-redux package ins't compatible
	 with react-router v4 yet)
  * reduxRouterMiddleware to dispatch router actions to the store.

  * redux-thunk to dispatch async actions.
  * redux-logger to log any action and state changes through the browser's console.
*/
import { createStore, applyMiddleware, compose }	from 'redux';
//import { routerMiddleware }											from 'react-router-redux';
import { createLogger }														from 'redux-logger';

/* We need to make use of asynchronous calls. Because Redux
 * doesn't support this without middleware, we're going to use a thunk to it.
 * A thunk is a function that wraps an expression to delay its evaluation.
 */
import thunkMiddleware														from 'redux-thunk';

/* Importing all reducers already combined together */
import reducers																		from 'reducers/index.js';

/* Import redux-offline and default configs */
import { createOffline }													from '@redux-offline/redux-offline';
import defaultConfig															from '@redux-offline/redux-offline/lib/defaults';
import Api																				from 'api';

const config = {
  ...defaultConfig,
	effect: (effect, action) => {
		console.log(`Executing effect for ${action.type}`);
		return Api.send(effect, action);
			// .then((res) => res.json())
						// ? res.json()
						// : Promise.reject(res.text().then(msg => new Error(msg)))
	},
};

const loggerMiddleware = createLogger({
  level: 'info',
  collapsed: true
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// const { middleware, enhanceReducer, enhanceStore } = createOffline(defaultConfig);
const { middleware, enhanceReducer, enhanceStore } = createOffline(config);
const enhancedReducers = enhanceReducer(reducers);

export default function configureStore(browserHistory) {
  //const reduxRouterMiddleware = routerMiddleware(browserHistory);
  const createStoreWithMiddleware = composeEnhancers(
		 applyMiddleware(
				//reduxRouterMiddleware,
				middleware,
				thunkMiddleware,
				loggerMiddleware,
		 ),
		 enhanceStore,
	)(createStore);

  // return createStoreWithMiddleware(reducers);
  return createStoreWithMiddleware(enhancedReducers);
}
