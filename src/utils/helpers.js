import _ from 'lodash';

export const pick = (o, ...props) => props.reduce((r, p) => p in o ? {...r, [p]: o[p]} : r, {});

export const pickRename = (o, ...props) => props.reduce((r, expr) => {
  const [p, np] = expr.split(":").map( e => e.trim() )
  return p in o ? {...r, [np || p]: o[p]} : r
}, {});

export const pickStartWith = (o, chars) => Object.keys(o).reduce((r, p) => _.startsWith(p, chars) ? {...r, [p]: o[p]} : r, {});

export const objIsEmpty = (o) => Object.values(o).every((value) => value === "" || _.isEmpty(value));

export const objHasData = (o) => !objIsEmpty(o);

export const arrEqual = (arr1, arr2) => arr1.length === arr2.length && arr1.every((v, i) => v === arr2[i])

export const getMonthName = (prefixName) => {
  switch (prefixName) {
    case "Jan":
      return "Janeiro"

    case "Feb":
      return "Fevereiro"

    case "Mar":
      return "Março"

    case "Apr":
      return "Abril"

    case "May":
      return "Maio"

    case "Jun":
      return "Junho"

    case "Jul":
      return "Julho"

    case "Aug":
      return "Agosto"

    case "Sep":
      return "Setembro"

    case "Oct":
      return "Outubro"

    case "Nov":
      return "Novembro"

    case "Dec":
      return "Dezembro"

    default:
      return prefixName;
  }
}
