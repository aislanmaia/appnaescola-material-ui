import { red } from 'material-ui/colors';

const drawerWidth = 260;

const headerStyles = theme => ({
	headerSection: {
		position: 'fixed',
		zIndex: 1000,
		backgroundColor: theme.palette.primary[300],
		marginLeft: '-1.6rem',
		marginTop: '-1.5rem',
		padding: '1rem 2rem',
		width: '100%',
		filter: 'drop-shadow(0 2px 3px rgba(0, 0, 0, 0.3))',

    '@media (min-height: 320px) and (max-height: 470px)': {
      marginTop: '-2.5rem',
		},
	},
});

const floatingActions = {
	floatingActions: {
		position: 'fixed',
		right: '2rem',
		bottom: '2rem',
		zIndex: 2,
	}
};

const form = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
    fontSize: '1.2em',
  },
  formControl: {
    fontSize: '1.6rem',
    padding: '2rem 0',
    width: '80%',
  },
  formHelperText: {
    marginLeft: 8,
  },
  inputLabel: {
    fontSize: '1.2rem',
    left: 8,
    top: '2rem',
  },
  input: {
    fontSize: '1rem',
  },
  inputUnderline: {
    '&:before': {
        backgroundColor: `rgba(0, 191, 165, 1)`,
        left: 0,
        bottom: 0,
        // Doing the other way around crash on IE11 "''" https://github.com/cssinjs/jss/issues/242
        content: '""',
        height: '2px',
        position: 'absolute',
        right: 0,
        transition: theme.transitions.create('background-color', {
          duration: theme.transitions.duration.shorter,
          easing: theme.transitions.easing.ease,
        }),
        pointerEvents: 'none', // Transparent to the hover style.
      },
      '&:hover:not($disabled):before': {
        backgroundColor: theme.palette.text.primary,
        height: '2px',
      },
      '&$disabled:before': {
        background: 'transparent',
        /* backgroundImage: `linear-gradient(to right, ${
         *   theme.palette.input.bottomLine
         * } 33%, transparent 0%)`,*/
        backgroundImage: `linear-gradient(to right,
         #00bfa5 33%, transparent 0%)`,
        backgroundPosition: 'left top',
        backgroundRepeat: 'repeat-x',
        backgroundSize: '5px 1px',
      },
  },
  fieldIcon: {
    position: 'absolute',
    left: -45,
    top: '1rem',
    width: 40,
    height: 40,
    color: 'rgba(0, 0, 0, 0.26)',
  },
  containerField: {
    '&:first-of-type': {
      marginTop: 0,
      width: '80%',
      position: 'relative',
      display: 'inline-block'
    },
    marginTop: 40,
    width: '80%',
    position: 'relative',
    display: 'inline-block'
  },
  button: {
    fontSize: '1.6rem',
  },
  validIconStatus: {
    color: 'green',
  },
  invalidIconStatus: {
    color: 'red',
  },
	buttonWarning: {
    backgroundColor: red[500],

		'&:hover': {
			backgroundColor: red[300],
		},
	},
});


export {
  drawerWidth,
  form,
	floatingActions,
	headerStyles,
};
