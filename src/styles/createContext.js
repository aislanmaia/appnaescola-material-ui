/* eslint-disable flowtype/require-valid-file-annotation */

// import jss, { SheetsRegistry } from 'react-jss/lib/jss';
import { create, SheetsRegistry } from 'jss';
import preset from 'jss-preset-default';
import { createMuiTheme } from 'material-ui/styles';
import { green, teal } from 'material-ui/colors';
import createGenerateClassName from 'material-ui/styles/createGenerateClassName';


// const greenPalette = {
//     50: '#f2fdf2',
//     100: '#defade',
//     200: '#c8f7c8',
//     300: '#b1f3b1',
//     400: '#a1f1a1',
//     500: '#90ee90',
//     600: '#88ec88',
//     700: '#7de97d',
//     800: '#73e773',
//     900: '#61e261',
//     A100: '#ffffff',
//     A200: '#ffffff',
//     A400: '#e8ffe8',
//     A700: '#cfffcf',
//     'contrastDefaultColor': 'dark',
// };

const theme = createMuiTheme({
  palette: {
    primary: teal,
    secondary: green,
    background: {
      default: '#eff2f7',
    },
    // input: {
    //   bottomLine: '#00bfa5'
    // }
  },
});

// Configure JSS
const jss = create(preset());
jss.options.createGenerateClassName = createGenerateClassName;

export const sheetsManager = new Map();

export default function createContext() {
  return {
    jss,
    theme,
    // This is needed in order to deduplicate the injection of CSS in the page.
    sheetsManager,
    // This is needed in order to inject the critical CSS.
    sheetsRegistry: new SheetsRegistry(),
  };
}
