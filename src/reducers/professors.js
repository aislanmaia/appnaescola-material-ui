import Constants from 'constants/professors';

const initialState =
  { professorsList:
    { professors: []
    , error: null
    , loading: false
    }
  , newProfessor:
    { professor: null
    , error: null
    , loading: false
    }
  , activeProfessor:
    { professor: null
    , error: null
    , loading: false
    }
  , deletedProfessor:
    { professor: null
    , error: null
    , loading: false
    }
  }

export default function reducer(state = initialState, action = {}) {
  let error;
  switch (action.type) {
    case Constants.PROFESSOR_CREATE:
      return {...state, newProfessor: {...state.newProfessor, loading: true}}

    case Constants.PROFESSOR_CREATE_SUCCESS:
      return { ...state, newProfessor: { professor: action.payload, error: null, loading: false } }

    case Constants.PROFESSOR_CREATE_FAILURE:
      error = action.payload || { message: action.payload.message };
      return { ...state, newProfessor: { professor: null, error: error, loading: false } };

    case Constants.PROFESSOR_RESET_NEW:
      return { ...state, newProfessor: {professor: null, error: null, loading: false} };

    case Constants.PROFESSORS_FETCH: // start fetching  and set loading = true
      return { ...state, professorsList: {professors: [], error: null, loading: true} };

    case Constants.PROFESSORS_FETCH_SUCCESS: // return list of  and make loading = false
      return { ...state, professorsList: {professors: action.payload.data, error: null, loading: false} };

    case Constants.PROFESSORS_FETCH_FAILURE: // return error and make loading = false
      error = action.payload || {message: action.payload.message};
      return { ...state, professorsList: {professors: [], error: error, loading: false} };

    case Constants.PROFESSORS_RESET_LIST: // reset professorsList to initial state
      return { ...state, professorsList: {professors: [], error: null, loading: false} };

    case Constants.PROFESSOR_FETCH:
      return { ...state, activeProfessor: { ...state.activeProfessor, loading: true } };

    case Constants.PROFESSOR_FETCH_SUCCESS:
      let professor;
      if (action.payload.data && action.payload.data.birthdate) {
        let { birthdate, ...professor } = action.payload.data;
        if (birthdate instanceof Object) {
          professor.birthdate = new Date(action.payload.data.birthdate.$date);
        } else {
          professor.birthdate = new Date(action.payload.data.birthdate);
        }
      } else {
        if (action.payload.data) {
          professor = action.payload.data;
        } else {
          professor = action.payload;
        }
      }
      return { ...state, activeProfessor: { professor: professor, error: null, loading: false } };

    case Constants.PROFESSOR_FETCH_FAILURE:
      error = action.payload.professor || { message: action.payload.message }
      return { ...state, activeProfessor: { professor: null, error: error, loading: false } };

    case Constants.PROFESSOR_RESET_PROFESSOR:
      return { ...state, activeProfessor: { professor: null, error: null, loading: false } };

    case Constants.PROFESSOR_DELETE:
      return { ...state, ...state.deletedProfessor, loading: true };

    case Constants.PROFESSOR_DELETE_SUCCESS:
      let { professors } = state.professorsList;
      console.log("lista atual na store:", );
      console.log("professor deletado na store:", action.payload);
      const deletedProfessor = action.payload;
      const newProfessorsList = professors.filter( (professor) => professor.id !== deletedProfessor.id )
      return {
        ...state,
        professorsList: {professors: newProfessorsList, error: error, loading: false},
        deletedProfessor: { professor: action.payload.data, error: null, loading: false }
      };

    case Constants.PROFESSOR_DELETE_FAILURE:
      error = action.payload || { message: action.payload.message };
      return { ...state, deletedProfessor: { professor: null, error: error, loading: false } };

    case Constants.PROFESSOR_RESET_DELETED:
      return { ...state, deletedProfessor: { professor: null, error: null, loading: false } };



    default:
      return state;
  }
}
