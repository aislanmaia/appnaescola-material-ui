// import Constants from 'constants';

const initialState =
  { error: null
  , urlDest: null
  };

export default function reducer(state = initialState, action = {}) {
  switch(action.type) {
  case "LOGIN_FAILED":
    return Object.assign({}, state,
        {
          error: action.payload
        });
  case "LOGIN_SUCCESS":
    return initialState;

  case "LOGIN_SET_DESTINATION":
    return Object.assign({}, state,
        { urlDest: action.payload
        });

  default: return state;
  }
};
