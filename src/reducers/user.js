import Constants from 'constants/user';

const initialState =
  { email: ''
  , username: ''
  , id: ''
  , admin: false
  , role: ''
  , loading: false
  };

export default function reducer(state = initialState, action = {}) {
  switch(action.type) {
  case Constants.USER_LOGGING:
    return Object.assign({}, state,
      {
        loading: true
      });
  case Constants.USER_LOGIN:
    return Object.assign({}, state,
      { ...action.payload.user
      , loading: false
      });
  case Constants.USER_IS_LOGGED_IN:
    return Object.assign({}, state,
      { ...action.payload.user
      , loading: false
      });
  case Constants.USER_LOGIN_FAILED:
    return Object.assign({}, state, { loading: false });

  case Constants.USER_LOGOUT:
    return initialState;

  default: return state;
  }
};
