import Constants from 'constants/classes';
import _ from 'lodash';

const initialState =
  { classesList:
    { classes: []
    , error: null
    , loading: false
    }
  , newClass:
    { class: null
    , error: null
    , loading: false
    }
  , activeClass:
    { class: null
    , error: null
    , loading: false
    }
  , deletedClass:
    { class: null
    , error: null
    , loading: false
    }
  , professorsSelected:
    {all: [], added: [], removed: []}
  , studentsSelected:
    {all: [], added: [], removed: []}
  , subjectsSelected: []
  };

export default function reducer(state = initialState, action = {}) {
  let error,
			newClass,
			newList,
			removedList,
			addedList,
			allList,
			_class,
			classes, /* classes = [], activeClass, */
			classId,
			newClassesList;

  switch (action.type) {
    case Constants.CLASS_CREATE:
      return {...state, newClass: {...state.newClass, loading: true}}

    case Constants.CLASS_CREATE_SUCCESS:
      return { ...state, newClass: { class: action.payload, error: null, loading: false } }

    case Constants.CLASS_CREATE_FAILURE:
      error = action.payload || { message: action.payload.message };
      return { ...state, newClass: { class: null, error: error, loading: false } };

		case Constants.CLASS_UPDATE:
			/* Optmistic update the list */
			classes = state.classesList.classes.data || [];
			newClass = action.payload.newClass;
			const classInList = _.find(classes, c => c.id === newClass.id);
			newClass.students_count = classInList.students_count;
			newClass.professors_count = classInList.professors_count;
			newClass.professors = state.professorsSelected.all;
			newClass.students = state.studentsSelected.all;
			delete newClass.profsAdded;
			delete newClass.profsRemoved;
			delete newClass.professorsIds;
			delete newClass.studentsAdded;
			delete newClass.studentsRemoved;
			delete newClass.studentsIds;

			newList = classes.map(c => c.id === newClass.id ? newClass : c);
			return { ...state,
							 classesList: {classes: {...state.classesList.classes, data: newList}, error: null, loading: false}
						 };

		case Constants.CLASS_UPDATE_SUCCESS:
			/* Case success, just returns the current classes list because the optimitstic updates were already applied */
			return { ...state, classesList: {...state.classesList} };

		case Constants.CLASS_UPDATE_FAILURE:
			error = action.payload || { message: action.payload.message };
			classes = state.classesList.classes.data || [];
			let restoredList = classes.map(c => c.id === action.meta.oldClass.id ? action.meta.oldClass : c);
			return { ...state,
							classesList: {classes: {...state.classesList.classes, data: restoredList}, error: null, loading: false},
							activeClass: {...state.activeClass, error: error, loading: false}
						};

    case Constants.CLASS_RESET_NEW:
      return { ...state, newClass: {class: null, error: null, loading: false} };

    case Constants.CLASSES_FETCH: // start fetching classes and set loading = true
			console.log("Loggin from CLASSES_FETCH reducer");
      return { ...state, classesList: {classes: state.classesList.classes, error: null, loading: true} };

    case Constants.CLASSES_FETCH_SUCCESS: // return list of classes and make loading = false
			let data = [];
			if (state.classesList.classes && !_.isEmpty( state.classesList.classes )) {
				if (state.classesList.classes.hasOwnProperty('data')) {
					data = state.classesList.classes.data;
					/* Diffing prev data collection with payload data */
					if (!_.isEqual(data, action.payload.data)) {
						data = _.unionBy(data, action.payload.data, "id");
					}
					classes = {
						...action.payload,
						data: data
					};
				}
			} else {
				classes = {
					...action.payload,
					data: _.unionBy(data, action.payload.data, "id")
				};
			}
			return { ...state, classesList: {classes: classes, error: null, loading: false} };

    case Constants.CLASSES_FETCH_FAILURE: // return error and make loading = false
      error = action.payload || {message: action.payload.message};
      return { ...state, classesList: {classes: [], error: error, loading: false} };

    case Constants.CLASSES_RESET_LIST: // reset classesList to initial state
      return { ...state, classesList: {classes: [], error: null, loading: false} };

    case Constants.CLASS_FETCH:
      return { ...state, activeClass: { ...state.activeClass, loading: true } };

    case Constants.CLASS_FETCH_SUCCESS:
    return {
			...state,
			activeClass: { class: action.payload.data, error: null, loading: false },
			studentsSelected: { ...state.studentsSelected, all: action.payload.data.students },
			professorsSelected: { ...state.professorsSelected, all: action.payload.data.professors }
		};

    case Constants.CLASS_FETCH_FAILURE:
      error = action.payload.class || { message: action.payload.message }
      return { ...state, activeClass: { class: null, error: error, loading: false } };

    case Constants.CLASS_RESET_CLASS:
      return { ...state, activeClass: { class: null, error: null, loading: false } };

    case Constants.CLASS_PROFESSORS_FETCH: // start fetching class's professors and set loading = true
      return { ...state, activeClass: { ...state.activeClass, class: {professors: {loading: true}} } };

    case Constants.CLASS_PROFESSORS_FETCH_SUCCESS:
      _class = _.merge(state.activeClass.class, action.payload);
      return { ...state, activeClass: { ...state.activeClass, class: _class, loading: false}}

    case Constants.CLASS_PROFESSORS_FETCH_FAILURE:
      error = action.payload.class || { message: action.payload.message }
      return { ...state, activeClass: { class: null, error: error, loading: false } };

    /* Professor's classes fetching */
    case Constants.PROFESSOR_CLASSES_FETCH:
      return { ...state, classesList: {classes: [], error: null, loading: true} };

    case Constants.PROFESSOR_CLASSES_FETCH_SUCCESS:
      console.log("action.payload.data", action.payload.data);
      return { ...state, classesList: {classes: action.payload.data, error: null, loading: false} };

    case Constants.PROFESSOR_CLASSES_FETCH_FAILURE:
      error = action.payload || {message: action.payload.message};
      return { ...state, classesList: {classes: [], error: error, loading: false} };


    case Constants.CLASS_DELETE:
      return { ...state, ...state.deletedClass, loading: true };

    case Constants.CLASS_DELETE_SUCCESS:
      let { classes } = state.classesList;
      console.log("lista atual na store:", classes);
      console.log("sala deletada na store:", action.payload);
      const deletedClass = action.payload;
      newClassesList = classes.filter( (item) => item.id !== deletedClass.id )
      return {
        ...state,
        classesList: {classes: newClassesList, error: error, loading: false},
        deletedClass: { class: action.payload, error: null, loading: false }
      };

    case Constants.CLASS_DELETE_FAILURE:
      error = action.payload || { message: action.payload.message };
      return { ...state, deletedClass: { class: null, error: error, loading: false } };

    case Constants.CLASS_RESET_DELETED:
      return { ...state, deletedClass: { class: null, error: null, loading: false } };

    case Constants.CLASS_SELECT_PROFESSORS:
      allList = state.professorsSelected.all;
			/* allList = _.unionBy(allList, action.payload.all, 'id');*/
			addedList = state.professorsSelected.added;
			addedList = _.unionBy(addedList, [ action.payload.added ], 'id');
			allList = _.unionBy(allList, addedList, 'id');
			return { ...state, professorsSelected: { ...state.professorsSelected, all: allList, added: addedList } }

    case Constants.CLASS_RESET_SELECT_PROFESSORS:
      return { ...state, professorsSelected: { all: [], added: [], removed: [] } }

    case Constants.CLASS_SELECT_STUDENTS:
			allList = state.studentsSelected.all;
			/* allList = _.unionBy(allList, action.payload.all, 'id');*/
      addedList = state.studentsSelected.added;
      // addedList.push(action.payload.added);
			addedList = _.unionBy(addedList, [ action.payload.added ], 'id');
			allList = _.unionBy(allList, addedList, 'id');
      return { ...state, studentsSelected: { ...state.studentsSelected, all: allList, added: addedList } }

    case Constants.CLASS_RESET_SELECT_STUDENTS:
      return { ...state, studentsSelected: { all: [], added: [], removed: [] } }

    case Constants.CLASS_REMOVE_PROFESSORS:
      removedList = state.professorsSelected.removed;
      addedList = state.professorsSelected.added;
      allList = state.professorsSelected.all;
      // removedList.push(action.payload)
			removedList = _.unionBy(removedList, action.payload, 'id');
			addedList = _.differenceWith(addedList, removedList, _.isEqual);
      if (!_.isEmpty(removedList)) {
        allList = _.differenceWith(allList, removedList, _.isEqual);
      }

      return { ...state, professorsSelected: { all: allList, added: addedList, removed: removedList } }

    case Constants.CLASS_REMOVE_STUDENTS:
      removedList = state.studentsSelected.removed;
      addedList = state.studentsSelected.added;
      allList = state.studentsSelected.all;
      // removedList.push(action.payload);
			removedList = _.unionBy(removedList, action.payload, 'id');
      addedList = _.differenceWith(addedList, removedList, _.isEqual);
      if (!_.isEmpty(removedList)) {
        allList = _.differenceWith(allList, removedList, _.isEqual);
      }
      return { ...state, studentsSelected: { all: allList, added: addedList, removed: removedList } }

    case Constants.CLASS_COUNT:
      return { ...state, classesList: { ...state.classesList, loading: true} }

    case Constants.CLASS_COUNT_SUCCESS:
      const { count } = action.payload;
      return { ...state, classesList: { ...state.classesList, count: count, loading: false} }

    case Constants.CLASS_COUNT_FAILURE:
      error = action.payload || {message: action.payload.message};
      return { ...state, classesList: { ...state.classesList, error: error, loading: false} }

    case Constants.CLASS_SUBJECT_SELECT:
      let subjectsSelected = state.subjectsSelected;
      let subjectSelected = action.payload;
      subjectsSelected.push(subjectSelected);
      return { ...state, subjectsSelected: subjectsSelected};

    case Constants.CLASS_SUBJECTS_FETCH: // start fetching class's subjects and set loading = true
      return { ...state, activeClass: { ...state.activeClass, class: { ...state.activeClass.class, subjects: {loading: true}} } };

    case Constants.CLASS_SUBJECTS_FETCH_SUCCESS:
      _class = state.activeClass.class;
      _class.subjects = action.payload.subjects;
      classId = action.payload.class_id;
      classes = state.classesList.classes;
      let classFound = classes.find((item) => {
        return item.id === classId
      });
      newClassesList = classes;
      if (classFound) {
        classFound.subjects = _class.subjects;
        newClassesList = classes.map(item => item.id === classFound.id ? classFound : item);
      }

      console.log("newClassesList", newClassesList)

      return { 
        ...state, 
        activeClass: { ...state.activeClass, class: _class, loading: false},
        classesList: {classes: newClassesList, error: null, loading: false}
      }

    case Constants.CLASS_SUBJECTS_FETCH_FAILURE:
      error = action.payload.class || { message: action.payload.message }
      return { ...state, activeClass: { class: null, error: error, loading: false } };

    /* SELECT A CLASS */
    case Constants.CLASS_SELECT:
      return { ...state, activeClass: { ...state.activeClass, class: action.payload }};

    // case Constants.classes.CLASS_SUBJECTS_SET:
    //   const { class_id, subjects } = action.payload;
      
    //   return { ...state }

    default:
      return state;
  }
}
