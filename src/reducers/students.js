import Constants from 'constants/students';

const initialState =
  { studentsList:
    { students: []
    , error: null
    , loading: false
    }
  , newStudent:
    { student: null
    , error: null
    , loading: false
    }
  , activeStudent:
    { student: null
    , error: null
    , loading: false
    }
  , deletedStudent:
    { student: null
    , error: null
    , loading: false
    }
  }

export default function reducer(state = initialState, action = {}) {
  let error;
  switch (action.type) {
    case Constants.STUDENT_CREATE:
      return {...state, newStudent: {...state.newStudent, loading: true}}

    case Constants.STUDENT_CREATE_SUCCESS:
      return { ...state, newStudent: { student: action.payload, error: null, loading: false } }

    case Constants.STUDENT_CREATE_FAILURE:
      error = action.payload || { message: action.payload.message };
      return { ...state, newStudent: { student: null, error: error, loading: false } };

    case Constants.STUDENT_RESET_NEW:
      return { ...state, newStudent: {student: null, error: null, loading: false} };

    case Constants.STUDENT_ADD_RESPONSIBLE:
      return { ...state, newStudent: {...state.newStudent, loading: true} }

    case Constants.STUDENT_ADD_RESPONSIBLE_SUCCESS:
      return { ...state, newStudent: { ...state.newStudent, error: null, loading: false } }

    case Constants.STUDENT_ADD_RESPONSIBLE_FAILURE:
      error = action.payload || { message: action.payload.message };
      return { ...state, newStudent: { student: null, error: error, loading: false } };

      case Constants.STUDENT_RESET_ADD_RESPONSIBLE:
        // TODO: find out a way to clean up the element in the responsibles array
        // into the student so that we have a reset in the correct element in the store
        const responsibleIndex = action.payload;
        return { ...state, newStudent: {student: null, error: null, loading: false} };

    case Constants.STUDENTS_FETCH: // start fetching students and set loading = true
      return { ...state, studentsList: {students: [], error: null, loading: true} };

    case Constants.STUDENTS_FETCH_SUCCESS: // return list of students and make loading = false
      return { ...state, studentsList: {students: action.payload.data, error: null, loading: false} };

    case Constants.STUDENTS_FETCH_FAILURE: // return error and make loading = false
      error = action.payload || {message: action.payload.message};
      return { ...state, studentsList: {students: [], error: error, loading: false} };

    case Constants.STUDENTS_RESET_LIST: // reset studentsList to initial state
      return { ...state, studentsList: {students: [], error: null, loading: false} };

    case Constants.STUDENT_FETCH:
      return { ...state, activeStudent: { ...state.activeStudent, loading: true } };

    case Constants.STUDENT_FETCH_SUCCESS:
      let { birthdate, ...student } = action.payload.data;
      if (birthdate instanceof Object) {
        student.birthdate = new Date(action.payload.data.birthdate.$date);
      } else {
        if (birthdate)
          student.birthdate = new Date(action.payload.data.birthdate);
      }
      return { ...state, activeStudent: { student: student, error: null, loading: false } };

    case Constants.STUDENT_FETCH_FAILURE:
      error = action.payload.student || { message: action.payload.message }
      return { ...state, activeStudent: { student: null, error: error, loading: false } };

    case Constants.STUDENT_RESET_STUDENT:
      return { ...state, activeStudent: { student: null, error: null, loading: false } };

    case Constants.STUDENT_DELETE:
      return { ...state, deletedStudent: { student: null, error: null, loading: true } };

    case Constants.STUDENT_DELETE_SUCCESS:
      let { students } = state.studentsList;
      console.log("lista atual na store:", students);
      console.log("aluno deletado na store:", action.payload.data);
      const deletedStudent = action.payload.data;
      const newStudentsList = students.filter( (student) => student.id !== deletedStudent.id )
      return {
        ...state,
        studentsList: {students: newStudentsList, error: error, loading: false},
        deletedStudent: { student: action.payload.data, error: null, loading: false }
      };

    case Constants.STUDENT_DELETE_FAILURE:
      error = action.payload || { message: action.payload.message };
      return { ...state, deletedStudent: { student: null, error: error, loading: false } };

    case Constants.STUDENT_RESET_DELETED:
      return { ...state, deletedStudent: { student: null, error: null, loading: false } };



    default:
      return state;
  }
}
