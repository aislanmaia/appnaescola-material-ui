import Constants from 'constants/subjects';
import _         from 'lodash';

const initialState =
  { subjectsList:
    { subjects: []
    , error: null
    , loading: false
    }
  , newSubject:
    { subject: null
    , error: null
    , loading: false
    }
  , activeSubject:
    { subject: null
    , error: null
    , loading: false
    }
  , deletedSubject:
    { subject: null
    , error: null
    , loading: false
    }
  , selectedSubject:
    { subject: null }
  }

export default function reducer(state = initialState, action = {}) {
  let error, newSubjecstList;
  switch (action.type) {
		case Constants.SUBJECT_CREATE: {
			/* Optimistically creates a new subject and add it to list */
			let subjects = state.subjectsList.subjects;
			const newSubject = action.payload.subject;
			subjects.unshift(newSubject);
			console.log("nova lista de subjects", subjects);
			return { ...state,
							 newSubject: {...state.newSubject, subject: newSubject, loading: false},
							 subjectsList: {subjects: subjects, error: null, loading: false}
						 }
		}

		case Constants.SUBJECT_CREATE_SUCCESS: {
			let created = action.payload.data;
			let subjects = state.subjectsList.subjects;
			_.remove(subjects, (s) => {
				return s.uuid === created.uuid;
			});
			delete created.uuid;
			subjects.push(created);
			console.log("lista de subjects definitiva", subjects);
      return { ...state,
							 newSubject: { subject: created, error: null, loading: false },
							 subjectsList: {subjects: subjects, error: null, loading: false}
						 }
		}

		case Constants.SUBJECT_CREATE_FAILURE: {
      const error = action.payload || { message: action.payload.message };
			let subjects = state.subjectsList.subjects;
			const oldSubject = action.meta.subject;
			const newList = _.difference(subjects, [ oldSubject ]);
      return { ...state,
							 newSubject: { subject: null, error: error, loading: false },
							 subjectsList: {subjects: newList, error: null, loading: false}
						 };
		}

		case Constants.SUBJECT_UPDATE:
			newSubjecstList = updateList(state.subjectsList.subjects, action.payload.newSubject);
			return Object.assign({}, state, { ...state, subjectsList: { ...state.subjectsList, subjects: newSubjecstList} });

		case Constants.SUBJECT_UPDATE_SUCCESS:
			newSubjecstList = updateList(state.subjectsList.subjects, action.meta.newSubject);
			return Object.assign({}, state, { ...state, subjectsList: { ...state.subjectsList, subjects: newSubjecstList} });

		case Constants.SUBJECT_UPDATE_FAILURE:
			console.log("loggin from reducer SUBJECT_UPDATE_FAILURE")
			console.log("state.subjectsList.subjects: ", state.subjectsList.subjects);
			console.log("params meta: ", action.meta);
			newSubjecstList = updateList(state.subjectsList.subjects, action.meta.oldSubject);
			return Object.assign({}, state, { ...state, subjectsList: { ...state.subjectsList, subjects: newSubjecstList} });


    case Constants.SUBJECT_RESET_NEW:
      return { ...state, newSubject: {subject: null, error: null, loading: false} };

    case Constants.SUBJECTS_FETCH: // start fetching subjects and set loading = true
      return { ...state, subjectsList: {...state.subjectsList, loading: true} };

    case Constants.SUBJECTS_FETCH_SUCCESS: // return list of subjects and make loading = false
      return { ...state, subjectsList: {subjects: action.payload.data, error: null, loading: false} };

    case Constants.SUBJECTS_FETCH_FAILURE: // return error and make loading = false
      error = action.payload || {message: action.payload.message};
      return { ...state, subjectsList: {...state.subjectsList, error: error, loading: false} };

    case Constants.SUBJECTS_RESET_LIST: // reset subjectsList to initial state
      return { ...state, subjectsList: {subjects: [], error: null, loading: false} };

    case Constants.SUBJECT_FETCH:
			console.log(" chamou SUBJECT_FETCH com payload = ", action.payload );
      // return { ...state, activeSubject: { ...state.activeSubject, loading: true } };
			return { ...state, activeSubject: { ...state.activeSubject, subject: action.payload } };

    case Constants.SUBJECT_FETCH_SUCCESS:
      return { ...state, activeSubject: { subject: action.payload.data, error: null, loading: false } };

    case Constants.SUBJECT_FETCH_FAILURE:
      error = action.payload.subject || { message: action.payload.message }
      return { ...state, activeSubject: { subject: null, error: error, loading: false } };

    case Constants.SUBJECT_RESET_SUBJECT:
      return { ...state, activeSubject: { subject: null, error: null, loading: false } };

    case Constants.SUBJECT_DELETE:
      return { ...state, ...state.deletedSubject, loading: true };

    case Constants.SUBJECT_SELECT:
      return { ...state, selectedSubject: { subject: action.payload } };

    case Constants.SUBJECT_DELETE_SUCCESS:
      let { subjects } = state.subjectsList;
      console.log("lista atual na store:", subjects);
      console.log("matéria deletada na store:", action.payload);
      const deletedSubject = action.payload;
      const newSubjectsList = subjects.filter( (subject) => subject.id !== deletedSubject.id )
      return {
        ...state,
        subjectsList: {subjects: newSubjectsList, error: error, loading: false},
        deletedSubject: { subject: action.payload, error: null, loading: false }
      };

    case Constants.SUBJECT_DELETE_FAILURE:
      error = action.payload || { message: action.payload.message };
      return { ...state, deletedSubject: { subject: null, error: error, loading: false } };

    case Constants.SUBJECTS_RESET_DELETED:
      return { ...state, deletedSubject: { subject: null, error: null, loading: false } };



    default:
      return state;
  }
}

const updateList = (subjectsList, subject) => {
	return subjectsList.map((s, i) => {
		if ( s.id === subject.id ) {
			return s[i] = subject;
		}
		return s;
	})
};
