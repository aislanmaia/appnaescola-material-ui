import Constants from 'constants/classrooms';
import _ from 'lodash';

const initialState =
  { classroomsList:
    { classrooms: null
		, searches: []
    , error: null
    , loading: false
    }
  , newClassroom:
    { classroom: null
		, error: null
		, loading: false
    }
  , activeClassroom:
    { classroom: null
    , error: null
    , loading: false
    }
  , deletedClassroom:
    { classroom: null
		, success: null
    , error: null
    , loading: false
    }
  }

export default function reducer(state = initialState, action = {}) {
  let error, classrooms, newList, newClassroom, oldClassroom;
  switch (action.type) {
    case Constants.CLASSROOM_CREATE:
		  /* Optimistic creates a new classroom and adds to the list */
      // return {...state, newClassroom: {...state.newClassroom, loading: true}}
			classrooms = state.classroomsList.classrooms.data;
			newClassroom = action.payload.classroom;
			classrooms.unshift(newClassroom);
			return { ...state,
							newClassroom: { classroom: newClassroom, error: null, loading: false },
							classroomsList: {classrooms: {...state.classroomsList.classrooms, data: classrooms}, error: null, loading: false}
						 };

    case Constants.CLASSROOM_CREATE_SUCCESS:
			/* Retirar o novo classroom verificando pelo uuid retornado com o ja existente e subsititui-lo */
      // return { ...state, newClassroom: { classroom: action.payload, error: null, loading: false } }
			let created = action.payload.data;
			classrooms = state.classroomsList.classrooms.data;
			_.remove(classrooms, (c) => {
				return c.uuid === created.uuid;
			});
			delete created.uuid;
			classrooms.unshift(created);
			return { ...state,
							 newClassroom: { classroom: created, error: null, loading: false },
							 classroomsList: {classrooms: {...state.classroomsList.classrooms, data: classrooms}, error: null, loading: false}
						 };

    case Constants.CLASSROOM_CREATE_FAILURE:
      error = action.payload || { message: action.payload.message };
			classrooms = state.classroomsList.classrooms.data;
			oldClassroom = action.meta.classroom;
			newList = _.difference(classrooms, [ oldClassroom ]);
			return { ...state,
							newClassroom: { classroom: null, error: error, loading: false },
							classroomsList: {classrooms: {...state.classroomsList.classrooms, data: newList}, error: null, loading: false}
						};

    case Constants.CLASSROOM_RESET_NEW:
      return { ...state, newClassroom: {classroom: null, error: null, loading: false} };

    case Constants.CLASSROOM_UPDATE:
		  /* Optmistic update the list */
			classrooms = state.classroomsList.classrooms.data || [];
			newList = classrooms.map(c => c.id === action.payload.newClassroom.id ? action.payload.newClassroom : c);
			return { ...state,
							classroomsList: {classrooms: {...state.classroomsList.classrooms, data: newList}, error: null, loading: false}
						};
      // return {...state, activeClassroom: {...state.activeClassroom, loading: true}}

    case Constants.CLASSROOM_UPDATE_SUCCESS:
		  /* Case success, just returns the current classrooms list because the optimitstic updates were already applied */
			return { ...state, classroomsList: {...state.classroomsList} };

		case Constants.CLASSROOM_UPDATE_FAILURE:
      error = action.payload || { message: action.payload.message };
			classrooms = state.classroomsList.classrooms.data || [];
			let restoredList = classrooms.map(c => c.id === action.meta.oldClassroom.id ? action.meta.oldClassroom : c);
			return { ...state,
							 classroomsList: {classrooms: {...state.classroomsList.classrooms, data: restoredList}, error: null, loading: false},
							 activeClassroom: {...state.activeClassroom, error: error, loading: false}
						 };

    case Constants.CLASSROOM_RESET_UPDATE:
      return { ...state, activeClassroom: { classroom: null, error: null, loading: false } }

    case Constants.CLASSROOMS_FETCH: // start fetching classrooms and set loading = true
			return { ...state, classroomsList: {classrooms: state.classroomsList.classrooms, error: null, loading: true} };

    case Constants.CLASSROOMS_FETCH_SUCCESS: // return list of classrooms and make loading = false
			/*
				TODO: verificar se no array de classrooms existem já dados carregados e dar um append na lista
							se for conveniente isto.
			*/
			let data = [];
			if (state.classroomsList.classrooms && !_.isEmpty( state.classroomsList.classrooms )) {
				if (state.classroomsList.classrooms.hasOwnProperty('data')) {
					data = state.classroomsList.classrooms.data;
					/* Diffing prev data collection with payload data */
					if (!_.isEqual(data, action.payload.data)) {
						data = _.unionBy(data, action.payload.data, "id");
					}
					classrooms = {
						...action.payload,
						data: data
					};
				}
			} else {
				classrooms = {
					...action.payload,
					data: _.unionBy(data, action.payload.data, "id")
				};
			}
			return { ...state, classroomsList: {classrooms: classrooms, error: null, loading: false} };

		case Constants.CLASSROOMS_FETCH_FAILURE: // return error and make loading = false
      error = action.payload || {message: action.payload.message};
      return { ...state, classroomsList: {...state.classroomsList, error: error, loading: false} };

    case Constants.CLASSROOMS_RESET_LIST: // reset classroomsList to initial state
      return { ...state, classroomsList: {classrooms: [], error: null, loading: false} };

    case Constants.CLASSROOMS_SEARCH:
      return { ...state, classroomsList: { ...state.classroomsList, loading: true } };

		case Constants.CLASSROOMS_SEARCH_SUCCESS:
			return { ...state, classroomsList: { ...state.classroomsList, searches: action.payload, error: null, loading: false } };

		case Constants.CLASSROOMS_SEARCH_FAILURE:
			return { ...state, classroomsList: { ...state.classroomsList, searches: [], error: action.payload, loading: false } };

    case Constants.CLASSROOM_FETCH:
      return { ...state, activeClassroom: { ...state.activeClassroom, loading: true } };

    case Constants.CLASSROOM_FETCH_SUCCESS:
      return { ...state, activeClassroom: { classroom: action.payload.data, error: null, loading: false } };

    case Constants.CLASSROOM_FETCH_FAILURE:
      error = action.payload.classroom || { message: action.payload.message }
      return { ...state, activeClassroom: { classroom: null, error: error, loading: false } };

    case Constants.CLASSROOM_RESET_CLASSROOM:
      return { ...state, activeClassroom: { classroom: null, error: null, loading: false } };

    case Constants.CLASSROOM_DELETE:
		  /* Optimistic deletes from the list */
			classrooms = state.classroomsList.classrooms.data;
			newList = classrooms.filter(c => c.id !== action.payload.classroom.id);
		return { ...state,
						 deletedClassroom: { ...state.deletedClassroom, success: true, classroom: action.payload.classroom },
						 classroomsList: { ...state.classroomsList, classrooms: { ...state.classroomsList.classrooms, data: newList } }
					 };

    case Constants.CLASSROOM_DELETE_SUCCESS:
			/* Case success, just returns the current state because the optimitstic updates were already applied */
      return { ...state };

    case Constants.CLASSROOM_DELETE_FAILURE:
		  /* Case failure, sets error message and returns the deleted classroom to the list, sorting by numbers in name */
      error = action.payload || { message: action.payload.message };
			classrooms = state.classroomsList.classrooms.data;
			oldClassroom = action.meta.classroom;
			newList = _.unionBy(classrooms, [ oldClassroom ], "id");
			newList = newList.sort(compareNumbers);
			return { ...state,
							 deletedClassroom: { classroom: null, success: null, error: error, loading: false },
							 classroomsList: { ...state.classroomsList, classrooms: { ...state.classroomsList.classrooms, data: newList } }
						};

    case Constants.CLASSROOM_RESET_DELETED:
			return { ...state, deletedClassroom: { classroom: null, success: null, error: null, loading: false } };


		/* Batch deletes */
    case Constants.CLASSROOMS_BATCH_DELETE:
		  /* Optimistic deletes the classrooms from the list */
			classrooms = state.classroomsList.classrooms.data;
			// newList = classrooms.filter(c => c.id !== action.payload.classroom.id);
			newList = _.difference(classrooms, action.payload.classrooms);
		return { ...state,
						 deletedClassroom: { ...state.deletedClassroom, success: true },
						 classroomsList: { ...state.classroomsList, classrooms: { ...state.classroomsList.classrooms, data: newList } }
					 };

    case Constants.CLASSROOMS_BATCH_DELETE_SUCCESS:
		  console.log("Entrou em CLASSROOMS_BATCH_DELETE_SUCCESS");
			/* Case success, just returns the current state because the optimitstic updates were already applied */
      return { ...state };
		// return loop(
    //    { ...state },
		// 	 Cmd.run(toastr.success, { args: ["", "Sucesso"] })
		// );

		case Constants.CLASSROOMS_BATCH_DELETE_FAILURE:
		  /* Case failure, sets error message and returns the deleted classroom to the list, sorting by numbers in name */
      error = action.payload || { message: action.payload.message };
			classrooms = state.classroomsList.classrooms.data;
			// let oldClassroom = action.meta.classroom;
			newList = _.unionBy(classrooms, action.meta.classrooms, "id");
			newList = newList.sort(compareNumbers);
			// return loop(
			// 	 { ...state,
			// 		 deletedClassroom: { classroom: null, error: error, loading: false },
			// 		 classroomsList: { ...state.classroomsList, classrooms: { ...state.classroomsList.classrooms, data: newList } }
			// 	 },
			// 	 Cmd.run(toastr.warning, { args: ["Erro", "Error....."]})
			// )
			return { ...state,
								deletedClassroom: { classroom: null, error: error, loading: false },
								classroomsList: { ...state.classroomsList, classrooms: { ...state.classroomsList.classrooms, data: newList } }
							};

		case Constants.CLASSROOMS_CLEAR_DELETE_STATE:
			return { ...state, deletedClassroom: { classroom: null, success: null, error: null, loading: false} };

		case Constants.CLASSROOM_SET_ACTIVE_CLASSROOM:
			return { ...state,
							activeClassroom: { classroom: action.payload.classroom, success: null, error: null, loading: false}
						}

    default:
      return state;
  }
}


const compareNumbers = (classroomCurrent, classroomNext) => {
	let num1 = extractNumbersFromName(classroomCurrent.name);
	let num2 = extractNumbersFromName(classroomNext.name);
	if (!!num1 && !!num2) {
		if (num1 === num2) return 0;
		return (num1 < num2) ? -1 : 1;
	} else {
		return 0;
	}
};

const extractNumbersFromName = (nameStringWithNumber) => {
	let numStr = nameStringWithNumber.replace(/[^0-9]/g, '');
	return (numStr) ? _.parseInt(numStr) : 0;
};
