import { combineReducers }        from 'redux';
//import { routerReducer }          from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import {reducer as toastrReducer} from 'react-redux-toastr';
// import activities                 from './activities';
// import drawer                     from './drawer';
import subjects                   from './subjects';
import classrooms                 from './classrooms';
// import classplans                 from './class_plans';
import classes                    from './classes';
import professors                 from './professors';
import students                   from './students';
// import responsibles               from './responsibles';
import user                       from './user';
import login                      from './login';

export default combineReducers({
  //routing: routerReducer,
  form: formReducer,
  toastr: toastrReducer,
  user,
  login,
  // activities,
  // drawer,
  subjects,
  classrooms,
  // classplans,
  classes,
  professors,
  students,
  // responsibles
});
